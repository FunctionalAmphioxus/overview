#!/usr/bin/perl

# Comments & Questions: Manuel Irimia (mirimia@gmail.com).
# Original name of script: CompareExpr_subfunct-v3.pl
# It needs a table of normalized cRPKMs (or any other measure of Gene expression)
#     for amphioxus (Bla) as well as vertebrates to compare (Mmu, Xtr, Mmu).
# The expr tables need to be sorted by tissue type. E.g.
# Bla_cRPKMs-TissuesC-s-NORM.tab 
# 1  GeneID
# 2  Gene_name
# 3  Eggs_G
# 4  Embr_21hpf_h
# 5  MaleGonads_b
# 6  FemGonads_b
# 7  Epidermis_b
# 8  Gut_b
# 9  Hepatic_b
# 10 Muscle_b
# 11 NeuralTube_b

die "\nUsage: Analyze_Red_Subf_Spec.pl Vert_species [Mmu,Dre,Xtr]\n\n" if !$ARGV[0];

### variables
$sp1="Bla"; # this could be changed to, e.g. compare Mmu with Dre (for the 3R)
$sp2=$ARGV[0]; # the vertebrate species
$min_tis=9; # requires the familes to be expressed "ancestrally" in ALL 9 tissues

$min_expr_default=5; # cut-off for calling an gene ON or OFF
# Defines the cut off-per species and tissue
#    In a more complex version of the script, it allowed to define the cut-off based on the average
#    of expression for each tissue and species. This was not used in the paper.
for $i (2..10){
    $min_expr{$sp1}{$i}=$min_expr_default;
    $min_expr{$sp2}{$i}=$min_expr_default;
}

### standard local abbreviations:
#      $g  => geneID
#      $cl => clusterID of the family
#      $sp => species


### Pre-parsing species expression to define which gene families to use (GE and Orth tables may not match fully)
### This can be modified to adapt to the format of the names of expression tables
open (EXPR_SP1a, "$sp1"."_cRPKMs-TissuesC-s-NORM.tab") || die "Expression Table for $sp1";
open (EXPR_SP2a, "$sp2"."_cRPKMs-TissuesC-s-NORM.tab") || die "Expression Table for $sp2";
<EXPR_SP1a>;
while (<EXPR_SP1a>){
    chomp;
    @t=split(/\t/);
    $gene_with_expr{$t[0]}=1; # just to make sure that the genes selected have GE values
}
close EXPR_SP1a;
<EXPR_SP2a>;
while (<EXPR_SP2a>){
    chomp;
    @t=split(/\t/);
    $gene_with_expr{$t[0]}=1; # just to make sure that the genes selected have GE values
}
close SP2a;


### Parses the orthology table (Supplementary Dataset 7 in Marletaz et al)
#   The format is that of clusters of homologs, similar to multiparanoid
#   Supplementary_Dataset_7.txt (original name: All_geneFam_red-medaka_parsed-Ens.txt)
open (ORTH, "Supplementary_Dataset_7.txt") || die "Table with Orthology clusters";
while (<ORTH>){
    chomp;
    @t=split(/\t/);
    $cl=$t[0];
    $sp=$t[1];
    $g=$t[2];

    next if !$gene_with_expr{$g}; # excludes genes with no expr info (18/05/17)
    
    $tally_genes_per_cl{$cl}{$sp}++; # counts the number of genes per species for each cluster
    $g_cl{$g}=$cl; # a simple assignment of GeneID => ClusterID 
    $cl_g{$cl}{$sp}.="$g;"; # array containing all genes in cluster for each species
}
close ORTH;


### Uploads lists for OHNOLOG and 1:1 families (Supplementary Dataset 12 in Marletaz et al)

#   OHNO: Supplementary_Dataset_9-OHNO.txt (original name: Ohnologs_Ferdi-F-clusters-v3.tab)
open (OHNO, "Supplementary_Dataset_9-OHNO.txt") || die "Cannot open the list of ohnolog families\n";
while (<OHNO>){
    chomp;
    @t=split(/\t/);
    $cl=$t[0];
    $sp=$t[1];
    $g=$t[2];
    
    ### raw number of ohnolog families
    $ohno_0++ if !$done0{$cl};
    $done0{$cl}=1;

    ### Only allows families with 1 Bla member
    next if $tally_genes_per_cl{$cl}{$sp1}>1;
    ### To account for species that may lose the OHNO status by not having expression data
    next if $tally_genes_per_cl{$cl}{$sp2}<2;

    ### number of ohno after fam size (with expr) filters
    $ohno_1++ if !$valid_fam{$cl};

    $fam_ohno{$cl}=1; # is the cluster $cl a ohnolog family?
    $g_ohno{$g}=1; # is the gene $g a member of an ohnolog family?
    $valid_fam{$cl}=1; # only uses these families for later
}
close OHNO;

#   ONE: Supplementary_Dataset_9-ONE.txt  (original name: List_of_1to1-Bla_Dre_Xtr_Mmu_Ola-clusters-v3.tab)
open (ONE, "Supplementary_Dataset_9-ONE.txt") || die "Cannot open the list of 1:1 familes\n";
while (<ONE>){
    chomp;
    @t=split(/\t/);
    $cl=$t[0];
    $sp=$t[1];
    $g=$t[2];

    ### To account for species that may lose the status by not having expr data
    next if $tally_genes_per_cl{$cl}{$sp1}!=1 || $tally_genes_per_cl{$cl}{$sp2}!=1;

    $fam_one{$cl}=1; # is the cluster $cl a 1:1 family? 
    $g_one{$g}=1; # is the gene $g a member of a 1:1 family? 
    $valid_fam{$cl}=1; # only uses these families for later
}
close ONE;


### Reopens the expression data, now to really parse it based on family info
open (EXPR_SP1, "$sp1"."_cRPKMs-TissuesC-s-NORM.tab") || die "Expr for $sp1";
$h_Sp1=<EXPR_SP1>;
chomp($h_Sp1);
@headSp1=split(/\t/,$h_Sp1); 
while (<EXPR_SP1>){
    chomp;
    @t=split(/\t/);
    $g=$t[0];
    $gene_name{$g}=$t[1];

    ### Gets the info for each tissue for each gene
    for $i (2..$#t){
        $OK{$g}=1 if $t[$i]>$min_expr{$sp1}{$i}; # i.e. the gene is expressed in at least 1 tissue
        $expr_gene{$sp1}{$g}{$i}=sprintf("%.2f",$t[$i]); # since expr table needs to be sorted, $i is the same in each species
	$expr_fam{$sp1}{$g_cl{$g}}{$i}=1 if $t[$i]>$min_expr{$sp1}{$i}; # gets the UNION of expression
	$expr_famS{$sp1}{$g_cl{$g}}{$i}+=$t[$i]; # i.e. the values are summed to later calculate the SUM of expression
    }
}
close EXPR_SP1;

open (EXPR_SP2, "$sp2"."_cRPKMs-TissuesC-s-NORM.tab") || die "Expr for $sp2";
$h_Sp2=<EXPR_SP2>;
chomp($h_Sp2);
@headSp2=split(/\t/,$h_Sp2); 
while (<EXPR_SP2>){
    chomp;
    @t=split(/\t/);
    $g=$t[0];
    $gene_name{$g}=$t[1];

    ### Gets the info for each tissue for each gene
    for $i (2..$#t){
        $OK{$g}=1 if $t[$i]>$min_expr{$sp2}{$i}; # i.e. the gene is expressed in at least 1 tissue
        $expr_gene{$sp2}{$g}{$i}=sprintf("%.2f",$t[$i]); # since expr table needs to be sorted, $i is the same in each species
	$expr_fam{$sp2}{$g_cl{$g}}{$i}=1 if $t[$i]>$min_expr{$sp2}{$i}; # gets the UNION of expression
	$expr_famS{$sp2}{$g_cl{$g}}{$i}+=$t[$i]; # i.e. the values are summed to later calculate the SUM of expression
    }
}
close EXPR_SP2;


####### Now does the comparison per LIST of genes
## Output files per fate of Ohnolog
open (SUBF, ">Strict_subf-MinTis_$min_tis-$sp1-$sp2-$min_expr{$sp1}{2}-$min_expr{$sp2}{2}-NORM-OR-v3.tab");
open (SPEC, ">Specialization-MinTis_$min_tis-$sp1-$sp2-$min_expr{$sp1}{2}-$min_expr{$sp2}{2}-NORM-OR-v3.tab");
open (ELSE, ">Redundant-MinTis_$min_tis-$sp1-$sp2-$min_expr{$sp1}{2}-$min_expr{$sp2}{2}-NORM-OR-v3.tab");

print SUBF "TYPE\tORTH_FAM\tSPECIES\tGENE\tNAME\tTOTAL_TIS\tEgg\tEmbryo\tTestis\tOvary\tSkin\tGut\tLiver\tMuscle\tBrain\n";
print SPEC "TYPE\tORTH_FAM\tSPECIES\tGENE\tNAME\tTOTAL_TIS\tEgg\tEmbryo\tTestis\tOvary\tSkin\tGut\tLiver\tMuscle\tBrain\n";
print ELSE "TYPE\tORTH_FAM\tSPECIES\tGENE\tNAME\tTOTAL_TIS\tEgg\tEmbryo\tTestis\tOvary\tSkin\tGut\tLiver\tMuscle\tBrain\n";

### Output files with GeneID by type of fate and by number of domains lost
$out_root1="O_$sp2";
$out_root2="G_SPEC_$sp2";
open ($out_root1, ">Gene_types-$sp2-v3.txt");
open ($out_root2, ">Spec_genes_byLost-$sp2-v3.txt");


####### Starts the analysis
open (DIFF, ">Difference_$sp2-$sp1-$min_expr{$sp1}{2}-$min_expr{$sp2}{2}-NORM-OR-v3.txt"); # temp file for the histograms
foreach $orth (sort {$a<=>$b} keys %valid_fam){ # orth => (new) cluster ID (aka cl)
    @g_Sp1=split(/\;/,$cl_g{$orth}{$sp1}); # this is forced to 1 in the current script
    @g_Sp2=split(/\;/,$cl_g{$orth}{$sp2});
    
### parses by gene in Sp1 (only 1)
    foreach $g1 (@g_Sp1){
	# Counts the number of tissues in which g1 is expressed and make a 011101010 like array
	$pos_Sp1=0; # number of expressed tissues (pos => positives)
	$string_Sp1="";
	for $i (2..$#headSp1){
	    $pos_Sp1++ if $expr_gene{$sp1}{$g1}{$i}>$min_expr{$sp1}{$i};
	    $string_Sp1.="1" if $expr_gene{$sp1}{$g1}{$i}>$min_expr{$sp1}{$i};
	    $string_Sp1.="0" if $expr_gene{$sp1}{$g1}{$i}<=$min_expr{$sp1}{$i};
	}
	# keeps a string with 0 and 1s for expression per gene
	$string{$g1}=$string_Sp1;

	### parses now each g from Sp2 and compares it to the g1 from Sp1
	foreach $g2 (@g_Sp2){
	    $pos_Sp2=0; # number of expressed tissues (pos => positives)
	    $string_Sp2="";
	    for $i (2..$#headSp2){
		$pos_Sp2++ if $expr_gene{$sp2}{$g2}{$i}>$min_expr{$sp2}{$i};
		$string_Sp2.="1" if $expr_gene{$sp2}{$g2}{$i}>$min_expr{$sp2}{$i};
		$string_Sp2.="0" if $expr_gene{$sp2}{$g2}{$i}<=$min_expr{$sp2}{$i};
	    }
	    
	    # keeps a string with 0 and 1s for expression per gene
	    $string{$g2}=$string_Sp2;
	    
	    ### Get the orth vs orth comparison per OHNO and 1-to-1 gene (Fig 6a,b)
	    #  At least one of the GENES MUST be expressed (using AND, similar results)
	    if ($pos_Sp1>=1 || $pos_Sp2>=1){
		$diff=$pos_Sp2-$pos_Sp1; ### the key value for histograms
		
		# this is not used in the paper, but it's the summary stats
		if ($fam_ohno{$orth}){ # OHNO families
		    $score{OHNO}{$sp1}++ if $pos_Sp1>$pos_Sp2;
		    $score{OHNO}{$sp2}++ if $pos_Sp1<$pos_Sp2;
		    $score{OHNO}{Eq}++ if $pos_Sp1==$pos_Sp2;
		    print DIFF "$orth=$g1=$gene_name{$g2}\t$diff\tOHNO\n";
		    
		    ### keep "memory" of genes from Sp2
		    $by_gene_Sp2{$g2}=$pos_Sp2;
		    $by_gene_Sp1{$g1}=$pos_Sp1;
		    $equal_gene{$orth}.="$g2;" if $pos_Sp1==$pos_Sp2;
		    $lower_gene{$orth}.="$g2;" if $pos_Sp1>$pos_Sp2;
		    $count_equal_gene{$orth}++ if $pos_Sp1==$pos_Sp2;
		    $count_lower_gene{$orth}++ if $pos_Sp1>$pos_Sp2;
		}
		if ($fam_one{$orth}){ # 1:1 families
		    $score{ONE}{$sp1}++ if $pos_Sp1>$pos_Sp2;
		    $score{ONE}{$sp2}++ if $pos_Sp1<$pos_Sp2;
		    $score{ONE}{Eq}++ if $pos_Sp1==$pos_Sp2;
		    print DIFF "$orth=$g1=$gene_name{$g2}\t$diff\t1-to-1\n";
		}
	    }
	}
    }
##########    

########## Parsing by FAMILY
    # Reinitializes the variables for each $g1 of $sp1
    $pos_Sp1_fam=$pos_Sp2_fam=0; 
    $pos_Sp1_famS=$pos_Sp2_famS=0;

    # Gets the ON/OFF calls per family
    for $i (2..$#headSp1){
	$pos_Sp1_fam+=$expr_fam{$sp1}{$orth}{$i}; # 0 or 1
	$pos_Sp1_famS++ if $expr_famS{$sp1}{$orth}{$i} > $min_expr{$sp1}{$i}; # all together
	$expr_fam{$sp1}{$orth}{$i}=0 if !$expr_fam{$sp1}{$orth}{$i};
	$string_fam{$orth}{$sp1}.="$expr_fam{$sp1}{$orth}{$i}";
    }
    for $i (2..$#headSp2){
	$pos_Sp2_fam+=$expr_fam{$sp2}{$orth}{$i}; # 0 or 1
	$pos_Sp2_famS++ if $expr_famS{$sp2}{$orth}{$i} > $min_expr{$sp2}{$i}; # all together
	$expr_fam{$sp2}{$orth}{$i}=0 if !$expr_fam{$sp2}{$orth}{$i};
	$string_fam{$orth}{$sp2}.="$expr_fam{$sp2}{$orth}{$i}";
    }
    
    ### Does the comparison for the Ohno UNION
    #   At least one FAMILY MUST be expressed in at least one species
    if ($pos_Sp1_fam>=1 || $pos_Sp2_fam>=1){
	$diff_fam=$pos_Sp2_fam-$pos_Sp1_fam; # the key for the histogram
	
	if ($fam_ohno{$orth}){ # i.e. the group is a ohnolog cluster, not a 1-to-1
	    $score{OHNO_fam}{$sp1}++ if $pos_Sp1_fam>$pos_Sp2_fam;
	    $score{OHNO_fam}{$sp2}++ if $pos_Sp1_fam<$pos_Sp2_fam;
	    $score{OHNO_fam}{Eq}++ if $pos_Sp1_fam==$pos_Sp2_fam;
	    print DIFF "$orth=$gene_name{$g_Sp2[0]}\t$diff_fam\tOHNO_fam\n";

	    
	    ####### Analysis of SPEC/SUBF/REDU (Fig 6c)
	    # Number of ohno in loop with at least 1 tissue expr
	    $ohno_2++;

	    # Requires the UNION of the FAMILIES to be expressed in ALL tissues in both species
	    if ($pos_Sp1_fam >= $min_tis && $string_fam{$orth}{$sp1} eq $string_fam{$orth}{$sp2}){ # i.e. the exact same tissues 
		$total_fam_analized++;
		
		# SUBFUNCTIONALIZATION: i.e. none of the Sp2 genes fully cover Sp1
		if (!$equal_gene{$orth}){ 
		    $total_subf++;
		    
		    # prints the data for Sp2
		    foreach $g2 (@g_Sp2){
			$by_gene_Sp2{$g2}=0 if !$by_gene_Sp2{$g2};
			print SUBF "SUBF\t$orth\t$sp2\t$g2\t$gene_name{$g2}\t$by_gene_Sp2{$g2}";
			for $i (2..$#headSp2){
			    print SUBF "\t$expr_gene{$sp2}{$g2}{$i}";
			}
			print SUBF "\n";
			# gene by fate
			print $out_root1 "$g2\tSUBFUNCT\n";
		    }
		    ## and now for Sp1
		    $t_g1=$g_Sp1[0]; # in principle this is always the same
		    print SUBF "SUBF\t$orth\t$sp1\t$t_g1\t$gene_name{$t_g1}\t$by_gene_Sp1{$t_g1}";
		    for $i (2..$#headSp1){
			print SUBF "\t$expr_gene{$sp1}{$t_g1}{$i}";
		    }
		    print SUBF "\n\n";
		}
		# At least one gene from Sp2 covers the entire Sp1 (RED and SPEC)
		elsif ($equal_gene{$orth}){ 
		    @lower_genes=split(/\;/,$lower_gene{$orth});
		    @equal_genes=split(/\;/,$equal_gene{$orth});
		    $check="";
		    
		    ### SPECIALIZATION: i.e. at least one gene has lost domains of expression
		    if ($lower_gene{$orth}){
			# Members that have lost domains:
			foreach $g2 (@lower_genes){
			    # Spec_strong: defined as 0-2 domains
			    if ($by_gene_Sp2{$g2} <= 2){
				# prints the data for Sp2
				$by_gene_Sp2{$g2}=0 if !$by_gene_Sp2{$g2};
				print SPEC "SPEC_STRONG\t$orth\t$sp2\t$g2\t$gene_name{$g2}\t$by_gene_Sp2{$g2}";
				for $i (2..$#headSp2){
				    print SPEC "\t$expr_gene{$sp2}{$g2}{$i}";
				}
				print SPEC "\n";
				$check=1;

				# gene by fate
				print $out_root1 "$g2\tSPEC_STRONG\n"; 
				# gene by domains lost
				$lost_dom=$pos_Sp1_fam-$by_gene_Sp2{$g2};
				print $out_root2 "$g2\t$lost_dom\n";
			    }
			    # Spec_mild: has lost domains, but has more than 2
			    else {
				# prints the data for Sp2				
				$by_gene_Sp2{$g2}=0 if !$by_gene_Sp2{$g2};
				print SPEC "SPEC_MILD\t$orth\t$sp2\t$g2\t$gene_name{$g2}\t$by_gene_Sp2{$g2}";
				for $i (2..$#headSp2){
				    print SPEC "\t$expr_gene{$sp2}{$g2}{$i}";
				}
				print SPEC "\n";

				# gene by fate
				print $out_root1 "$g2\tSPEC_MILD\n";
				# gene by domains lost
				$lost_dom=$pos_Sp1_fam-$by_gene_Sp2{$g2};
				print $out_root2 "$g2\t$lost_dom\n";
			    }
			    
			    ###  Scores tissue bias loss
			    @temp_by_tis=split(//,$string{$g2});
			    for $j (0..8){
				$tally_losses{$j}++ if $temp_by_tis[$j]==0; # directly scores losses
				$tally_retention{$j}++ if $temp_by_tis[$j]==1; # directly scores retention
				if ($by_gene_Sp2{$g2}<=2){ # retained in 0-2 tissues (SPEC_STRONG)
				    $tally_retentionS{$j}++ if $temp_by_tis[$j]==1; # directly scores retention
				}
			    }
			}
			# Spec_equal: have retained all "ancestral" domains
			foreach $g2 (@equal_genes){
			    if (!$done_SPEC{$g2}){
				# prints the data
				print SPEC "EQUAL\t$orth\t$sp2\t$g2\t$gene_name{$g2}\t$by_gene_Sp2{$g2}";
				for $i (2..$#headSp2){
				    print SPEC "\t$expr_gene{$sp2}{$g2}{$i}";
				}
				print SPEC "\n";
				$done_SPEC{$g2}=1;

				# gene by fate
				print $out_root1 "$g2\tSPEC_EQUAL\n";
				# gene by domains lot (0 here)
				$lost_dom=$pos_Sp1_fam-$by_gene_Sp2{$g2};
				print $out_root2 "$g2\t$lost_dom\n";
			    }
			}
			$total_spec++ if $check; # means that at least 1 got extreme
			$total_partial++ if !$check; # means that none got really extreme

			## Prints the data for the Sp1 gene (only 1 and always the same)
			$t_g1=$g_Sp1[0];
			print SPEC "FULL\t$orth\t$sp1\t$t_g1\t$gene_name{$t_g1}\t$by_gene_Sp1{$t_g1}";
			for $i (2..$#headSp1){
			    print SPEC "\t$expr_gene{$sp1}{$t_g1}{$i}";
			}
			print SPEC "\n\n";
		    }
		    ### REDUNDANT: all expressed in ALL domains (i.e. no lower_expression genes)
		    else { 
			$total_redundant++;
			foreach $g2 (@g_Sp2){
			    # prints the data
			    $by_gene_Sp2{$g2}=0 if !$by_gene_Sp2{$g2};
			    print ELSE "REDUNDANT\t$orth\t$sp2\t$g2\t$gene_name{$g2}\t$by_gene_Sp2{$g2}";
			    for $i (2..$#headSp2){
				print ELSE "\t$expr_gene{$sp2}{$g2}{$i}";
			    }
			    print ELSE "\n";

			    # gene by fate
			    print $out_root1 "$g2\tREDUNDANT\n";
			}
			
			# Prints the data for Sp1
			$t_g1=$g_Sp1[0];
			print ELSE "REDUNDANT\t$orth\t$sp1\t$t_g1\t$gene_name{$t_g1}\t$by_gene_Sp1{$t_g1}";
			for $i (2..$#headSp1){
			    print ELSE "\t$expr_gene{$sp1}{$t_g1}{$i}";
			}
			print ELSE "\n\n";
		    }
		}
	    }
	}
    } # closes the "at least one species with expr" and the "is ohno"
	
    ### Get the comparison for the OHNO SUM for the histograms (Fig. 6a)
    #   At least one FAMILY_SUM MUST have expression in at least one species
    if ($pos_Sp1_famS>=1 || $pos_Sp2_famS>=1){
	$diff_famS=$pos_Sp2_famS-$pos_Sp1_famS;
	if ($fam_ohno{$orth}){
	    $score{OHNO_famS}{$sp1}++ if $pos_Sp1_famS>$pos_Sp2_famS;
	    $score{OHNO_famS}{$sp2}++ if $pos_Sp1_famS<$pos_Sp2_famS;
	    $score{OHNO_famS}{Eq}++ if $pos_Sp1_famS==$pos_Sp2_famS;
	    print DIFF "$orth=$gene_name{$g_Sp2[0]}\t$diff_famS\tOHNO_famS\n";
	}
    }
}
close DIFF;

##### General summary
print "\nGENERAL SUMMARY OF ANALYSIS ($sp1 vs $sp2)\n";
print "Starting Ohno\t$ohno_0\n";
print "Ohno after size filter\t$ohno_1\n";
print "Ohno in final loop subf\t$ohno_2\n";
print "Ohno valid for subf\t$total_fam_analized\n";

##### Summary analysis Subf vs Spec (strong + mild) vs Redundancy
print "\nCOMPARISON OF SUBF vs SPEC vs REDUNDANCY\n";
print "Total subfunctionalized families\t$total_subf\n";
print "Total strongly specialized families\t$total_spec\n";
print "Total mildy specialized families\t$total_partial\n";
print "Total redundant families\t$total_redundant\n";

##### Prints the retention of Spec_Strong by tissue
print "\nRETENTION OF SPEC_STRONG BY TISSUE\n";
print "Tissue_$sp2\tNumber_of_Genes\n";
for $i (0..8){
    $j=$i+2;
    print "$headSp2[$j]\t$tally_retentionS{$i}\n";
}

#### Print summary of the histograms
print "\nSUMMARY OF HISTOGRAM ANALYSIS\n";
print "$sp1 vs $sp2 (Normalized and OR); Min_expr: $min_expr{$sp1}{2} and $min_expr{$sp2}{2}\n";
print "LIST\tHIGHER_$sp1\tHIGHER_$sp2\tEQUAL\n";
foreach $group (sort keys %score){
    print "$group\t$score{$group}{$sp1}\t$score{$group}{$sp2}\t$score{$group}{Eq}\n";
}

#### Prints the data for the histograms
open (DIFF, "Difference_$sp2-$sp1-$min_expr{$sp1}{2}-$min_expr{$sp2}{2}-NORM-OR-v3.txt") || die "Can't open the output with differences\n";
while (<DIFF>){
    chomp;
    @t=split(/\t/);
    $hist{$t[2]}{$t[1]}++;
}
close DIFF;

print "\nDATA FOR HISTOGRAMS\n";
print "GROUP";
foreach $group (sort keys %hist){
    print "\t$group";
}
print "\n";
for $i (-8..8){
    print "$i";
    foreach $group (sort keys %hist){
	$hist{$group}{$i}=0 if !$hist{$group}{$i};
	print "\t$hist{$group}{$i}";
    }
    print "\n";
}