# Fig 6a-e and EDF10a,b,f,g

Data for panels Fig 6b, 6d and 6e (as well as panels from EDF 10a, 10b, 10f and 10g) were generated using a single script (Analyze_Red_Subf_Spec.pl).

* The scripts needs to have in the same folder the following files:
    * 1) Supplementary_Dataset_7.txt: containing all orthology families
    * 2) Supplementary_Dataset_12-OHNO.txt: Ohnolog families
    * 3) Supplementary_Dataset_12-ONE.txt: 1:1 families
    * 4) Bla_cRPKMs-TissuesC-s-NORM.tab: file with AMPHIOXUS expression levels for the 9 selected tissues
    * 5) $sp2_cRPKMs-TissuesC-s-NORM.tab: file with VERTEBRATE SPECIES ($sp2) expression levels for the 9 selected tissues

Files 4 and 5 need to have their columns SORTED in the same way in both species.

* The script produces a screen-printed output, plus several additional files:
    * 1) Difference_$sp2-Bla-5-5-NORM-OR-v3.txt: result of the pairwise ortholog comparisons
    * 2) Gene_types-$sp2-v3.txt: All genes from $sp2 by fate
    * 3) Spec_genes_byLost-$sp2-v3.txt: All genes from $sp2 by number of domains lost
    * 4) Redundant-MinTis_9-Bla-$sp2-5-5-NORM-OR-v3.tab: Data for redundant families.
    * 5) Specialization-MinTis_9-Bla-$sp2-5-5-NORM-OR-v3.tab: Data for specialized families.
    * 6) Strict_subf-MinTis_9-Bla-$sp2-5-5-NORM-OR-v3.tab: Data for subfunctionalized families.


# Usage and results:

## perl Analyze_Red_Subf_Spec.pl Mmu

```
GENERAL SUMMARY OF ANALYSIS (Bla vs Mmu)
Starting Ohno	1509
Ohno after size filter	1215
Ohno in final loop subf	1212
Ohno valid for subf	370

COMPARISON OF SUBF vs SPEC vs REDUNDANCY
Total subfunctionalized families	80
Total strongly specialized families	77
Total mildy specialized families	167
Total redundant families	46

RETENTION OF SPEC_STRONG BY TISSUE
Tissue_Mmu	Number_of_Genes
Oocyte_M	17
Embr_E9_5_18s	3
Testis_B12	25
OvaryE	9
Skin_a	11
Intestine_M	6
Liver_B12	3
Muscle_B12	9
Brain_B12	24

SUMMARY OF HISTOGRAM ANALYSIS
Bla vs Mmu (Normalized and OR); Min_expr: 5 and 5
LIST	HIGHER_Bla	HIGHER_Mmu	EQUAL
OHNO	2023	438	550
OHNO_fam	383	378	451
OHNO_famS	277	437	499
ONE	981	831	638

DATA FOR HISTOGRAMS
GROUP	1-to-1	OHNO	OHNO_fam	OHNO_famS
-8	16	142	3	3
-7	20	176	10	6
-6	47	205	8	9
-5	68	228	25	19
-4	97	249	33	18
-3	144	270	70	37
-2	267	329	87	74
-1	320	379	147	111
0	638	550	451	499
1	281	193	139	159
2	153	99	74	80
3	115	65	55	62
4	96	37	41	47
5	69	24	29	27
6	63	10	14	26
7	37	7	11	17
8	15	2	8	11
```

## perl Analyze_Red_Subf_Spec.pl Dre

```
GENERAL SUMMARY OF ANALYSIS (Bla vs Dre)
Starting Ohno	1509
Ohno after size filter	1142
Ohno in final loop subf	1135
Ohno valid for subf	366

COMPARISON OF SUBF vs SPEC vs REDUNDANCY
Total subfunctionalized families	68
Total strongly specialized families	99
Total mildy specialized families	154
Total redundant families	45

RETENTION OF SPEC_STRONG BY TISSUE
Tissue_Dre	Number_of_Genes
Eggs_A	12
Embr_26hpf_H	8
Testis_b	24
Ovary_A	3
Skin	4
Intestine_c	10
Liver_a	9
Muscle_A	15
Brain_a	83

SUMMARY OF HISTOGRAM ANALYSIS
Bla vs Dre (Normalized and OR); Min_expr: 5 and 5
LIST	HIGHER_Bla	HIGHER_Dre	EQUAL
OHNO	2475	339	613
OHNO_fam	374	308	453
OHNO_famS	234	401	501
ONE	821	848	809

DATA FOR HISTOGRAMS
GROUP	1-to-1	OHNO	OHNO_fam	OHNO_famS
-8	21	245	8	6
-7	21	245	7	5
-6	51	300	19	10
-5	73	288	39	17
-4	82	281	43	32
-3	134	284	56	33
-2	168	330	84	45
-1	268	389	117	86
0	809	613	453	501
1	316	184	137	159
2	181	68	67	85
3	124	38	36	52
4	95	26	27	37
5	64	11	13	26
6	34	8	12	16
7	24	2	7	8
8	9	1	1	7
```

## perl Analyze_Red_Subf_Spec.pl Xtr

```
GENERAL SUMMARY OF ANALYSIS (Bla vs Xtr)
Starting Ohno	1509
Ohno after size filter	1097
Ohno in final loop subf	1094
Ohno valid for subf	360

COMPARISON OF SUBF vs SPEC vs REDUNDANCY
Total subfunctionalized families	66
Total strongly specialized families	66
Total mildy specialized families	157
Total redundant families	71

RETENTION OF SPEC_STRONG BY TISSUE
Tissue_Xtr	Number_of_Genes
Oocyte_a	5
Embr_Stage24_26_a	18
Testis_b1	13
Ovary_b1	4
Skin_Pub	4
Intestine_Solid	6
Liver_a	4
Muscle	15
Brain_a	34

SUMMARY OF HISTOGRAM ANALYSIS
Bla vs Xtr (Normalized and OR); Min_expr: 5 and 5
LIST	HIGHER_Bla	HIGHER_Xtr	EQUAL
OHNO	1735	355	547
OHNO_fam	353	308	433
OHNO_famS	273	359	464
ONE	746	919	819

DATA FOR HISTOGRAMS
GROUP	1-to-1	OHNO	OHNO_fam	OHNO_famS
-8	8	105	2	2
-7	26	159	8	6
-6	35	170	25	15
-5	52	205	28	19
-4	72	225	33	23
-3	95	211	60	42
-2	176	263	83	55
-1	280	336	114	111
0	819	547	433	464
1	301	172	120	139
2	172	77	73	78
3	142	32	28	35
4	107	35	37	41
5	84	14	13	18
6	60	13	13	16
7	30	5	11	13
8	20	6	4	9
```