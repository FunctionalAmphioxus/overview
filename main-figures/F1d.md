```python
import pandas as pd
from pybedtools import BedTool as BT
import numpy as np
# from glob import glob
# import pysam

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt

from collections import Counter
```

# Load some data:


```python
# lists of genes from our homologous gene families file
genefamdf = pd.read_csv("../data/gene_families_table.tsv.gz", sep='\t')

good_amphi_genes = [x for y in genefamdf['Bla'].dropna().apply(lambda x: x.split(':') if ':' in x else [x]).values for x in y]
good_zebra_genes = [x for y in genefamdf['Dre'].dropna().apply(lambda x: x.split(':') if ':' in x else [x]).values for x in y]
good_medaka_genes = [x for y in genefamdf['Ola'].dropna().apply(lambda x: x.split(':') if ':' in x else [x]).values for x in y]
good_mouse_genes = [x for y in genefamdf['Mmu'].dropna().apply(lambda x: x.split(':') if ':' in x else [x]).values for x in y]
```


```python
len(good_amphi_genes),len(good_zebra_genes),len(good_medaka_genes),len(good_mouse_genes),
```




    (20569, 20082, 15978, 19429)




```python
amphi_tss_df = pd.read_csv("../data/genomic_regions/TSS_bla.bed", sep='\t')
amphi_tss_df['score'] = 0
amphi_tss_df = amphi_tss_df[['chrom','start','end','gene_id','score','strand']]
amphi_tss_df = amphi_tss_df[amphi_tss_df.gene_id.isin(good_amphi_genes)]
amphi_tss_bed = BT().from_dataframe(amphi_tss_df)
amphi_tss_df.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>gene_id</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Sc0000000</td>
      <td>25509</td>
      <td>25510</td>
      <td>BL09450</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Sc0000000</td>
      <td>169904</td>
      <td>169905</td>
      <td>BL10006</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Sc0000000</td>
      <td>372950</td>
      <td>372951</td>
      <td>BL73495</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>10</th>
      <td>Sc0000000</td>
      <td>390779</td>
      <td>390780</td>
      <td>BL03349</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>14</th>
      <td>Sc0000000</td>
      <td>463399</td>
      <td>463400</td>
      <td>BL11263</td>
      <td>0</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
zebra_tss_df = pd.read_csv("../data/genomic_regions/TSS_dre.bed", sep='\t', header=None)
zebra_tss_df.columns = ['chrom','start','end','gene_id','score','strand']
zebra_tss_df = zebra_tss_df[zebra_tss_df.gene_id.isin(good_zebra_genes)].copy()
zebra_tss_bed = BT().from_dataframe(zebra_tss_df)

zebra_tss_df.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>gene_id</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MT</td>
      <td>3803</td>
      <td>3804</td>
      <td>ENSDARG00000063895</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MT</td>
      <td>4993</td>
      <td>4994</td>
      <td>ENSDARG00000063899</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MT</td>
      <td>6425</td>
      <td>6426</td>
      <td>ENSDARG00000063905</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MT</td>
      <td>8130</td>
      <td>8131</td>
      <td>ENSDARG00000063908</td>
      <td>0</td>
      <td>+</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MT</td>
      <td>8894</td>
      <td>8895</td>
      <td>ENSDARG00000063910</td>
      <td>0</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
medaka_tss_df = pd.read_csv("../data/genomic_regions/TSS_ola.bed", sep='\t', header=None)
medaka_tss_df.columns = ['chrom','start','end','gene_id','score','strand']
medaka_tss_df = medaka_tss_df[medaka_tss_df.gene_id.isin(good_medaka_genes)].copy()
medaka_tss_bed = BT().from_dataframe(medaka_tss_df)
medaka_tss_df.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>gene_id</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>13</th>
      <td>chr1</td>
      <td>167143</td>
      <td>167144</td>
      <td>ENSORLG00000000003</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>15</th>
      <td>chr1</td>
      <td>184640</td>
      <td>184641</td>
      <td>ENSORLG00000000007</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>16</th>
      <td>chr1</td>
      <td>231082</td>
      <td>231083</td>
      <td>ENSORLG00000000009</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>17</th>
      <td>chr1</td>
      <td>246352</td>
      <td>246353</td>
      <td>ENSORLG00000000013</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>19</th>
      <td>chr1</td>
      <td>283183</td>
      <td>283184</td>
      <td>ENSORLG00000000016</td>
      <td>0</td>
      <td>-</td>
    </tr>
  </tbody>
</table>
</div>




```python
mouse_tss_df = pd.read_csv("../data/genomic_regions/TSS_mmu.bed", sep='\t', header=None)
mouse_tss_df.columns = ['chrom','start','end','gene_id','score','strand']
mouse_tss_df = mouse_tss_df[mouse_tss_df.gene_id.isin(good_mouse_genes)].copy()

mouse_tss_bed = BT().from_dataframe(mouse_tss_df)

mouse_tss_df.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>gene_id</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>chr1</td>
      <td>3671498</td>
      <td>3671499</td>
      <td>ENSMUSG00000051951</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>1</th>
      <td>chr1</td>
      <td>4360314</td>
      <td>4360315</td>
      <td>ENSMUSG00000025900</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>2</th>
      <td>chr1</td>
      <td>4496413</td>
      <td>4496414</td>
      <td>ENSMUSG00000025902</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>3</th>
      <td>chr1</td>
      <td>4785710</td>
      <td>4785711</td>
      <td>ENSMUSG00000033845</td>
      <td>0</td>
      <td>-</td>
    </tr>
    <tr>
      <th>4</th>
      <td>chr1</td>
      <td>4807823</td>
      <td>4807824</td>
      <td>ENSMUSG00000025903</td>
      <td>0</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
a = pd.read_csv("../data/genomic_regions/intergenics_Bla.tsv.gz",
           sep='\t', header=None)
amphi_aid = (a[2]-a[1]).mean()

a = pd.read_csv("../data/genomic_regions/intergenics_Dre.tsv.gz",
           sep='\t', header=None)
zebra_aid = (a[2]-a[1]).mean()

a = pd.read_csv("../data/genomic_regions/intergenics_Ola.tsv.gz",
           sep='\t', header=None)
medaka_aid = (a[2]-a[1]).mean()

a = pd.read_csv("../data/genomic_regions/intergenics_Mmu.tsv.gz",
           sep='\t', header=None)
mouse_aid = (a[2]-a[1]).mean()
```


```python
# the average intergenic regions, we'll use these later to normalize
amphi_aid, zebra_aid, medaka_aid, mouse_aid
```




    (22280.674919495326,
     67502.639512022986,
     48814.903121566596,
     136213.51419155949)




```python
amphi_tss_df.shape, zebra_tss_df.shape, medaka_tss_df.shape, mouse_tss_df.shape
```




    ((20569, 6), (20053, 6), (15978, 6), (18842, 6))



### In the following cells:

The species_APs_ii are ATAC peaks found only inside the intergenic regions,
This way we can investigate their distance to a gene and make sure
that those distances are not affected by the fragmenttion of the genome.    
    
species_closest then is a 


```python
amphi_APs = BT("../data/atac_peaks/amphi_merged_idrpeaks.bed")
amphi_intergenics = BT("../data/genomic_regions/intergenics_Bla.tsv.gz")

amphi_APs_ii = amphi_APs.intersect(amphi_intergenics, u=True)
amphi_closest = amphi_APs.closest(b=amphi_tss_bed, D='b', io=True, t='first').to_dataframe()
amphi_ii_closest = amphi_APs_ii.closest(b=amphi_tss_bed, D='b', io=True, t='first').to_dataframe()
```


```python
zebra_APs = BT("../data/atac_peaks/zebra_danRer10_merged_idrpeaks.bed.gz")
zebra_intergenics = BT("../data/genomic_regions/intergenics_Dre.tsv.gz")

zebra_closest = zebra_APs.closest(b=zebra_tss_bed, D='b', io=True, t='first', nonamecheck=True).to_dataframe()
zebra_APs_ii = zebra_APs.intersect(zebra_intergenics, u=True, nonamecheck=True)
zebra_ii_closest = zebra_APs_ii.closest(b=zebra_tss_bed, D='b', io=True, t='first', nonamecheck=True).to_dataframe()
```


```python
medaka_APs = BT("../data/atac_peaks/medaka_merged_idrpeaks.bed.gz")
medaka_intergenics = BT("../data/genomic_regions/intergenics_Ola.tsv.gz")

medaka_closest = medaka_APs.closest(b=medaka_tss_bed, D='b', io=True, t='first',nonamecheck=True).to_dataframe()
medaka_APs_ii = medaka_APs.intersect(medaka_intergenics, u=True, nonamecheck=True)
medaka_ii_closest = medaka_APs_ii.closest(b=medaka_tss_bed, D='b', io=True, t='first', nonamecheck=True).to_dataframe()
```


```python
mouse_APs = BT().from_dataframe(pd.read_csv("../data/atac_peaks/mouse_ESC_idrpeaks.bed.gz",
           sep='\t', header=None)[[0,1,2]]).sort()
mouse_intergenics = BT("../data/genomic_regions/intergenics_Mmu.tsv.gz").sort()

mouse_closest = mouse_APs.closest(b=mouse_tss_bed, D='b', io=True, t='first').to_dataframe()
mouse_APs_ii = mouse_APs.intersect(mouse_intergenics, u=True, nonamecheck=True)
mouse_ii_closest = mouse_APs_ii.closest(b=mouse_tss_bed, D='b', io=True, t='first', nonamecheck=True).to_dataframe()
```

Some peaks will not be assigned a "closest" TSS, for example if there no TSS    
on the scaffold where the peak was found. We want to get rid of those and we do it by filtering
the rows where 'thickStart'=='.' (those are the rows with no assignment)


```python
amphi_closest = amphi_closest[amphi_closest.thickStart!='.']
amphi_ii_closest = amphi_ii_closest[amphi_ii_closest.thickStart!='.']

zebra_closest = zebra_closest[zebra_closest.thickStart!='.']
zebra_ii_closest = zebra_ii_closest[zebra_ii_closest.thickStart!='.']

medaka_closest = medaka_closest[medaka_closest.thickStart!='.']
medaka_ii_closest = medaka_ii_closest[medaka_ii_closest.thickStart!='.']

mouse_closest = mouse_closest[mouse_closest.thickStart!='.']
mouse_ii_closest = mouse_ii_closest[mouse_ii_closest.thickStart!='.']
```


```python
# Make the distances absolute:
amphi_closest['absdist'] = amphi_closest.blockCount.abs()
zebra_closest['absdist'] = zebra_closest.blockCount.abs()
medaka_closest['absdist'] = medaka_closest.blockCount.abs()
mouse_closest['absdist'] = mouse_closest.blockCount.abs()


amphi_ii_closest['absdist'] = amphi_ii_closest.blockCount.abs()
zebra_ii_closest['absdist'] = zebra_ii_closest.blockCount.abs()
medaka_ii_closest['absdist'] = medaka_ii_closest.blockCount.abs()
mouse_ii_closest['absdist'] = mouse_ii_closest.blockCount.abs()
```

## Now we manipulate a bit in order to get the data in plotting order


```python
# we order our peaks by distance 
ac = amphi_closest.sort_values(by='absdist').copy()

# Then we divide the rank of each peak by the number of peaks
# this value then becomes: "what % of peaks are above me in the table?"
ac['EDCV'] = np.arange(len(ac))/len(ac)

# make a normalized absolute distance:
ac['absdist_normed'] = ac.absdist/amphi_aid

ac.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>thickStart</th>
      <th>thickEnd</th>
      <th>itemRgb</th>
      <th>blockCount</th>
      <th>absdist</th>
      <th>EDCV</th>
      <th>absdist_normed</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>36074</th>
      <td>Sc0000043</td>
      <td>546364</td>
      <td>546830</td>
      <td>Sc0000043</td>
      <td>546830</td>
      <td>546831</td>
      <td>BL04095</td>
      <td>0</td>
      <td>-</td>
      <td>1</td>
      <td>1</td>
      <td>0.000000</td>
      <td>0.000045</td>
    </tr>
    <tr>
      <th>58074</th>
      <td>Sc0000135</td>
      <td>61630</td>
      <td>61808</td>
      <td>Sc0000135</td>
      <td>61808</td>
      <td>61809</td>
      <td>BL09036</td>
      <td>0</td>
      <td>-</td>
      <td>1</td>
      <td>1</td>
      <td>0.000012</td>
      <td>0.000045</td>
    </tr>
    <tr>
      <th>13908</th>
      <td>Sc0000007</td>
      <td>6635417</td>
      <td>6635764</td>
      <td>Sc0000007</td>
      <td>6635416</td>
      <td>6635417</td>
      <td>BL72351</td>
      <td>0</td>
      <td>+</td>
      <td>1</td>
      <td>1</td>
      <td>0.000024</td>
      <td>0.000045</td>
    </tr>
    <tr>
      <th>22904</th>
      <td>Sc0000016</td>
      <td>3049350</td>
      <td>3049805</td>
      <td>Sc0000016</td>
      <td>3049805</td>
      <td>3049806</td>
      <td>BL06190</td>
      <td>0</td>
      <td>-</td>
      <td>1</td>
      <td>1</td>
      <td>0.000036</td>
      <td>0.000045</td>
    </tr>
    <tr>
      <th>34343</th>
      <td>Sc0000039</td>
      <td>263674</td>
      <td>264005</td>
      <td>Sc0000039</td>
      <td>263673</td>
      <td>263674</td>
      <td>BL02587</td>
      <td>0</td>
      <td>+</td>
      <td>1</td>
      <td>1</td>
      <td>0.000048</td>
      <td>0.000045</td>
    </tr>
  </tbody>
</table>
</div>




```python
# to-plot object:
# we drop the duplicates of 'absdist_normed' and keep only the last instance of each
# the 'EDCV' value will be our Y acis values with the absdist_normed value on the X axis
tp_ac = (ac
        .drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']]
        .to_records( index=False))
```


```python
tp_ac[:3]
```




    rec.array([(  4.48819438e-05,  0.00028753),
               (  8.97638876e-05,  0.00049119), (  1.34645831e-04,  0.00079069)], 
              dtype=[('absdist_normed', '<f8'), ('EDCV', '<f8')])




```python
# repeat for the other species
zc = zebra_closest.sort_values(by='absdist').copy()
zc['EDCV'] = np.arange(len(zc))/len(zc)
zc['absdist_normed'] = zc.absdist/zebra_aid

tp_zc = zc.drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)

medc = medaka_closest.sort_values(by='absdist').copy()
medc['EDCV'] = np.arange(len(medc))/len(medc)
medc['absdist_normed'] = medc.absdist/medaka_aid

tp_medc = medc.drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)

mmuc = mouse_closest.sort_values(by='absdist').copy()
mmuc['EDCV'] = np.arange(len(mmuc))/len(mmuc)
mmuc['absdist_normed'] = mmuc.absdist/mouse_aid

tp_mmuc = mmuc[mmuc.chrom !='chrY'].drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)
```


```python
len(tp_ac),len(tp_zc),len(tp_medc),len(tp_mmuc)
```




    (29738, 111926, 75342, 130958)




```python
plt.figure(figsize = (16,9))


x,y = list(zip(*tp_ac))
plt.plot(x,y, label='amphi', linewidth=4)

x,y = list(zip(*tp_zc))
plt.plot(x,y, label='zebra', linewidth=4, alpha=1, )

x,y = list(zip(*tp_medc))
plt.plot(x,y, label='medaka', linewidth=4)

x,y = list(zip(*tp_mmuc))
plt.plot(x,y, label='mouse', linewidth=1, alpha=1, linestyle='--' , color='black')


plt.legend()

plt.ylabel(("ratio of ATAC peaks that are found at equal or closer distance from a TSS"))
plt.xlabel(("Genomic Length in units of 'average intergenic distance'"))

# plt.xscale('log')
plt.xlim((-0.05,4))
```




    (-0.05, 4)




![png](main-figures/img/f1d_output25_1.png)


### Now we'll do the same but without normalizing the distances


```python
# we order our peaks by distance 
ac = amphi_closest.sort_values(by='absdist').copy()
# Then we divide the rank of each peak by the number of peaks
# this value then becomes: "what % of peaks are above me in the table?"
ac['EDCV'] = np.arange(len(ac))/len(ac)

# to-plot object:
# we drop the duplicates of 'absdist'
tp_ac = (ac
        .drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']]
        .to_records( index=False))

# repeat for the other species
zc = zebra_closest.sort_values(by='absdist').copy()
zc['EDCV'] = np.arange(len(zc))/len(zc)

tp_zc = zc.drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)

medc = medaka_closest.sort_values(by='absdist').copy()
medc['EDCV'] = np.arange(len(medc))/len(medc)

tp_medc = medc.drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)

mmuc = mouse_closest.sort_values(by='absdist').copy()
mmuc['EDCV'] = np.arange(len(mmuc))/len(mmuc)

tp_mmuc = mmuc[mmuc.chrom !='chrY'].drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)
```


```python
plt.figure(figsize = (16,9))


x,y = list(zip(*tp_ac))
plt.plot(x,y, label='amphi', linewidth=4)

x,y = list(zip(*tp_zc))
plt.plot(x,y, label='zebra', linewidth=4, alpha=1, )

x,y = list(zip(*tp_medc))
plt.plot(x,y, label='medaka', linewidth=4)

x,y = list(zip(*tp_mmuc))
plt.plot(x,y, label='mouse', linewidth=1, alpha=1, linestyle='--' , color='black')


plt.legend()

plt.ylabel(("ratio of ATAC peaks that are found at equal or closer distance from a TSS"))
plt.xlabel("Genomic Length (bp)")
plt.xscale('log')
```


![png](main-figures/img/f1d_output28_0.png)


### The same for peaks inside intergenic regions:


```python
# we order our peaks by distance 
ac = amphi_ii_closest.sort_values(by='absdist').copy()
# Then we divide the rank of each peak by the number of peaks
# this value then becomes: "what % of peaks are above me in the table?"
ac['EDCV'] = np.arange(len(ac))/len(ac)
# make a normalized absolute distance:
ac['absdist_normed'] = ac.absdist/amphi_aid
# to-plot object:
# we drop the duplicates of 'absdist_normed'
tp_ac = (ac
        .drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']]
        .to_records( index=False))

# repeat for the other species
zc = zebra_ii_closest.sort_values(by='absdist').copy()
zc['EDCV'] = np.arange(len(zc))/len(zc)
zc['absdist_normed'] = zc.absdist/zebra_aid

tp_zc = zc.drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)

medc = medaka_ii_closest.sort_values(by='absdist').copy()
medc['EDCV'] = np.arange(len(medc))/len(medc)
medc['absdist_normed'] = medc.absdist/medaka_aid

tp_medc = medc.drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)

mmuc = mouse_ii_closest.sort_values(by='absdist').copy()
mmuc['EDCV'] = np.arange(len(mmuc))/len(mmuc)
mmuc['absdist_normed'] = mmuc.absdist/mouse_aid

tp_mmuc = mmuc[mmuc.chrom !='chrY'].drop_duplicates('absdist_normed', keep='last')[['absdist_normed', 'EDCV']].to_records( index=False)
```


```python
plt.figure(figsize = (16,9))


x,y = list(zip(*tp_ac))
plt.plot(x,y, label='amphi', linewidth=4)

x,y = list(zip(*tp_zc))
plt.plot(x,y, label='zebra', linewidth=4, alpha=1, )

x,y = list(zip(*tp_medc))
plt.plot(x,y, label='medaka', linewidth=4)

x,y = list(zip(*tp_mmuc))
plt.plot(x,y, label='mouse', linewidth=1, alpha=1, linestyle='--' , color='black')


plt.legend()

plt.ylabel(("ratio of ATAC peaks that are found at equal or closer distance from a TSS"))
plt.xlabel(("Genomic Length in units of 'average intergenic distance'"))

plt.xlim((-0.05,4))
```




    (-0.05, 4)




![png](main-figures/img/f1d_output31_1.png)


### Again, with intergenic-only peaks and non-normalized distance:


```python
# we order our peaks by distance 
ac = amphi_ii_closest.sort_values(by='absdist').copy()
# Then we divide the rank of each peak by the number of peaks
# this value then becomes: "what % of peaks are above me in the table?"
ac['EDCV'] = np.arange(len(ac))/len(ac)

# to-plot object:
# we drop the duplicates of 'absdist'
tp_ac = (ac
        .drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']]
        .to_records( index=False))

# repeat for the other species
zc = zebra_ii_closest.sort_values(by='absdist').copy()
zc['EDCV'] = np.arange(len(zc))/len(zc)

tp_zc = zc.drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)

medc = medaka_ii_closest.sort_values(by='absdist').copy()
medc['EDCV'] = np.arange(len(medc))/len(medc)

tp_medc = medc.drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)

mmuc = mouse_ii_closest.sort_values(by='absdist').copy()
mmuc['EDCV'] = np.arange(len(mmuc))/len(mmuc)

tp_mmuc = mmuc[mmuc.chrom !='chrY'].drop_duplicates('absdist', keep='last')[['absdist', 'EDCV']].to_records( index=False)
```


```python

len(tp_ac),len(tp_zc),len(tp_medc),len(tp_mmuc)
# we don't lose many datapoints, the previous counts were:
# (29738, 111926, 75342, 130958)
```




    (27872, 111541, 73355, 130495)




```python
plt.figure(figsize = (16,9))


x,y = list(zip(*tp_ac))
plt.plot(x,y, label='amphi', linewidth=4)

x,y = list(zip(*tp_zc))
plt.plot(x,y, label='zebra', linewidth=4, alpha=1, )

x,y = list(zip(*tp_medc))
plt.plot(x,y, label='medaka', linewidth=4)

x,y = list(zip(*tp_mmuc))
plt.plot(x,y, label='mouse', linewidth=1, alpha=1, linestyle='--' , color='black')


plt.legend()

plt.ylabel(("ratio of ATAC peaks that are found at equal or closer distance from a TSS"))
plt.xlabel("Genomic Length (bp)")

plt.xscale('log')
```


![png](main-figures/img/f1d_output35_0.png)