
## Gene modules comparisons

## Needed data:
* ### Assignment of genes to modules (WGCNA / MFUZZ)    

We use WGCNA to cluster genes by their RNA activity in various TRNAseq experiments, both in developmental stages and pure tissue samples. This analysis assigns each gene in a single cluster. These clusters have names like "blue". "green" etc.
We manually explore the clusters, and by determining at which experiment the genes of the cluster seem to be differentially expressed (but also by GO analysis on each cluster), we assign more meaningful names to the clusters , like "liver", "muscle" etc. The translating dictionaries are in the end of the notebook.

* ### Assignment of genome regions to genes 
    
We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream,
or untill it encounters another TSS.
    
* ### TF Motifs mapped in the genome
* ### Grouping of genes into homology-families       


```python
import gzip
import os
import pysam

from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
sns.set_style('white')
from matplotlib import pyplot as plt

import pandas as pd
import numpy as np

from collections import Counter
from functools import reduce
from operator import add


from scipy.stats import hypergeom
statf = hypergeom.sf
from math import log

from scipy.stats import pearsonr
import scipy.spatial as sp, scipy.cluster.hierarchy as hc

from sklearn.preprocessing import scale, normalize, robust_scale
from sklearn.metrics.pairwise import pairwise_distances as PWD


import warnings
warnings.filterwarnings('ignore')
```

## Preprocessing:


```python
# Here we load the mapping of genes to modules from csv
# into dictionaries:
dan_cl = pd.read_csv("../data/wgcna_results/wgcna_dre_modulekey.tab.gz", sep='\t')
dan_cl.columns = ['geneID','clusterID','updown']
dan_cl_d = dict(dan_cl[['geneID','clusterID']].to_records(index=False))
bla_cl = pd.read_csv("../data/wgcna_results/wgcna_bla_modulekey.tab.gz", sep='\t')
bla_cl.columns = ['geneID','clusterID','updown']
bla_cl_d = dict(bla_cl[['geneID','clusterID']].to_records(index=False))

# bla_cl_d ~= {'BL06687': 'greenyellow',
#  'BL12105': 'greenyellow',
#  'BL18033': 'magenta',....}
```


```python
# we load the gene family bindings into a dictionary where 
# key = geneID
# value =  unique gene family number (some integer)
gfams = (pd
            .read_csv("../data/gene_families_table.tsv.gz",
                     sep='\t')
            .applymap(lambda c: c.split(':') if c==c else []))

gfamsC = gfams.applymap(lambda c: len(c))

gfamsD = {}
for rowi, row in gfams.iterrows():
    for species in ['Bla','Dre','Mmu','Hsa','Dme']:
        for gene in row[species]:
            gfamsD[gene] = int(rowi)
fam_of = {}
for rowi,row in gfams[['Bla']].dropna().iterrows():
    for gene in row.Bla:
        fam_of[gene]= rowi
for rowi,row in gfams[['Dre']].dropna().iterrows():
    for gene in row.Dre:
        fam_of[gene]= rowi
# fam_of maps all genes to their gene family ID
```


```python
### translate the uninformative color names to manually made informative names for each module
### Cleaner versions of these dictionaries at the bottom of the notebook
transA = {'black' : '32 cells','blue' : 'N.tube(neurotrans.)','brown' : 'Gills','cyan' : 'N.tube(neurogen.)','darkmagenta' : 'Ovary/ Testis(translation)','darkorange' : 'Cilium','darkred' : 'Muscle','darkseagreen4' : '8-36hpf embryo/ Skin/ Cirri(memb. synt.)','darkslateblue' : '8hpf embryo(transcription, spliceosome)','darkturquoise' : '36hpf embryo','green' : 'Eggs/ 32 cells(cell cycle)','greenyellow' : 'Hepatic(lipid catabolism)','lavenderblush3' : 'Cirri / PreMet. / Muscle - actomyosin','lightpink4' : '15hpf embryo','magenta' : 'Skin','navajowhite2' : 'Immune','palevioletred3' : 'Ovary/ Eggs','pink' : 'Gut','plum1' : 'Gut/ Hepatic','plum2' : 'Proteasome','red' : 'PreMet.larvae','salmon' : 'Cirri(memb. synt.)','sienna3' : 'Hepatic','thistle2' : 'Gills/ PreMet larvae',
'turquoise' : 'Mitochondrion'}
transZ = {'bisque4' : "larvae 7d melanin",'black' : "Ovary/ Sperm",'blue' : "Brain",'brown' : "Cilium",'brown4' : "Liver(carboxi.met./ lipid trans)",'coral2' : "Mitochondrion",'darkgreen' : "Intestine",'darkgrey' : "RNA, ribosome, proteasome",'darkmagenta' : "Translation, ribosome, RNA bin.",'darkorange' : "Muscle",'darkred' : "Liver(oxi-red.proc./ hemostasis)",'darkseagreen4' : "12-26hpf embryo/ Skin",'darkslateblue' : "Immune",'green' : "Spliceosome",'honeydew1' : "12-26hpf embryo",'ivory' : "Eye",'lightcyan' : "Heart",'lightgreen' : "Pancreas/ Testis",'magenta' : "Gills",'pink' : "Skin",
    'salmon' : "20-26hpf embryo",'yellow4' : "Kidney",
'yellowgreen' : "Gills/ Skin"}
```


```python
# We compute the upper tail of the hypergeometric distribution
# (survival function) for each pair of modules as a metric of enrichment

gimme_fams = lambda g: [fam_of.get(x) for x in g.geneID.tolist()]
# Population size:
gene_POP = len(gfams)

PVS = pd.DataFrame()
# for each module in zebra
for gn,g  in dan_cl.groupby('clusterID'):
    # for each module in amphioxus:
    for bgn,bg  in bla_cl.groupby('clusterID'):
        
        fams = set(gimme_fams(g))
        bfams = set(gimme_fams(bg))
        
        gene_SS = len(fams) # Sample Size
        gene_SIP = len(bfams) # Success in Population
        gene_SIS = len(bfams.intersection(fams)) # success in sample
        
        if gene_SIS>0:
            # statf is the survival function of the hypergeometric distribution            
            fish = statf(gene_SIS,  gene_POP,  gene_SS, gene_SIP)
        else:
            fish =1 
            
        PVS.loc[transZ[gn], transA[bgn]] = fish
        
# the translating dictionaries are at the end of the notebook
```

## The gene-based heatmap


```python
# we get the -log transformation of the pvalues, clip them to a workable range
mPVS = PVS.applymap(lambda x: -log(x,10) ).copy().clip(upper=25,lower=0)

# We will compute the clustering outselves in order to be able to re-use it,
# and to have better control
# we cluster by row and by column, based on euclidean distance and complete method:
linkage_HG_rows = hc.linkage(sp.distance.squareform(PWD(  mPVS.values, metric='euclidean'), checks=False), method='complete')
linkage_HG_cols = hc.linkage(sp.distance.squareform(PWD(  mPVS.T.values, metric='euclidean'), checks=False), method='complete')

# Calculating the clustering like this, allows us to use the same clustering later on
```


```python
# Finally we plot this as a clustered heatmap
geneHM = sns.clustermap( mPVS,
                      row_linkage = linkage_HG_rows,
                      col_linkage = linkage_HG_cols,
                      linewidths=0.01,
                      figsize =(12,12),
                      cmap='Reds',
                      vmax=25, vmin=5

                      )

_ = plt.setp(geneHM.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
plt.title('Enrichment for orthologous genes')
```




    Text(0.5,1,'Enrichment for orthologous genes')




![png](main-figures/img/f4bcf_output_9_1.png)


## We now want to map PWMs to modules

To do this, we will use the BASAL region of genes:


```python
# gene to "gene region" connection
# This dataframe connects each gene to a genomic region:
dan_greg = pd.read_csv("../data/genomic_regions/BASAL_dre.bed.gz", sep='\t', header=None)
dan_greg.columns = ['chrom','start','end','geneID','strand']
dan_greg['score'] = 0
dan_greg = dan_greg[['chrom','start','end','geneID','score','strand']]

bla_greg = pd.read_csv("../data/genomic_regions/BASAL_bla.bed.gz", sep='\t', header=None)
bla_greg.columns = ['chrom','start','end','geneID','strand']
bla_greg['score'] = 0
bla_greg = bla_greg[['chrom','start','end','geneID','score','strand']]
```

and of course the PWMs that have been mapped on ATACseq peaks already:


```python
# load the PWM hits in a pybedtools object and sort
dan_motif_bed = BT("../data/danre_pwm_hits.bed.gz").sort()
bla_motif_bed = BT("../data/bralan_pwm_hits.bed.gz").sort()

amphi_stages = ['8','15','36','60','hep']
zebra_stages = ['dome','shield','80epi','8som','24h','48h']
```

We also need the following table, which assigns a unique id number to each PWM.


```python
# busy-work to assign unique IDs to each PWM family
superfams = pd.read_csv("../data/PWMname_to_ProteinFamily.tsv.gz", sep='\t', header=None)
def order_things(x):
    return ';'.join(sorted(x.split(';')))
lot = []
for i,row in superfams.iterrows():
    if ';' not in row[1]:
        lot.append( row.tolist() )
    else:
        a,b = [';'.join(jj) for jj in zip(*sorted(zip(row[1].split(';'), row[2].split(';')), key= lambda x: int(x[0]) ))]
        lot.append( [row[0], a, b] )
superfams_ = pd.DataFrame(lot)
SFD = dict(superfams_[[0,1]].to_records(index=False))

fuid = {}
c = 0
for v in SFD.values():
    if v not in fuid:
        fuid[v] = c
        c += 1
SFDu = {k:fuid[v] for k,v in SFD.items()}

superfams_['u'] = superfams_[0].map(SFDu)
SF = superfams_.set_index('u')
```

We will now compute the LOJ intersection of the PMs to the BASAL regions,
For each gene this will give us all PWMs in its "regulatory landscape"


```python
# Map the cluster ID to gene IDs:
dan_greg['cluster'] = dan_greg.geneID.map(dan_cl_d)
dan_greg = dan_greg[~dan_greg.cluster.isnull()]

bla_greg['cluster'] = bla_greg.geneID.map(bla_cl_d)
bla_greg = bla_greg[~bla_greg.cluster.isnull()]

# cast the gene region dataframes into pybedtools objects
# and intersect with motifs (with LEFT OUTER JOIN)
dan_loj = (BT()
        .from_dataframe(dan_greg[['chrom','start','end','cluster','score','strand']])
        .sort()
        .intersect(dan_motif_bed, loj=True, nonamecheck=True, sorted=True)
        ).to_dataframe()[['chrom','start','end','name','score','strand','blockCount']]

dan_loj = dan_loj[dan_loj.blockCount != '.']
dan_loj['fam'] = dan_loj.blockCount.map(SFDu)
dan_loj = dan_loj[~dan_loj['fam'].isnull()]
dan_loj.fam = dan_loj.fam.astype(int)

bla_loj = (BT()
        .from_dataframe(bla_greg[['chrom','start','end','cluster','score','strand']])
        .sort()
        .intersect(bla_motif_bed, loj=True, nonamecheck=True, sorted=True)
        ).to_dataframe()[['chrom','start','end','name','score','strand','blockCount']]

bla_loj = bla_loj[bla_loj.blockCount != '.']
bla_loj['fam'] = bla_loj.blockCount.map(SFDu)
bla_loj = bla_loj[~bla_loj['fam'].isnull()]
dan_loj.fam = dan_loj.fam.astype(int)
bla_loj.head()

# we had 
# gene_module --> genes
# genes --> genomic_regions
# motifs --> genomic_positions

# and we managed to connect them:
# module/cluster --> genes -> genomic regions --> motifs
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>blockCount</th>
      <th>fam</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>C2H2_ZF_Average_244</td>
      <td>11.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>ARID_BRIGHT_RFX_M4343_1.02</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>RFX_Average_38</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>bZIP_Average_125</td>
      <td>236.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>C2H2_ZF_M6539_1.02</td>
      <td>21.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# convert the left outer join dataframes into
# tables that contain a count per TF per stage:
bla_loC = [Counter(g.fam) for gn,g in bla_loj.groupby("name")]
bla_temp = pd.DataFrame(bla_loC)   
bla_temp = bla_temp.fillna(0)   
clustorder = [transA[gn] for gn,g in bla_loj.groupby("name")]

bla_temp.index = clustorder
bla_table = bla_temp.copy()

dan_loC = [Counter(g.fam) for gn,g in dan_loj.groupby("name")]
dan_temp = pd.DataFrame(dan_loC)   
dan_temp = dan_temp.fillna(0)   
clustorder = [transZ[gn] for gn,g in dan_loj.groupby("name")]

dan_temp.index = clustorder
dan_table = dan_temp.copy()
dan_table.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>...</th>
      <th>247</th>
      <th>248</th>
      <th>249</th>
      <th>250</th>
      <th>251</th>
      <th>252</th>
      <th>253</th>
      <th>254</th>
      <th>255</th>
      <th>256</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>larvae 7d melanin</th>
      <td>223</td>
      <td>537</td>
      <td>14.0</td>
      <td>71</td>
      <td>21</td>
      <td>114</td>
      <td>18</td>
      <td>13.0</td>
      <td>69</td>
      <td>6</td>
      <td>...</td>
      <td>14</td>
      <td>24</td>
      <td>15</td>
      <td>11</td>
      <td>10</td>
      <td>53</td>
      <td>10</td>
      <td>50</td>
      <td>53</td>
      <td>52</td>
    </tr>
    <tr>
      <th>Ovary/ Sperm</th>
      <td>368</td>
      <td>2109</td>
      <td>52.0</td>
      <td>239</td>
      <td>66</td>
      <td>204</td>
      <td>39</td>
      <td>29.0</td>
      <td>182</td>
      <td>33</td>
      <td>...</td>
      <td>59</td>
      <td>53</td>
      <td>67</td>
      <td>48</td>
      <td>38</td>
      <td>148</td>
      <td>30</td>
      <td>103</td>
      <td>116</td>
      <td>162</td>
    </tr>
    <tr>
      <th>Brain</th>
      <td>1714</td>
      <td>5518</td>
      <td>129.0</td>
      <td>953</td>
      <td>241</td>
      <td>1183</td>
      <td>185</td>
      <td>101.0</td>
      <td>549</td>
      <td>70</td>
      <td>...</td>
      <td>122</td>
      <td>218</td>
      <td>215</td>
      <td>140</td>
      <td>132</td>
      <td>457</td>
      <td>89</td>
      <td>371</td>
      <td>304</td>
      <td>503</td>
    </tr>
    <tr>
      <th>Cilium</th>
      <td>531</td>
      <td>3008</td>
      <td>61.0</td>
      <td>786</td>
      <td>103</td>
      <td>334</td>
      <td>63</td>
      <td>57.0</td>
      <td>241</td>
      <td>30</td>
      <td>...</td>
      <td>69</td>
      <td>86</td>
      <td>105</td>
      <td>82</td>
      <td>71</td>
      <td>166</td>
      <td>57</td>
      <td>145</td>
      <td>176</td>
      <td>223</td>
    </tr>
    <tr>
      <th>Liver(carboxi.met./ lipid trans)</th>
      <td>29</td>
      <td>107</td>
      <td>0.0</td>
      <td>5</td>
      <td>10</td>
      <td>28</td>
      <td>2</td>
      <td>0.0</td>
      <td>6</td>
      <td>2</td>
      <td>...</td>
      <td>4</td>
      <td>6</td>
      <td>4</td>
      <td>3</td>
      <td>2</td>
      <td>7</td>
      <td>3</td>
      <td>9</td>
      <td>6</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 243 columns</p>
</div>



There might be some differences in the columns of the two tables, lets fix that:


```python
# Get a list of the TFs that are found in both species:
dan_allfams = set(dan_table.columns)
bla_allfams = set(bla_table.columns)
allfams = sorted([ int(x) for x in dan_allfams.intersection(bla_allfams)])
```


```python
len(dan_allfams), len(bla_allfams),len(allfams)
```




    (243, 242, 242)



## Normalization/Scaling


```python
plt.figure()
for rowi,row in bla_table.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module before normalization')
plt.show()

# We divide each count with the sum for each TF,
# to normalize for TF promiscuity
bla_toplot = (bla_table.loc[:,allfams]/bla_table.loc[:,allfams].sum())

plt.figure()
for rowi,row in bla_toplot.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module after normalizing the columns')
plt.show()

# then scale the rows to normalize for module size
bla_toplot.loc[:,:] = scale(bla_toplot, axis=1)

plt.figure()
for rowi,row in bla_toplot.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module after scaling the rows')
plt.show()

dan_toplot =  (dan_table.loc[:,allfams]/dan_table.loc[:,allfams].sum())
dan_toplot.loc[:,:] = scale(dan_toplot, axis=1)      
```


![png](main-figures/img/f4bcf_output_23_0.png)



![png](main-figures/img/f4bcf_output_23_1.png)



![png](main-figures/img/f4bcf_output_23_2.png)


We can now directly compare modules between the two species.     
We compute pairwise correlations next:


```python
# Get euclidean distances for each pair of modules in a table:
dists =  pd.DataFrame( PWD(dan_toplot,bla_toplot, metric="correlation") )
dists.columns = bla_toplot.index
dists.index = dan_toplot.index

dists.head(3)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>32 cells</th>
      <th>N.tube(neurotrans.)</th>
      <th>Gills</th>
      <th>N.tube(neurogen.)</th>
      <th>Ovary/ Testis(translation)</th>
      <th>Cilium</th>
      <th>Muscle</th>
      <th>8-36hpf embryo/ Skin/ Cirri(memb. synt.)</th>
      <th>8hpf embryo(transcription, spliceosome)</th>
      <th>36hpf embryo</th>
      <th>...</th>
      <th>Immune</th>
      <th>Ovary/ Eggs</th>
      <th>Gut</th>
      <th>Gut/ Hepatic</th>
      <th>Proteasome</th>
      <th>PreMet.larvae</th>
      <th>Cirri(memb. synt.)</th>
      <th>Hepatic</th>
      <th>Gills/ PreMet larvae</th>
      <th>Mitochondrion</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>larvae 7d melanin</th>
      <td>1.151380</td>
      <td>0.947201</td>
      <td>0.913505</td>
      <td>0.887989</td>
      <td>0.953334</td>
      <td>1.158688</td>
      <td>0.864133</td>
      <td>0.908501</td>
      <td>1.182973</td>
      <td>0.849711</td>
      <td>...</td>
      <td>1.094687</td>
      <td>1.124850</td>
      <td>1.108710</td>
      <td>1.062467</td>
      <td>1.005558</td>
      <td>0.941354</td>
      <td>1.047911</td>
      <td>1.116202</td>
      <td>1.108350</td>
      <td>0.949150</td>
    </tr>
    <tr>
      <th>Ovary/ Sperm</th>
      <td>0.937669</td>
      <td>1.057291</td>
      <td>1.042298</td>
      <td>1.137438</td>
      <td>1.037023</td>
      <td>0.876615</td>
      <td>1.071010</td>
      <td>0.988779</td>
      <td>0.872905</td>
      <td>1.048430</td>
      <td>...</td>
      <td>1.018499</td>
      <td>0.873161</td>
      <td>0.889911</td>
      <td>0.969410</td>
      <td>1.152619</td>
      <td>1.105972</td>
      <td>1.006390</td>
      <td>0.916814</td>
      <td>0.931047</td>
      <td>0.983802</td>
    </tr>
    <tr>
      <th>Brain</th>
      <td>1.054350</td>
      <td>0.729294</td>
      <td>0.885493</td>
      <td>0.669514</td>
      <td>1.079856</td>
      <td>1.125982</td>
      <td>0.798176</td>
      <td>1.101103</td>
      <td>1.055849</td>
      <td>0.861110</td>
      <td>...</td>
      <td>1.044327</td>
      <td>1.087449</td>
      <td>1.142921</td>
      <td>1.124648</td>
      <td>0.918456</td>
      <td>0.822229</td>
      <td>0.942510</td>
      <td>1.051548</td>
      <td>0.959279</td>
      <td>1.092851</td>
    </tr>
  </tbody>
</table>
<p>3 rows × 25 columns</p>
</div>



## The final plot:

We can now directly visualize the table with the correlations.    
We will use the clustering we got for the gene-based table earlier:


```python
# we can now visualize the TF-based distances
# we'll use the clustering from the gene-based comparison

cg = sns.clustermap( 1-dists, 
                    
                    # The previously computed clustering
                    row_linkage=linkage_HG_rows, 
                    col_linkage=linkage_HG_cols,
                    
                    linewidths=0.01,
                    figsize =(12,12),
                    
                    vmax=0.35,
                    vmin=0.15,
                    cmap = 'Reds'
                   )

_ = plt.setp(cg.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
```


![png](main-figures/img/f4bcf_output_27_0.png)


### PWM z-scores plots


```python
def RBOscore(l1, l2, p = 0.98):
    # https://github.com/ragrawal/measures/blob/master/measures/rankedlist/RBO.py

    """
        Calculates Ranked Biased Overlap (RBO) score. 
        l1 -- Ranked List 1
        l2 -- Ranked List 2
    """
#     if l1 == None: l1 = []
#     if l2 == None: l2 = []
    
    sl,ll = sorted([(len(l1), l1),(len(l2),l2)], key=lambda x: x[0])
    s, S = sl
    l, L = ll
    if s == 0: return 0

    # Calculate the overlaps at ranks 1 through l 
    # (the longer of the two lists)
    ss = set([]) # contains elements from the smaller list till depth i
    ls = set([]) # contains elements from the longer list till depth i
    x_d = {0: 0}
    sum1 = 0.0
    for i in range(l):
        x = L[i]
        y = S[i] if i < s else None
        d = i + 1
        
        # if two elements are same then 
        # we don't need to add to either of the set
        if x == y: 
            x_d[d] = x_d[d-1] + 1.0
        # else add items to respective list
        # and calculate overlap
        else: 
            ls.add(x) 
            if y != None: ss.add(y)
            x_d[d] = x_d[d-1] + (1.0 if x in ss else 0.0) + (1.0 if y in ls else 0.0)     
        #calculate average overlap
        sum1 += x_d[d]/d * pow(p, d)
        
    sum2 = 0.0
    for i in range(l-s):
        d = s+i+1
        sum2 += x_d[d]*(d-s)/(d*s)*pow(p,d)

    sum3 = ((x_d[l]-x_d[s])/l+x_d[s]/s)*pow(p,l)

    # Equation 32
    rbo_ext = (1-p)/p*(sum1+sum2)+sum3
    return rbo_ext
```


```python
# Select some interesting cases and plot the RBOscore for those TFs in both species:
for drown, drow in dan_toplot.iterrows():
    for brown, brow in bla_toplot.iterrows():
        ld = drow.sort_values(ascending=False).index.values
        lb = brow.sort_values(ascending=False).index.values
        rbo = RBOscore(ld,lb, 0.85)
        if rbo >0.25:
            
            
            d = dan_toplot.loc[drown,:].sort_values(ascending=False)
            dset = set(d[d>1.5].index.values)

            b = bla_toplot.loc[brown,:].sort_values(ascending=False)
            bset = set(b[b>1.5].index.values)

            gset = dset.intersection(bset)

            gee = SF.loc[gset].drop_duplicates(subset=2).copy()

            gee['Danio'] = gee.index.to_series().map(d.loc[gset])
            gee['Bla'] = gee.index.to_series().map(b.loc[gset])


            tp = gee[[0,'Danio','Bla']]
            
            plt.figure(figsize=(16,1.5*len(gset)))
            order = tp.loc[tp.sum( axis=1).sort_values(ascending=False).index,0].values
            
            sns.barplot(data=pd.melt(tp,id_vars=0), x='value',y=0, hue='variable', order=order,
                       palette={'Danio':'#0072b2','Bla':'#cc79a7'})
            
            title = "{}_{} ".format(drown, brown)
            plt.title(title)
            sns.despine()
            plt.xlim((0,6))
            
            plt.show()
```


![png](main-figures/img/f4bcf_output_30_0.png)



![png](main-figures/img/f4bcf_output_30_1.png)



![png](main-figures/img/f4bcf_output_30_2.png)



![png](main-figures/img/f4bcf_output_30_3.png)



![png](main-figures/img/f4bcf_output_30_4.png)



![png](main-figures/img/f4bcf_output_30_5.png)



![png](main-figures/img/f4bcf_output_30_6.png)



![png](main-figures/img/f4bcf_output_30_7.png)



![png](main-figures/img/f4bcf_output_30_8.png)



![png](main-figures/img/f4bcf_output_30_9.png)



![png](main-figures/img/f4bcf_output_30_10.png)



![png](main-figures/img/f4bcf_output_30_11.png)



![png](main-figures/img/f4bcf_output_30_12.png)

