

```python
import pandas as pd
import numpy as np

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
import matplotlib

from collections import Counter
from sklearn.metrics.pairwise import pairwise_distances as PWD
from scipy.stats import hypergeom
statf = hypergeom.sf
from math import log
import scipy.spatial as sp, scipy.cluster.hierarchy as hc

```

### Load homology gene families


```python
genefams = pd.read_csv("../data/gene_families_table.tsv.gz",
                      sep='\t')
genefams = genefams.applymap(lambda x: x.split(":") if x==x else x)

genefamsC = genefams.applymap(lambda x: len(x) if x==x else 0)
genefams.sample(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Bbe</th>
      <th>Bla</th>
      <th>Cmi</th>
      <th>Dre</th>
      <th>Gga</th>
      <th>Hsa</th>
      <th>Mmu</th>
      <th>Ola</th>
      <th>Sko</th>
      <th>Spu</th>
      <th>Xtr</th>
      <th>Dme</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3482</th>
      <td>[201100R, 108090R]</td>
      <td>[BL05609, BL10632]</td>
      <td>[SINCAMG00000007279, SINCAMG00000007284, SINCA...</td>
      <td>[ENSDARG00000013711, ENSDARG00000042332]</td>
      <td>[ENSGALG00000004157, ENSGALG00000021972, ENSGA...</td>
      <td>[ENSG00000105355, ENSG00000147872, ENSG0000016...</td>
      <td>[ENSMUSG00000024197, ENSMUSG00000028494, ENSMU...</td>
      <td>[ENSORLG00000005030, ENSORLG00000016426]</td>
      <td>NaN</td>
      <td>[WHL22.300882]</td>
      <td>[ENSXETG00000024167, ENSXETG00000023101, ENSXE...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>16399</th>
      <td>[149300R]</td>
      <td>NaN</td>
      <td>[SINCAMG00000007874]</td>
      <td>[ENSDARG00000090568]</td>
      <td>NaN</td>
      <td>[ENSG00000172590]</td>
      <td>[ENSMUSG00000010406]</td>
      <td>[ENSORLG00000007322]</td>
      <td>[Sakowv30008857m]</td>
      <td>[WHL22.212258]</td>
      <td>[ENSXETG00000033124]</td>
      <td>[FBgn0033208]</td>
    </tr>
  </tbody>
</table>
</div>




```python
genefamsC.head(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Bbe</th>
      <th>Bla</th>
      <th>Cmi</th>
      <th>Dre</th>
      <th>Gga</th>
      <th>Hsa</th>
      <th>Mmu</th>
      <th>Ola</th>
      <th>Sko</th>
      <th>Spu</th>
      <th>Xtr</th>
      <th>Dme</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# We construct a single dictionary that maps gene IDs to family IDs
# genes from different species share the same ID if they belong in the same homology fmaily
fam_of = {}
for rowi,row in genefams[['Bla']].dropna().iterrows():
    for gene in row.Bla:
        fam_of[gene]= rowi
for rowi,row in genefams[['Dre']].dropna().iterrows():
    for gene in row.Dre:
        fam_of[gene]= rowi
```

### Load the mfuzz results

These tables map gene IDs to MFUZZ cluster numbers


```python
dan_cl = pd.read_csv("../data/mfuzz/mfuzz_geneToCluster_dan.txt.gz", sep='\t')
dan_cl.columns = ['geneID','clusterID']
dan_cl_d = dict(dan_cl[['geneID','clusterID']].to_records(index=False))
bla_cl = pd.read_csv("../data/mfuzz/mfuzz_geneToCluster_bla.txt.gz", sep='\t')
bla_cl.columns = ['geneID','clusterID']
bla_cl_d = dict(bla_cl[['geneID','clusterID']].to_records(index=False))
```


```python
dan_cl.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>geneID</th>
      <th>clusterID</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ENSDARG00000020850</td>
      <td>20</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ENSDARG00000098739</td>
      <td>22</td>
    </tr>
    <tr>
      <th>2</th>
      <td>ENSDARG00000017624</td>
      <td>23</td>
    </tr>
    <tr>
      <th>3</th>
      <td>ENSDARG00000053254</td>
      <td>10</td>
    </tr>
    <tr>
      <th>4</th>
      <td>ENSDARG00000033683</td>
      <td>10</td>
    </tr>
  </tbody>
</table>
</div>



### The core computation

Here we go through our data. We compare all pairwise combinations of clusters    
between the two species and compute the survival function of a hypergeometric distribution.    
This is equivalent to an upper tail test.    
      
We consider the total number of gene families as the Population,    
the number of different gene families in the zebrafish cluster is the Sample Size,
the number of different gene families in the amphioxus cluster is the Success in Population,
the number of common gene families between the two clusters is the Success in Sample


```python
gimme_fams = lambda g: [fam_of.get(x) for x in g.geneID.tolist()]
gene_POP = len(genefams)

# PVs is the table that will hold the Pvalues
PVs = pd.DataFrame()
# hits will record the number of common gene families between clusters
hits = pd.DataFrame()
# hl will be a detailed table
hl = []
fon = []
for gn,g  in dan_cl.groupby('clusterID'):
    for bgn,bg  in bla_cl.groupby('clusterID'):
        fams = set(gimme_fams(g))
        bfams = set(gimme_fams(bg))
        
        gene_SS = len(fams) # number of zebra families here
        gene_SIP = len(bfams) # number of amphi gene families here
        
        gene_SIS = len(bfams.intersection(fams))
        
        if gene_SIS>0:
            PV = statf(gene_SIS,  gene_POP,  gene_SS, gene_SIP)
            hits.loc[gn, bgn] = gene_SIS
            hl.append( (gn, bgn, gene_SIS,gene_SS, gene_SIP, -log(PV,10)) )
            
            fon.append( (gene_SIS,  gene_SS, gene_SIP, gene_POP, gn, bgn) )

        else:
            PV =1 
            jacc = 0
            hl.append( (gn, bgn, gene_SIS,gene_SS, gene_SIP, 0 ) )
            
        PVs.loc[gn, bgn] = PV
        
```


```python
fon = pd.DataFrame(fon)
fon.columns = ["common gene families (Success in Sample)", "zebra families (Sample Size)","amphi families (Success in Polulation)", "Total Families (population)",
              "zebra cluster", "amphi cluster"]
```


```python
fon.to_csv("./EDF8_suppletable.csv", sep='\t', index=False)
```


```python
# our detailed table for inspection of the results
hl = pd.DataFrame(hl)
hl.columns = ['Dre Cluster #', 'Bla cluster #', 'Overlapping orthologues',
              'Dre genes in cluster','Bla genes in cluster','-log(pvalue) of hypergeom. test']
hl.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Dre Cluster #</th>
      <th>Bla cluster #</th>
      <th>Overlapping orthologues</th>
      <th>Dre genes in cluster</th>
      <th>Bla genes in cluster</th>
      <th>-log(pvalue) of hypergeom. test</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>8</td>
      <td>435</td>
      <td>283</td>
      <td>0.919668</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>2</td>
      <td>44</td>
      <td>435</td>
      <td>310</td>
      <td>24.793323</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>3</td>
      <td>20</td>
      <td>435</td>
      <td>279</td>
      <td>6.572917</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1</td>
      <td>4</td>
      <td>2</td>
      <td>435</td>
      <td>287</td>
      <td>0.031037</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>5</td>
      <td>11</td>
      <td>435</td>
      <td>315</td>
      <td>1.571135</td>
    </tr>
  </tbody>
</table>
</div>



### Clustering:


```python
# get the -log(x,10) of the pvalues:
mPVs_uncl = PVs.applymap(lambda x: -log(x,10) ).copy()
# clipping the very high pvalues to 25 gives better clustering
mPVs = mPVs_uncl.clip(upper=25,lower=0)

# Make  manual hierarchical linkage objects for better control of the clustering:
linkage_rows = hc.linkage(
        sp.distance.squareform(
            PWD(  mPVs.values, metric='correlation'), 
            checks=False), 
        method='average')

linkage_cols = hc.linkage(sp.distance.squareform(PWD(  mPVs.T.values, metric='correlation'), checks=False), method='average')
```

### The plot:


```python

geneHM = sns.clustermap( 
                      mPVs_uncl.astype(int),
                      row_linkage = linkage_rows,
                      col_linkage = linkage_cols,
                      linewidths=0.01,
                      figsize =(12,12),
                      cmap='Reds',
                      vmax=25, 
                        vmin=5,
                        annot = True
                      )

_ = plt.setp(geneHM.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
```


![png](supplementary-figures/img/output_16_0.png)



```python
# The plot without numbers:
geneHM = sns.clustermap( mPVs,
                      row_linkage = linkage_rows,
                      col_linkage = linkage_cols,
                      linewidths=0.01,
                      figsize =(12,12),
                      cmap='Reds',
                      vmax=25, 
                        vmin=5

                      )

_ = plt.setp(geneHM.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
```


![png](supplementary-figures/img/output_17_0.png)

