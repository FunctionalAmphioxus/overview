

```python
import pandas as pd
from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
# This saves fonts as fonts in the svg and not as shapes
plt.rcParams['svg.fonttype'] = 'none'

import numpy as np
from scipy.stats import mannwhitneyu as MWU

import warnings
warnings.filterwarnings('ignore')
```

## Data Load

We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream, or untill it encounters another TSS.

For the GREAT region, we extend the BASAL region UP TO 1Mb in each way, or until a BASAL region of another gene is encountered.    
     
Here, we load and use the GREAT regions


```python
greg = {}

greg['Dre'] = pd.read_csv("./data/genomic_regions/GREAT_dre.bed.gz", sep='\t', header=None)
greg['Dre'].columns = ['chrom','start','end','geneID','score','strand']
greg['Dre']['score'] = greg['Dre']['end'] - greg['Dre']['start']

greg['Bla'] = pd.read_csv("./data/genomic_regions/GREAT_bla.bed.gz", sep='\t', header=None)
greg['Bla'].columns = ['chrom','start','end','geneID','score','strand']
greg['Bla']['score'] = greg['Bla']['end'] - greg['Bla']['start']

greg['Ola'] = pd.read_csv("./data/genomic_regions/GREAT_ola.bed.gz", sep='\t', header=None)
greg['Ola'].columns = ['chrom','start','end','geneID','score','strand']
greg['Ola']['score'] = greg['Ola']['end'] - greg['Ola']['start']

greg['Mmu'] = pd.read_csv("./data/genomic_regions/GREAT_mmu.bed.gz", sep='\t', header=None)
greg['Mmu'].columns = ['chrom','start','end','geneID','score','strand']
greg['Mmu']['score'] = greg['Mmu']['end'] - greg['Mmu']['start']

print( [(k,len(v)) for k,v in greg.items()])
```

    [('Dre', 20053), ('Bla', 20569), ('Ola', 15978), ('Mmu', 18842)]



```python
greg['Mmu'].head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>geneID</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>chr1</td>
      <td>2670503</td>
      <td>4359310</td>
      <td>ENSMUSG00000051951</td>
      <td>1688807</td>
      <td>-</td>
    </tr>
    <tr>
      <th>1</th>
      <td>chr1</td>
      <td>3676503</td>
      <td>4495409</td>
      <td>ENSMUSG00000025900</td>
      <td>818906</td>
      <td>-</td>
    </tr>
    <tr>
      <th>2</th>
      <td>chr1</td>
      <td>4365319</td>
      <td>4784706</td>
      <td>ENSMUSG00000025902</td>
      <td>419387</td>
      <td>-</td>
    </tr>
    <tr>
      <th>3</th>
      <td>chr1</td>
      <td>4501418</td>
      <td>4802819</td>
      <td>ENSMUSG00000033845</td>
      <td>301401</td>
      <td>-</td>
    </tr>
    <tr>
      <th>4</th>
      <td>chr1</td>
      <td>4790715</td>
      <td>4852810</td>
      <td>ENSMUSG00000025903</td>
      <td>62095</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
stages = {}
stages['Bla'] = ['8','15','36','60']
stages['Dre'] = ["dome","shield","80epi","8som","24h","48h"]
stages['Ola'] = ["dome","shield","8som","24h","48h"]
stages['Mmu'] = ['DE','ESC']
```

### The ATAC-seq peaks

As determined by the idr "pipeline", see more in the peak-calling notebook


```python
pre_ = './data/atac_peaks/'

peak_beds = {}

peak_beds['Dre'] = [ (BT("{}zebra_danRer10_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Dre']]
peak_beds['Bla'] = [(BT("{}amphi_{}_idrpeaks.bed.gz".format(pre_,x))
                     .sort()
                     ) for x in stages['Bla']]
peak_beds['Ola'] = [ (BT("{}medaka_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Ola']]
peak_beds['Mmu'] = [ (BT("{}mouse_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Mmu']]

```


```python
fate_dict_zebra = dict(pd.read_csv("./data/gene_fates/Spec_genes_byLost-Dre-v3.txt.gz", sep='\t',header=None).set_index(0)[1])
fate_dict_mouse = dict(pd.read_csv("./data/gene_fates/Spec_genes_byLost-Mmu-v3.txt.gz", sep='\t',header=None).set_index(0)[1])
```


```python
bedfields = ['chrom','start','end','name','score','strand']
big = {}

big['Dre'] = BT().from_dataframe(greg['Dre']).sort()
for bee in peak_beds['Dre']:
    big['Dre'] = big['Dre'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Dre'] = big['Dre'].to_dataframe()
big['Dre'].columns = bedfields + stages['Dre']

big['Mmu'] = BT().from_dataframe(greg['Mmu']).sort()
for bee in peak_beds['Mmu']:
    big['Mmu'] = big['Mmu'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Mmu'] = big['Mmu'].to_dataframe()
big['Mmu'].columns = bedfields + stages['Mmu']
```


```python
big['Dre'].sample(5)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>dome</th>
      <th>shield</th>
      <th>80epi</th>
      <th>8som</th>
      <th>24h</th>
      <th>48h</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>7275</th>
      <td>chr18</td>
      <td>38211343</td>
      <td>38262859</td>
      <td>ENSDARG00000074628</td>
      <td>51516</td>
      <td>-</td>
      <td>3</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>5</td>
    </tr>
    <tr>
      <th>8084</th>
      <td>chr19</td>
      <td>43317981</td>
      <td>43375524</td>
      <td>ENSDARG00000061057</td>
      <td>57543</td>
      <td>-</td>
      <td>7</td>
      <td>7</td>
      <td>8</td>
      <td>8</td>
      <td>11</td>
      <td>11</td>
    </tr>
    <tr>
      <th>14633</th>
      <td>chr4</td>
      <td>16807717</td>
      <td>16846026</td>
      <td>ENSDARG00000004904</td>
      <td>38309</td>
      <td>-</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9069</th>
      <td>chr2</td>
      <td>56535388</td>
      <td>56649453</td>
      <td>ENSDARG00000052496</td>
      <td>114065</td>
      <td>-</td>
      <td>7</td>
      <td>10</td>
      <td>14</td>
      <td>23</td>
      <td>16</td>
      <td>12</td>
    </tr>
    <tr>
      <th>12361</th>
      <td>chr24</td>
      <td>25533496</td>
      <td>25547850</td>
      <td>ENSDARG00000019342</td>
      <td>14354</td>
      <td>-</td>
      <td>4</td>
      <td>2</td>
      <td>3</td>
      <td>5</td>
      <td>3</td>
      <td>3</td>
    </tr>
  </tbody>
</table>
</div>




```python
dre_melt = pd.melt(big['Dre'][['name']+stages['Dre']], id_vars='name')
dre_melt['DomL'] = dre_melt['name'].map(fate_dict_zebra)
dre_melt = dre_melt.dropna()
```


```python
mmu_melt = pd.melt(big['Mmu'][['name']+stages['Mmu']], id_vars='name')
mmu_melt['DomL'] = mmu_melt['name'].map(fate_dict_mouse)
mmu_melt = mmu_melt.dropna()
```


```python
mergerdic = {0 : '0',
            1 : '1-2',
            2: '1-2',
            3: '3-4',
            4: '3-4',
            5: '5-6',
            6: '5-6',
            7: '7+',
            8: '7+',
            9: '7+'}
```


```python
dre_melt['mergedfate'] = dre_melt.DomL.map(mergerdic)
mmu_melt['mergedfate'] = mmu_melt.DomL.map(mergerdic)
```


```python
dre_melt.head(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>name</th>
      <th>variable</th>
      <th>value</th>
      <th>DomL</th>
      <th>mergedfate</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>28</th>
      <td>ENSDARG00000100181</td>
      <td>dome</td>
      <td>6</td>
      <td>1.0</td>
      <td>1-2</td>
    </tr>
    <tr>
      <th>32</th>
      <td>ENSDARG00000041592</td>
      <td>dome</td>
      <td>4</td>
      <td>1.0</td>
      <td>1-2</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Phyloty. stage:
plt.figure(figsize=(16,9))
sns.boxplot( data=dre_melt[dre_melt.variable == '8som'], 
           y='value',
           x='mergedfate',
           order = ['0','1-2','3-4','5-6','7+'],
            
        fliersize=0
           )


plt.ylim((0,55))
```




    (0, 55)




![png](supplementary-figures/img/output_15_1.png)


## The p values


```python
buz = dre_melt[dre_melt.variable == '8som']
```


```python
MWU(buz.loc[buz['mergedfate']=='7+','value'].values,
    buz.loc[buz['mergedfate']=='0','value'].values)
```




    MannwhitneyuResult(statistic=18352.0, pvalue=7.034079147608669e-05)




```python
MWU(buz.loc[buz['mergedfate']=='5-6','value'].values,
    buz.loc[buz['mergedfate']=='0','value'].values)
```




    MannwhitneyuResult(statistic=11713.0, pvalue=0.00023464727422464147)




```python
MWU(buz.loc[buz['mergedfate']=='3-4','value'].values,
    buz.loc[buz['mergedfate']=='0','value'].values)
```




    MannwhitneyuResult(statistic=13215.5, pvalue=0.039048790996443199)




```python
pd.DataFrame( buz.groupby("mergedfate").apply(lambda g: len(g)) )
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
    </tr>
    <tr>
      <th>mergedfate</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>334</td>
    </tr>
    <tr>
      <th>1-2</th>
      <td>129</td>
    </tr>
    <tr>
      <th>3-4</th>
      <td>90</td>
    </tr>
    <tr>
      <th>5-6</th>
      <td>92</td>
    </tr>
    <tr>
      <th>7+</th>
      <td>141</td>
    </tr>
  </tbody>
</table>
</div>


