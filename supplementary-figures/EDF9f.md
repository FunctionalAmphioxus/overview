

```python
import pandas as pd
from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
# This saves fonts as fonts in the svg and not as shapes
plt.rcParams['svg.fonttype'] = 'none'

import numpy as np
from scipy.stats import mannwhitneyu as MWU

import warnings
warnings.filterwarnings('ignore')

from functools import reduce
from operator import add
```


```python
from math import log
```

## Data Load

We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream, or untill it encounters another TSS.

For the GREAT region, we extend the BASAL region UP TO 1Mb in each way, or until a BASAL region of another gene is encountered.    
     
Here, we load and use the GREAT regions


```python
greg = {}

greg['Dre'] = pd.read_csv("./data/genomic_regions/intergenics_Dre.tsv.gz", sep='\t', header=None)
greg['Dre'].columns = ['chrom','start','end','gene1','gene2']
greg['Dre']['score'] = greg['Dre']['end'] - greg['Dre']['start']

greg['Bla'] = pd.read_csv("./data/genomic_regions/intergenics_Bla.tsv.gz", sep='\t', header=None)
greg['Bla'].columns = ['chrom','start','end','gene1','gene2']
greg['Bla']['score'] = greg['Bla']['end'] - greg['Bla']['start']

greg['Ola'] = pd.read_csv("./data/genomic_regions/intergenics_Ola.tsv.gz", sep='\t', header=None)
greg['Ola'].columns = ['chrom','start','end','gene1','gene2']
greg['Ola']['score'] = greg['Ola']['end'] - greg['Ola']['start']

greg['Mmu'] = pd.read_csv("./data/genomic_regions/intergenics_Mmu.tsv.gz", sep='\t', header=None)
greg['Mmu'].columns = ['chrom','start','end','gene1','gene2']
greg['Mmu']['score'] = greg['Mmu']['end'] - greg['Mmu']['start']

print( [(k,len(v)) for k,v in greg.items()])
```

    [('Dre', 19837), ('Bla', 18943), ('Ola', 15473), ('Mmu', 18814)]



```python
greg['Dre'].head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>gene1</th>
      <th>gene2</th>
      <th>score</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MT</td>
      <td>3803</td>
      <td>4994</td>
      <td>ENSDARG00000063895</td>
      <td>ENSDARG00000063899</td>
      <td>1191</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MT</td>
      <td>4993</td>
      <td>6426</td>
      <td>ENSDARG00000063899</td>
      <td>ENSDARG00000063905</td>
      <td>1433</td>
    </tr>
    <tr>
      <th>2</th>
      <td>MT</td>
      <td>6425</td>
      <td>8131</td>
      <td>ENSDARG00000063905</td>
      <td>ENSDARG00000063908</td>
      <td>1706</td>
    </tr>
    <tr>
      <th>3</th>
      <td>MT</td>
      <td>8130</td>
      <td>8895</td>
      <td>ENSDARG00000063908</td>
      <td>ENSDARG00000063910</td>
      <td>765</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MT</td>
      <td>8894</td>
      <td>9053</td>
      <td>ENSDARG00000063910</td>
      <td>ENSDARG00000063911</td>
      <td>159</td>
    </tr>
  </tbody>
</table>
</div>




```python
stages = {}
stages['Bla'] = ['8','15','36','60']
stages['Dre'] = ["dome","shield","80epi","8som","24h","48h"]
stages['Ola'] = ["dome","shield","8som","24h","48h"]
stages['Mmu'] = ['DE','ESC']
```

### The ATAC-seq peaks

As determined by the idr "pipeline", see more in the peak-calling notebook


```python
pre_ = './data/atac_peaks/'

peak_beds = {}

peak_beds['Dre'] = [ (BT("{}zebra_danRer10_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Dre']]
peak_beds['Bla'] = [(BT("{}amphi_{}_idrpeaks.bed.gz".format(pre_,x))
                     .sort()
                     ) for x in stages['Bla']]
peak_beds['Ola'] = [ (BT("{}medaka_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Ola']]
peak_beds['Mmu'] = [ (BT("{}mouse_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Mmu']]

```

### Grouping of genes into homologous-families       

We have in our disposal a precomputed table where genes of various species
are separated into homologous families.

Each row is a family, each column a species. 
The paralogues of each species are separated with ":" so when we load this dataset
we split the strings into lists.

We also create a second dataframe "genefamsC" which has the same index and shape as the first one,
but contains the count of genes in each cell.


```python
genefams = pd.read_csv("./data/gene_families_table.tsv.gz",
                      sep='\t')
genefams = genefams.applymap(lambda x: x.split(":") if x==x else x)

genefamsC = genefams.applymap(lambda x: len(x) if x==x else 0)
```


```python
genefamsC.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Bbe</th>
      <th>Bla</th>
      <th>Cmi</th>
      <th>Dre</th>
      <th>Gga</th>
      <th>Hsa</th>
      <th>Mmu</th>
      <th>Ola</th>
      <th>Sko</th>
      <th>Spu</th>
      <th>Xtr</th>
      <th>Dme</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>28</td>
      <td>36</td>
      <td>6</td>
      <td>9</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>61</td>
      <td>8</td>
      <td>16</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Make some masks for the dataframe
mask_oto = (genefamsC['Bla']==1) & (genefamsC['Mmu']==1)  # 1-1
mask_ottw = (genefamsC['Bla']==1) & (genefamsC['Mmu']==2) # 1-2
mask_otth = (genefamsC['Bla']==1) & (genefamsC['Mmu']==3) # 1-3
mask_otfo = (genefamsC['Bla']==1) & (genefamsC['Mmu']==4) # 1-4

masks = [mask_oto, mask_ottw,mask_otth,mask_otfo]
titles = ['1-1','1-2','1-3','1-4']

# then some sets
oto_genes = genefams.loc[mask_oto,['Bla','Dre','Mmu','Ola']]
oto_genes = set([x for y in oto_genes.values.flatten() if y==y for x in y])
ottw_genes = genefams.loc[mask_ottw,['Bla','Dre','Mmu','Ola']]
ottw_genes = set([x for y in ottw_genes.values.flatten() if y==y for x in y])
otth_genes = genefams.loc[mask_otth,['Bla','Dre','Mmu','Ola']]
otth_genes = set([x for y in otth_genes.values.flatten() if y==y for x in y])
otfo_genes = genefams.loc[mask_otfo,['Bla','Dre','Mmu','Ola']]
otfo_genes = set([x for y in otfo_genes.values.flatten() if y==y for x in y])

# and use the sets to categorize the genes
def categorize(x):
    if x in oto_genes:
        return '1-1'
    elif x in ottw_genes:
        return '1-2'
    elif x in otth_genes:
        return '1-3'
    elif x in otfo_genes:
        return '1-4'
    else:
        return 'nop'
```


```python
bedfields = ['chrom','start','end','name','score','strand']
big = {}

big['Dre'] = BT().from_dataframe(greg['Dre']).sort()
for bee in peak_beds['Dre']:
    big['Dre'] = big['Dre'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Dre'] = big['Dre'].to_dataframe()
big['Dre'].columns = bedfields + stages['Dre']

# the other species:
big['Bla'] = BT().from_dataframe(greg['Bla']).sort()
for bee in peak_beds['Bla']:
    big['Bla'] = big['Bla'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Bla'] = big['Bla'].to_dataframe()
big['Bla'].columns = bedfields + stages['Bla']

big['Ola'] = BT().from_dataframe(greg['Ola']).sort()
for bee in peak_beds['Ola']:
    big['Ola'] = big['Ola'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Ola'] = big['Ola'].to_dataframe()
big['Ola'].columns = bedfields + stages['Ola']

big['Mmu'] = BT().from_dataframe(greg['Mmu']).sort()
for bee in peak_beds['Mmu']:
    big['Mmu'] = big['Mmu'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Mmu'] = big['Mmu'].to_dataframe()
big['Mmu'].columns = bedfields + stages['Mmu']
```


```python
big['Dre'].head(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>dome</th>
      <th>shield</th>
      <th>80epi</th>
      <th>8som</th>
      <th>24h</th>
      <th>48h</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>MT</td>
      <td>3803</td>
      <td>4994</td>
      <td>ENSDARG00000063895</td>
      <td>ENSDARG00000063899</td>
      <td>1191</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>MT</td>
      <td>4993</td>
      <td>6426</td>
      <td>ENSDARG00000063899</td>
      <td>ENSDARG00000063905</td>
      <td>1433</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
a = big['Dre'][['name','strand','8som']]
a.columns = [1,2,3]
b = big['Dre'][['score','strand', '8som']]
b.columns = [1,2,3]
dre_ = pd.concat([a,b]).groupby(1).sum()
dre_.columns = ['size','count']
dre_['category'] = dre_.index.to_series().apply(categorize)
dre_['species'] = 'Dre'
dre_['logsize'] = dre_['size'].apply(lambda x: log(x,10))

a = big['Bla'][['name','strand','15']]
a.columns = [1,2,3]
b = big['Bla'][['score','strand', '15']]
b.columns = [1,2,3]
bla_ = pd.concat([a,b]).groupby(1).sum()
bla_.columns = ['size','count']
bla_['category'] = bla_.index.to_series().apply(categorize)
bla_['species'] = 'Bla'
bla_['logsize'] = bla_['size'].apply(lambda x: log(x,10))

a = big['Ola'][['name','strand','8som']]
a.columns = [1,2,3]
b = big['Ola'][['score','strand', '8som']]
b.columns = [1,2,3]
ola_ = pd.concat([a,b]).groupby(1).sum()
ola_.columns = ['size','count']
ola_['category'] = ola_.index.to_series().apply(categorize)
ola_['species'] = 'Ola'
ola_['logsize'] = ola_['size'].apply(lambda x: log(x,10))

a = big['Mmu'][['name','strand','ESC']]
a.columns = [1,2,3]
b = big['Mmu'][['score','strand', 'ESC']]
b.columns = [1,2,3]
mmu_ = pd.concat([a,b]).groupby(1).sum()
mmu_.columns = ['size','count']
mmu_['category'] = mmu_.index.to_series().apply(categorize)
mmu_['species'] = 'Mmu'
mmu_['logsize'] = mmu_['size'].apply(lambda x: log(x,10))
```


```python
tp = pd.concat([dre_, bla_, ola_,mmu_])
tp.columns = ['size','count','category','species','logsize']
```


```python
tp.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>size</th>
      <th>count</th>
      <th>category</th>
      <th>species</th>
      <th>logsize</th>
    </tr>
    <tr>
      <th>1</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ENSDARG00000000001</th>
      <td>37852</td>
      <td>4</td>
      <td>nop</td>
      <td>Dre</td>
      <td>4.578089</td>
    </tr>
    <tr>
      <th>ENSDARG00000000002</th>
      <td>244365</td>
      <td>36</td>
      <td>1-3</td>
      <td>Dre</td>
      <td>5.388039</td>
    </tr>
    <tr>
      <th>ENSDARG00000000018</th>
      <td>409113</td>
      <td>52</td>
      <td>1-1</td>
      <td>Dre</td>
      <td>5.611843</td>
    </tr>
    <tr>
      <th>ENSDARG00000000019</th>
      <td>97431</td>
      <td>20</td>
      <td>nop</td>
      <td>Dre</td>
      <td>4.988697</td>
    </tr>
    <tr>
      <th>ENSDARG00000000068</th>
      <td>98087</td>
      <td>13</td>
      <td>nop</td>
      <td>Dre</td>
      <td>4.991611</td>
    </tr>
  </tbody>
</table>
</div>




```python
plt.figure(figsize=(6,12))


sns.boxplot(data = tp,
           x='species',
            order=['Bla','Dre','Ola','Mmu'],
            hue='category',
            hue_order=['1-1','1-2','1-3','1-4'],
           y='logsize',
           palette = 'Blues',
           fliersize=0)

plt.legend(loc='upper left')
plt.ylabel('log10 of assigned intragenic regions total size')
```




    Text(0,0.5,'log10 of assigned intragenic regions total size')




![png](supplementary-figures/img/edf9foutput_17_1.png)


#### Since we're here, let's count peaks as well


```python
plt.figure(figsize=(6,12))


sns.boxplot(data = tp,
           x='species',
            order=['Bla','Dre','Ola','Mmu'],
            hue='category',
            hue_order=['1-1','1-2','1-3','1-4'],
           y='count',
           palette = 'Blues',
           fliersize=0)

plt.legend(loc='upper left')

plt.ylim((0,80))
```




    (0, 80)




![png](supplementary-figures/img/edf9foutput_19_1.png)

