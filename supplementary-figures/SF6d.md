
## Gene modules comparisons

## Needed data:
* ### Assignment of genes to modules (WGCNA / MFUZZ)    

We use WGCNA to cluster genes by their RNA activity in various TRNAseq experiments, both in developmental stages and pure tissue samples. This analysis assigns each gene in a single cluster. These clusters have names like "blue". "green" etc.
We manually explore the clusters, and by determining at which experiment the genes of the cluster seem to be differentially expressed (but also by GO analysis on each cluster), we assign more meaningful names to the clusters , like "liver", "muscle" etc. The translating dictionaries are in the end of the notebook.

* ### Assignment of genome regions to genes 
    
We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream,
or untill it encounters another TSS.
    
* ### TF Motifs mapped in the genome
* ### Grouping of genes into homology-families       


```python
import gzip
import os
import pysam

from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
sns.set_style('white')
from matplotlib import pyplot as plt

import pandas as pd
import numpy as np

from collections import Counter
from functools import reduce
from operator import add


from scipy.stats import hypergeom
statf = hypergeom.sf
from math import log

from scipy.stats import pearsonr
import scipy.spatial as sp, scipy.cluster.hierarchy as hc

from sklearn.preprocessing import scale, normalize, robust_scale
from sklearn.metrics.pairwise import pairwise_distances as PWD


import warnings
warnings.filterwarnings('ignore')
```

## Preprocessing:


```python
# Here we load the mapping of genes to modules from csv
# into dictionaries:
dan_cl = pd.read_csv("./data/wgcna_results/wgcna_dre_modulekey.tab.gz", sep='\t')
dan_cl.columns = ['geneID','clusterID','updown']
dan_cl_d = dict(dan_cl[['geneID','clusterID']].to_records(index=False))
bla_cl = pd.read_csv("./data/wgcna_results/wgcna_bla_modulekey.tab.gz", sep='\t')
bla_cl.columns = ['geneID','clusterID','updown']
bla_cl_d = dict(bla_cl[['geneID','clusterID']].to_records(index=False))

# bla_cl_d ~= {'BL06687': 'greenyellow',
#  'BL12105': 'greenyellow',
#  'BL18033': 'magenta',....}
```


```python
# we load the gene family bindings into a dictionary where 
# key = geneID
# value =  unique gene family number (some integer)
gfams = (pd
            .read_csv("./data/gene_families_table.tsv.gz",
                     sep='\t')
            .applymap(lambda c: c.split(':') if c==c else []))

gfamsC = gfams.applymap(lambda c: len(c))

gfamsD = {}
for rowi, row in gfams.iterrows():
    for species in ['Bla','Dre','Mmu','Hsa','Dme']:
        for gene in row[species]:
            gfamsD[gene] = int(rowi)
fam_of = {}
for rowi,row in gfams[['Bla']].dropna().iterrows():
    for gene in row.Bla:
        fam_of[gene]= rowi
for rowi,row in gfams[['Dre']].dropna().iterrows():
    for gene in row.Dre:
        fam_of[gene]= rowi
# fam_of maps all genes to their gene family ID
```


```python
### translate the uninformative color names to manually made informative names for each module
### Cleaner versions of these dictionaries at the bottom of the notebook
transA = {'black' : '32 cells','blue' : 'N.tube(neurotrans.)','brown' : 'Gills','cyan' : 'N.tube(neurogen.)','darkmagenta' : 'Ovary/ Testis(translation)','darkorange' : 'Cilium','darkred' : 'Muscle','darkseagreen4' : '8-36hpf embryo/ Skin/ Cirri(memb. synt.)','darkslateblue' : '8hpf embryo(transcription, spliceosome)','darkturquoise' : '36hpf embryo','green' : 'Eggs/ 32 cells(cell cycle)','greenyellow' : 'Hepatic(lipid catabolism)','lavenderblush3' : 'Cirri / PreMet. / Muscle - actomyosin','lightpink4' : '15hpf embryo','magenta' : 'Skin','navajowhite2' : 'Immune','palevioletred3' : 'Ovary/ Eggs','pink' : 'Gut','plum1' : 'Gut/ Hepatic','plum2' : 'Proteasome','red' : 'PreMet.larvae','salmon' : 'Cirri(memb. synt.)','sienna3' : 'Hepatic','thistle2' : 'Gills/ PreMet larvae',
'turquoise' : 'Mitochondrion'}
transZ = {'bisque4' : "larvae 7d melanin",'black' : "Ovary/ Sperm",'blue' : "Brain",'brown' : "Cilium",'brown4' : "Liver(carboxi.met./ lipid trans)",'coral2' : "Mitochondrion",'darkgreen' : "Intestine",'darkgrey' : "RNA, ribosome, proteasome",'darkmagenta' : "Translation, ribosome, RNA bin.",'darkorange' : "Muscle",'darkred' : "Liver(oxi-red.proc./ hemostasis)",'darkseagreen4' : "12-26hpf embryo/ Skin",'darkslateblue' : "Immune",'green' : "Spliceosome",'honeydew1' : "12-26hpf embryo",'ivory' : "Eye",'lightcyan' : "Heart",'lightgreen' : "Pancreas/ Testis",'magenta' : "Gills",'pink' : "Skin",
    'salmon' : "20-26hpf embryo",'yellow4' : "Kidney",
'yellowgreen' : "Gills/ Skin"}
```


```python
# We compute the upper tail of the hypergeometric distribution
# (survival function) for each pair of modules as a metric of enrichment

gimme_fams = lambda g: [fam_of.get(x) for x in g.geneID.tolist()]
# Population size:
gene_POP = len(gfams)

PVS = pd.DataFrame()
# for each module in zebra
for gn,g  in dan_cl.groupby('clusterID'):
    # for each module in amphioxus:
    for bgn,bg  in bla_cl.groupby('clusterID'):
        
        fams = set(gimme_fams(g))
        bfams = set(gimme_fams(bg))
        
        gene_SS = len(fams) # Sample Size
        gene_SIP = len(bfams) # Success in Population
        gene_SIS = len(bfams.intersection(fams)) # success in sample
        
        if gene_SIS>0:
            # statf is the survival function of the hypergeometric distribution            
            fish = statf(gene_SIS,  gene_POP,  gene_SS, gene_SIP)
        else:
            fish =1 
            
        PVS.loc[transZ[gn], transA[bgn]] = fish
        
# the translating dictionaries are at the end of the notebook
```

## The gene-based heatmap


```python
# we get the -log transformation of the pvalues, clip them to a workable range
mPVS = PVS.applymap(lambda x: -log(x,10) ).copy().clip(upper=25,lower=0)

# We will compute the clustering outselves in order to be able to re-use it,
# and to have better control
# we cluster by row and by column, based on euclidean distance and complete method:
linkage_HG_rows = hc.linkage(sp.distance.squareform(PWD(  mPVS.values, metric='euclidean'), checks=False), method='complete')
linkage_HG_cols = hc.linkage(sp.distance.squareform(PWD(  mPVS.T.values, metric='euclidean'), checks=False), method='complete')

# Calculating the clustering like this, allows us to use the same clustering later on
```


```python
# Finally we plot this as a clustered heatmap
geneHM = sns.clustermap( mPVS,
                      row_linkage = linkage_HG_rows,
                      col_linkage = linkage_HG_cols,
                      linewidths=0.01,
                      figsize =(12,12),
                      cmap='Reds',
                      vmax=25, vmin=5

                      )

_ = plt.setp(geneHM.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
plt.title('Enrichment for orthologous genes')
```




    Text(0.5,1,'Enrichment for orthologous genes')




![png](supplementary-figures/img/output_9_1.png)


## We now want to map PWMs to modules

To do this, we will use the BASAL region of genes:


```python
# gene to "gene region" connection
# This dataframe connects each gene to a genomic region:
dan_greg = pd.read_csv("./data/genomic_regions/BASAL_dre.bed.gz", sep='\t', header=None)
dan_greg.columns = ['chrom','start','end','geneID','strand']
dan_greg['score'] = 0
dan_greg = dan_greg[['chrom','start','end','geneID','score','strand']]

bla_greg = pd.read_csv("./data/genomic_regions/BASAL_bla.bed.gz", sep='\t', header=None)
bla_greg.columns = ['chrom','start','end','geneID','strand']
bla_greg['score'] = 0
bla_greg = bla_greg[['chrom','start','end','geneID','score','strand']]
```

and of course the PWMs that have been mapped on ATACseq peaks already:


```python
from glob import glob
```


```python
dan_Acceptable_regions1 = BT("./data/atac_peaks_clustered_byk27/zebra_JTcluster1_mergeStages.bed")
```


```python
lot = []

for fp in glob("./data/atac_peaks_clustered_byk4/clusters_zebra_k4me3_TSS_*.txt"):
    tmp = pd.read_csv(fp, sep='\t', header=None)
    tmp[1] = (tmp[1] -1000).clip(lower=0)
    tmp[2] = (tmp[2] +1000).clip(lower=0)
    lot.append( tmp[tmp[6]=='cluster1'])
dan_Acceptable_regions2 = BT().from_dataframe(pd.concat(lot))    
```


```python
dan_acceptable = dan_Acceptable_regions1.cat(dan_Acceptable_regions2)
```


```python
bla_Acceptable_regions1 = BT("./data/atac_peaks_clustered_byk27/amphi_JTclusters_mergeStages.bed")
```


```python
lot = []

for fp in glob("./data/atac_peaks_clustered_byk4/clusters_Bla_*_k4me3.txt"):
    print(fp)
    tmp = pd.read_csv(fp, sep='\t', header=None)
    
    tmp[1] = (tmp[1] -1000).clip(lower=0)
    tmp[2] = (tmp[2] +1000).clip(lower=0)
    
    lot.append( tmp[tmp[6]=='cluster1'])
    
bla_Acceptable_regions2 = BT().from_dataframe(pd.concat(lot)) 
```

    ./data/atac_peaks_clustered_byk4/clusters_Bla_36h_k4me3.txt
    ./data/atac_peaks_clustered_byk4/clusters_Bla_8h_k4me3.txt
    ./data/atac_peaks_clustered_byk4/clusters_Bla_15h_k4me3.txt



```python
bla_acceptable = bla_Acceptable_regions1.cat(bla_Acceptable_regions2)
```


```python
len(bla_acceptable), len(dan_acceptable)
```




    (55923, 82327)




```python
dan_motif_bed = (BT("./data/danre_pwm_hits.bed.gz")
                    .intersect(dan_acceptable, u=True)
                    .sort())
# .intersect(b=dan_good_peaks, u=True)
```


```python
bla_motif_bed = (BT("./data/bralan_pwm_hits.bed.gz")
                 .intersect(bla_acceptable, u=True)
                 .sort())
                 
```


```python
amphi_stages = ['8','15','36','60','hep']
zebra_stages = ['dome','shield','80epi','8som','24h','48h']
```

We also need the following table, which assigns a unique id number to each PWM.


```python
# busy-work to assign unique IDs to each PWM family
superfams = pd.read_csv("./data/PWMname_to_ProteinFamily.tsv.gz", sep='\t', header=None)
def order_things(x):
    return ';'.join(sorted(x.split(';')))
lot = []
for i,row in superfams.iterrows():
    if ';' not in row[1]:
        lot.append( row.tolist() )
    else:
        a,b = [';'.join(jj) for jj in zip(*sorted(zip(row[1].split(';'), row[2].split(';')), key= lambda x: int(x[0]) ))]
        lot.append( [row[0], a, b] )
superfams_ = pd.DataFrame(lot)
SFD = dict(superfams_[[0,1]].to_records(index=False))

fuid = {}
c = 0
for v in SFD.values():
    if v not in fuid:
        fuid[v] = c
        c += 1
SFDu = {k:fuid[v] for k,v in SFD.items()}

superfams_['u'] = superfams_[0].map(SFDu)
SF = superfams_.set_index('u')
```

We will now compute the LOJ intersection of the PMs to the BASAL regions,
For each gene this will give us all PWMs in its "regulatory landscape"


```python
# Map the cluster ID to gene IDs:
dan_greg['cluster'] = dan_greg.geneID.map(dan_cl_d)
dan_greg = dan_greg[~dan_greg.cluster.isnull()]

bla_greg['cluster'] = bla_greg.geneID.map(bla_cl_d)
bla_greg = bla_greg[~bla_greg.cluster.isnull()]

# cast the gene region dataframes into pybedtools objects
# and intersect with motifs (with LEFT OUTER JOIN)
dan_loj = (BT()
        .from_dataframe(dan_greg[['chrom','start','end','cluster','score','strand']])
        .sort()
        .intersect(dan_motif_bed, loj=True, nonamecheck=True, sorted=True)
        ).to_dataframe()[['chrom','start','end','name','score','strand','blockCount']]

dan_loj = dan_loj[dan_loj.blockCount != '.']
dan_loj['fam'] = dan_loj.blockCount.map(SFDu)
dan_loj = dan_loj[~dan_loj['fam'].isnull()]
dan_loj.fam = dan_loj.fam.astype(int)

bla_loj = (BT()
        .from_dataframe(bla_greg[['chrom','start','end','cluster','score','strand']])
        .sort()
        .intersect(bla_motif_bed, loj=True, nonamecheck=True, sorted=True)
        ).to_dataframe()[['chrom','start','end','name','score','strand','blockCount']]

bla_loj = bla_loj[bla_loj.blockCount != '.']
bla_loj['fam'] = bla_loj.blockCount.map(SFDu)
bla_loj = bla_loj[~bla_loj['fam'].isnull()]
dan_loj.fam = dan_loj.fam.astype(int)
bla_loj.head()

# we had 
# gene_module --> genes
# genes --> genomic_regions
# motifs --> genomic_positions

# and we managed to connect them:
# module/cluster --> genes -> genomic regions --> motifs
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>blockCount</th>
      <th>fam</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>C2H2_ZF_Average_244</td>
      <td>11.0</td>
    </tr>
    <tr>
      <th>5</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>ARID_BRIGHT_RFX_M4343_1.02</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>6</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>RFX_Average_38</td>
      <td>3.0</td>
    </tr>
    <tr>
      <th>7</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>bZIP_Average_125</td>
      <td>236.0</td>
    </tr>
    <tr>
      <th>8</th>
      <td>Sc0000000</td>
      <td>20509</td>
      <td>26510</td>
      <td>blue</td>
      <td>0</td>
      <td>+</td>
      <td>C2H2_ZF_M6539_1.02</td>
      <td>21.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# convert the left outer join dataframes into
# tables that contain a count per TF per stage:
bla_loC = [Counter(g.fam) for gn,g in bla_loj.groupby("name")]
bla_temp = pd.DataFrame(bla_loC)   
bla_temp = bla_temp.fillna(0)   
clustorder = [transA[gn] for gn,g in bla_loj.groupby("name")]

bla_temp.index = clustorder
bla_table = bla_temp.copy()

dan_loC = [Counter(g.fam) for gn,g in dan_loj.groupby("name")]
dan_temp = pd.DataFrame(dan_loC)   
dan_temp = dan_temp.fillna(0)   
clustorder = [transZ[gn] for gn,g in dan_loj.groupby("name")]

dan_temp.index = clustorder
dan_table = dan_temp.copy()
dan_table.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th>6</th>
      <th>7</th>
      <th>8</th>
      <th>9</th>
      <th>...</th>
      <th>247</th>
      <th>248</th>
      <th>249</th>
      <th>250</th>
      <th>251</th>
      <th>252</th>
      <th>253</th>
      <th>254</th>
      <th>255</th>
      <th>256</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>larvae 7d melanin</th>
      <td>135</td>
      <td>363</td>
      <td>11.0</td>
      <td>44</td>
      <td>11</td>
      <td>72</td>
      <td>11</td>
      <td>8.0</td>
      <td>42</td>
      <td>3.0</td>
      <td>...</td>
      <td>11</td>
      <td>19</td>
      <td>12</td>
      <td>9</td>
      <td>6</td>
      <td>39</td>
      <td>6</td>
      <td>28</td>
      <td>41</td>
      <td>31</td>
    </tr>
    <tr>
      <th>Ovary/ Sperm</th>
      <td>314</td>
      <td>1824</td>
      <td>51.0</td>
      <td>201</td>
      <td>57</td>
      <td>162</td>
      <td>37</td>
      <td>27.0</td>
      <td>164</td>
      <td>28.0</td>
      <td>...</td>
      <td>53</td>
      <td>43</td>
      <td>62</td>
      <td>45</td>
      <td>33</td>
      <td>122</td>
      <td>26</td>
      <td>90</td>
      <td>101</td>
      <td>141</td>
    </tr>
    <tr>
      <th>Brain</th>
      <td>1109</td>
      <td>3748</td>
      <td>91.0</td>
      <td>619</td>
      <td>157</td>
      <td>744</td>
      <td>123</td>
      <td>74.0</td>
      <td>361</td>
      <td>47.0</td>
      <td>...</td>
      <td>88</td>
      <td>134</td>
      <td>163</td>
      <td>107</td>
      <td>84</td>
      <td>325</td>
      <td>52</td>
      <td>236</td>
      <td>198</td>
      <td>331</td>
    </tr>
    <tr>
      <th>Cilium</th>
      <td>379</td>
      <td>2311</td>
      <td>49.0</td>
      <td>527</td>
      <td>81</td>
      <td>239</td>
      <td>49</td>
      <td>41.0</td>
      <td>186</td>
      <td>19.0</td>
      <td>...</td>
      <td>59</td>
      <td>64</td>
      <td>87</td>
      <td>62</td>
      <td>57</td>
      <td>125</td>
      <td>41</td>
      <td>114</td>
      <td>140</td>
      <td>172</td>
    </tr>
    <tr>
      <th>Liver(carboxi.met./ lipid trans)</th>
      <td>15</td>
      <td>49</td>
      <td>0.0</td>
      <td>2</td>
      <td>5</td>
      <td>15</td>
      <td>1</td>
      <td>0.0</td>
      <td>2</td>
      <td>1.0</td>
      <td>...</td>
      <td>3</td>
      <td>3</td>
      <td>2</td>
      <td>1</td>
      <td>2</td>
      <td>3</td>
      <td>2</td>
      <td>4</td>
      <td>4</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 243 columns</p>
</div>



There might be some differences in the columns of the two tables, lets fix that:


```python
# Get a list of the TFs that are found in both species:
dan_allfams = set(dan_table.columns)
bla_allfams = set(bla_table.columns)
allfams = sorted([ int(x) for x in dan_allfams.intersection(bla_allfams)])
```


```python
len(dan_allfams), len(bla_allfams),len(allfams)
```




    (243, 242, 242)



## Normalization/Scaling


```python
plt.figure()
for rowi,row in bla_table.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module before normalization')
plt.show()

# We divide each count with the sum for each TF,
# to normalize for TF promiscuity
bla_toplot = (bla_table.loc[:,allfams]/bla_table.loc[:,allfams].sum())

plt.figure()
for rowi,row in bla_toplot.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module after normalizing the columns')
plt.show()

# then scale the rows to normalize for module size
bla_toplot.loc[:,:] = scale(bla_toplot, axis=1)

plt.figure()
for rowi,row in bla_toplot.iterrows():
    sns.kdeplot(row.values)
plt.title('Distributions of counts in each module after scaling the rows')
plt.show()

dan_toplot =  (dan_table.loc[:,allfams]/dan_table.loc[:,allfams].sum())
dan_toplot.loc[:,:] = scale(dan_toplot, axis=1)      
```


![png](supplementary-figures/img/output_33_0.png)



![png](supplementary-figures/img/output_33_1.png)



![png](supplementary-figures/img/output_33_2.png)


We can now directly compare modules between the two species.     
We compute pairwise correlations next:


```python
# Get euclidean distances for each pair of modules in a table:
dists =  pd.DataFrame( PWD(dan_toplot,bla_toplot, metric="correlation") )
dists.columns = bla_toplot.index
dists.index = dan_toplot.index

dists.head(3)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>32 cells</th>
      <th>N.tube(neurotrans.)</th>
      <th>Gills</th>
      <th>N.tube(neurogen.)</th>
      <th>Ovary/ Testis(translation)</th>
      <th>Cilium</th>
      <th>Muscle</th>
      <th>8-36hpf embryo/ Skin/ Cirri(memb. synt.)</th>
      <th>8hpf embryo(transcription, spliceosome)</th>
      <th>36hpf embryo</th>
      <th>...</th>
      <th>Immune</th>
      <th>Ovary/ Eggs</th>
      <th>Gut</th>
      <th>Gut/ Hepatic</th>
      <th>Proteasome</th>
      <th>PreMet.larvae</th>
      <th>Cirri(memb. synt.)</th>
      <th>Hepatic</th>
      <th>Gills/ PreMet larvae</th>
      <th>Mitochondrion</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>larvae 7d melanin</th>
      <td>1.154636</td>
      <td>0.978651</td>
      <td>0.838089</td>
      <td>0.912212</td>
      <td>0.951865</td>
      <td>1.082635</td>
      <td>0.899440</td>
      <td>0.835883</td>
      <td>1.188712</td>
      <td>0.901015</td>
      <td>...</td>
      <td>1.161637</td>
      <td>1.077064</td>
      <td>1.000462</td>
      <td>1.028691</td>
      <td>1.079304</td>
      <td>0.996793</td>
      <td>0.997349</td>
      <td>0.990834</td>
      <td>0.962136</td>
      <td>0.985527</td>
    </tr>
    <tr>
      <th>Ovary/ Sperm</th>
      <td>1.040482</td>
      <td>0.989317</td>
      <td>1.054382</td>
      <td>1.072651</td>
      <td>1.012691</td>
      <td>0.921127</td>
      <td>1.039021</td>
      <td>0.988082</td>
      <td>0.879019</td>
      <td>1.029327</td>
      <td>...</td>
      <td>1.022339</td>
      <td>0.860245</td>
      <td>0.923210</td>
      <td>0.941424</td>
      <td>1.200813</td>
      <td>1.127407</td>
      <td>0.974054</td>
      <td>0.987378</td>
      <td>0.968299</td>
      <td>0.980205</td>
    </tr>
    <tr>
      <th>Brain</th>
      <td>1.041155</td>
      <td>0.767285</td>
      <td>0.845796</td>
      <td>0.752412</td>
      <td>1.031208</td>
      <td>1.087790</td>
      <td>0.880709</td>
      <td>1.084854</td>
      <td>1.070097</td>
      <td>0.947770</td>
      <td>...</td>
      <td>1.061158</td>
      <td>1.118625</td>
      <td>1.130662</td>
      <td>1.094315</td>
      <td>0.921916</td>
      <td>0.895495</td>
      <td>0.975830</td>
      <td>1.057731</td>
      <td>0.906599</td>
      <td>0.942674</td>
    </tr>
  </tbody>
</table>
<p>3 rows × 25 columns</p>
</div>



## The final plot:

We can now directly visualize the table with the correlations.    
We will use the clustering we got for the gene-based table earlier:


```python
# we can now visualize the TF-based distances
# we'll use the clustering from the gene-based comparison

cg = sns.clustermap( 1-dists, 
                    
                    # The previously computed clustering
                    row_linkage=linkage_HG_rows, 
                    col_linkage=linkage_HG_cols,
                    
                    linewidths=0.01,
                    figsize =(12,12),
                    
                    vmax=0.25,
                    vmin=0.15,
                    cmap = 'Reds'
                   )

_ = plt.setp(cg.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)

# plt.title('')
```


![png](supplementary-figures/img/output_37_0.png)

