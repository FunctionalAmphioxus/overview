

```python
import pandas as pd
from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
# This saves fonts as fonts in the svg and not as shapes
plt.rcParams['svg.fonttype'] = 'none'

import numpy as np
from scipy.stats import mannwhitneyu as MWU


import warnings
warnings.filterwarnings('ignore')
```


```python
def boxplot_pvals(data, x,x_anq,x_targs,y,hue,hue_order):
    for targetx in x_targs:
        for huel in hue_order:
            m1 = (data[x]==x_anq)
            mh = data[hue] == huel
            m2 = (data[x]==targetx)
            print(targetx, huel, MWU( data.loc[mh & m2, y].values, data.loc[mh & m1, y].values, alternative='greater' ).pvalue)

def boxplot_pvals_nohue(data, x,x_anq,x_targs,y):
    for targetx in x_targs:
            m1 = (data[x]==x_anq)
            m2 = (data[x]==targetx)
            print(targetx, MWU( data.loc[m2, y].values, data.loc[ m1, y].values, alternative='greater' ).pvalue)

```

## Data Load

We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream, or untill it encounters another TSS.

For the GREAT region, we extend the BASAL region UP TO 1Mb in each way, or until a BASAL region of another gene is encountered.    
     
Here, we load and use the GREAT regions


```python
greg = {}

greg['Dre'] = pd.read_csv("./data/genomic_regions/GREAT_dre.bed.gz", sep='\t', header=None)
greg['Dre'].columns = ['chrom','start','end','geneID','score','strand']
greg['Dre']['score'] = greg['Dre']['end'] - greg['Dre']['start']

greg['Bla'] = pd.read_csv("./data/genomic_regions/GREAT_bla.bed.gz", sep='\t', header=None)
greg['Bla'].columns = ['chrom','start','end','geneID','score','strand']
greg['Bla']['score'] = greg['Bla']['end'] - greg['Bla']['start']

# greg['Ola'] = pd.read_csv("./data/genomic_regions/GREAT_ola.bed.gz", sep='\t', header=None)
# greg['Ola'].columns = ['chrom','start','end','geneID','score','strand']
# greg['Ola']['score'] = greg['Ola']['end'] - greg['Ola']['start']

# greg['Mmu'] = pd.read_csv("./data/genomic_regions/GREAT_mmu.bed.gz", sep='\t', header=None)
# greg['Mmu'].columns = ['chrom','start','end','geneID','score','strand']
# greg['Mmu']['score'] = greg['Mmu']['end'] - greg['Mmu']['start']

print( [(k,len(v)) for k,v in greg.items()])
```

    [('Dre', 20053), ('Bla', 20569)]



```python
# greg['Mmu'].head()
```


```python
stages = {}
stages['Bla'] = [8,15,36]
stages['Dre'] = ['dome','shield','80epi','8som','24h','48h']
# stages['Ola'] = ["24h"]
# stages['Mmu'] = ['DE','ESC']
```

### The ATAC-seq peaks

In this, we only use a set of peaks that was selected per stage 
for belonging to the CHIPseq active cluster of ATAC peaks


```python
pre_ = './data/atac_peaks/'

peak_beds = {}

peak_beds['Dre'] = [ 
            BT(x).sort() for x in ['./data/atac_peaks_clustered_byk27/zebra_JTcluster1_{}.bed'.format(stage) for stage in stages['Dre']]
                    ]
```


```python
peak_beds['Bla'] = [ ]
for x in ['./data/atac_peaks_clustered_byk27/amphi_JTclusters_{}h.txt'.format(stage) for stage in stages['Bla']]:
    ldf = pd.read_csv(x, sep='\t', header=None)
    peak_beds['Bla'] .append(BT().from_dataframe(ldf[ldf[3]==1]).sort() )
```

### Grouping of genes into homologous-families       

We have in our disposal a precomputed table where genes of various species
are separated into homologous families.

Each row is a family, each column a species. 
The paralogues of each species are separated with ":" so when we load this dataset
we split the strings into lists.

We also create a second dataframe "genefamsC" which has the same index and shape as the first one,
but contains the count of genes in each cell.


```python
genefams = pd.read_csv("./data/gene_families_table.tsv.gz",
                      sep='\t')
genefams = genefams.applymap(lambda x: x.split(":") if x==x else x)

genefamsC = genefams.applymap(lambda x: len(x) if x==x else 0)
```


```python
genefamsC.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Bbe</th>
      <th>Bla</th>
      <th>Cmi</th>
      <th>Dre</th>
      <th>Gga</th>
      <th>Hsa</th>
      <th>Mmu</th>
      <th>Ola</th>
      <th>Sko</th>
      <th>Spu</th>
      <th>Xtr</th>
      <th>Dme</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>28</td>
      <td>36</td>
      <td>6</td>
      <td>9</td>
      <td>1</td>
      <td>4</td>
      <td>4</td>
      <td>3</td>
      <td>61</td>
      <td>8</td>
      <td>16</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Make some masks for the dataframe
mask_oto = (genefamsC['Bla']==1) & (genefamsC['Mmu']==1)  # 1-1
mask_ottw = (genefamsC['Bla']==1) & (genefamsC['Mmu']==2) # 1-2
mask_otth = (genefamsC['Bla']==1) & (genefamsC['Mmu']==3) # 1-3
mask_otfo = (genefamsC['Bla']==1) & (genefamsC['Mmu']==4) # 1-4

masks = [mask_oto, mask_ottw,mask_otth,mask_otfo]
titles = ['1-1','1-2','1-3','1-4']

# then some sets
oto_genes = genefams.loc[mask_oto,['Bla','Dre','Mmu','Ola']]
oto_genes = set([x for y in oto_genes.values.flatten() if y==y for x in y])
ottw_genes = genefams.loc[mask_ottw,['Bla','Dre','Mmu','Ola']]
ottw_genes = set([x for y in ottw_genes.values.flatten() if y==y for x in y])
otth_genes = genefams.loc[mask_otth,['Bla','Dre','Mmu','Ola']]
otth_genes = set([x for y in otth_genes.values.flatten() if y==y for x in y])
otfo_genes = genefams.loc[mask_otfo,['Bla','Dre','Mmu','Ola']]
otfo_genes = set([x for y in otfo_genes.values.flatten() if y==y for x in y])

# and use the sets to categorize the genes
def categorize(x):
    if x in oto_genes:
        return '1-1'
    elif x in ottw_genes:
        return '1-2'
    elif x in otth_genes:
        return '1-3'
    elif x in otfo_genes:
        return '1-4'
    else:
        return 'nop'
```


```python
bedfields = ['chrom','start','end','name','score','strand']
big = {}

big['Dre'] = BT().from_dataframe(greg['Dre']).sort()
for bee in peak_beds['Dre']:
    big['Dre'] = big['Dre'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Dre'] = big['Dre'].to_dataframe()
big['Dre'].columns = bedfields + stages['Dre']

# the other species:
big['Bla'] = BT().from_dataframe(greg['Bla']).sort()
for bee in peak_beds['Bla']:
    big['Bla'] = big['Bla'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Bla'] = big['Bla'].to_dataframe()
big['Bla'].columns = bedfields + stages['Bla']

# big['Ola'] = BT().from_dataframe(greg['Ola']).sort()
# for bee in peak_beds['Ola']:
#     big['Ola'] = big['Ola'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
# big['Ola'] = big['Ola'].to_dataframe()
# big['Ola'].columns = bedfields + stages['Ola']

# big['Mmu'] = BT().from_dataframe(greg['Mmu']).sort()
# for bee in peak_beds['Mmu']:
#     big['Mmu'] = big['Mmu'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
# big['Mmu'] = big['Mmu'].to_dataframe()
# big['Mmu'].columns = bedfields + stages['Mmu']
```


```python
big['Dre'].sample(5)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>dome</th>
      <th>shield</th>
      <th>80epi</th>
      <th>8som</th>
      <th>24h</th>
      <th>48h</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>17551</th>
      <td>chr7</td>
      <td>24249711</td>
      <td>24252861</td>
      <td>ENSDARG00000054805</td>
      <td>3150</td>
      <td>+</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>15312</th>
      <td>chr5</td>
      <td>6642436</td>
      <td>6897187</td>
      <td>ENSDARG00000041301</td>
      <td>254751</td>
      <td>+</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
      <td>2</td>
      <td>2</td>
      <td>6</td>
    </tr>
    <tr>
      <th>1514</th>
      <td>chr10</td>
      <td>40484092</td>
      <td>40489697</td>
      <td>ENSDARG00000096070</td>
      <td>5605</td>
      <td>-</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>6031</th>
      <td>chr16</td>
      <td>55325720</td>
      <td>55381981</td>
      <td>ENSDARG00000077559</td>
      <td>56261</td>
      <td>+</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <th>9371</th>
      <td>chr20</td>
      <td>20418121</td>
      <td>20454900</td>
      <td>ENSDARG00000029569</td>
      <td>36779</td>
      <td>-</td>
      <td>1</td>
      <td>1</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>




```python
# set the gene ID as index in all dataframes of 'big'
big_ind = {}
for k,v in big.items():
    big_ind[k] = v.set_index('name')
    big_ind[k].columns = [str(x) for x in big_ind[k].columns]
```


```python

dd_dre = big_ind['Dre'].copy()
dd_dre['category'] = dd_dre.index.to_series().map(categorize)
# dd_dre = dd_dre[dd_dre.category != 'nop']
dd_dre['species'] = 'dre'
dd_dre = dd_dre[stages['Dre']+['category','species']]
dd_dre.columns = stages['Dre']+['category','species']

# dd_ola = big_ind['Ola'].copy()
# dd_ola['category'] = dd_ola.index.to_series().map(categorize)
# # dd_ola = dd_ola[dd_ola.category != 'nop']
# dd_ola['species'] = 'ola'
# dd_ola = dd_ola[stages['Ola']+['category','species']]
# dd_ola.columns = stages['Ola']+['category','species']

dd_bla = big_ind['Bla'].copy()
dd_bla['category'] = dd_bla.index.to_series().map(categorize)
# dd_bla = dd_bla[dd_bla.category != 'nop']
dd_bla['species'] = 'bla'
dd_bla = dd_bla[[str(x) for x in stages['Bla']]+['category','species']]
dd_bla.columns = [str(x) for x in stages['Bla']]+['category','species']

# dd_mmu = big_ind['Mmu'].copy()
# dd_mmu['category'] = dd_mmu.index.to_series().map(categorize)
# # dd_mmu = dd_mmu[dd_mmu.category != 'nop']
# dd_mmu['species'] = 'mmu'
# dd_mmu = dd_mmu[stages['Mmu']+['category','species']]
# dd_mmu.columns = stages['Mmu']+['category','species']
```


```python
# then melt the DFs from each species and concatenate those
TOPLOT = pd.concat([pd.melt(dd_dre, id_vars=['category','species']),
            pd.melt(dd_bla, id_vars=['category','species']),
#             pd.melt(dd_ola, id_vars=['category','species']),
            
            
                   ])
TOPLOT.columns = ['category','species','stage','count']
TOPLOT['specstage'] = TOPLOT.species + '_' + TOPLOT.stage



TOPLOT.head(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>category</th>
      <th>species</th>
      <th>stage</th>
      <th>count</th>
      <th>specstage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>1</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
  </tbody>
</table>
</div>




```python
TOPLOT.head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>category</th>
      <th>species</th>
      <th>stage</th>
      <th>count</th>
      <th>specstage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>1</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>2</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>3</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>4</th>
      <td>nop</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
  </tbody>
</table>
</div>




```python
plt.figure(figsize=(16,9))
sns.boxplot(data = TOPLOT[TOPLOT['category'] != 'nop'], 
            
            x='specstage',
            order=["dre_dome","dre_shield","dre_80epi","dre_8som","dre_24h","dre_48h","","bla_8","bla_15","bla_36"],
#             order=stagespecorder,
#             hue='stage',
#             hue_order = ['1-1','1-2','1-3','1-4'],
            y='count',
           fliersize=0, 
              palette='Blues'
            
           )
        
plt.ylim((0,12.5))
plt.legend(loc='upper left')

```


![png](supplementary-figures/img/output_19_0.png)



```python
lot = pd.DataFrame()
for zthing in ["dre_dome","dre_shield","dre_80epi","dre_8som","dre_24h","dre_48h"]:
    for athing in ["bla_8","bla_15","bla_36"]: 
        z  = TOPLOT.loc[(TOPLOT['category'] != 'nop')  & (TOPLOT.specstage == zthing) ,'count'].values
        a  = TOPLOT.loc[(TOPLOT['category'] != 'nop')  & (TOPLOT.specstage == athing) ,'count'].values
        lot.loc[zthing, athing] = MWU(z,a, alternative='greater').pvalue
lot
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>bla_8</th>
      <th>bla_15</th>
      <th>bla_36</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>dre_dome</th>
      <td>1.485294e-13</td>
      <td>1.000000e+00</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>dre_shield</th>
      <td>9.847585e-38</td>
      <td>1.000000e+00</td>
      <td>9.998698e-01</td>
    </tr>
    <tr>
      <th>dre_80epi</th>
      <td>2.849052e-87</td>
      <td>3.872078e-01</td>
      <td>1.106593e-04</td>
    </tr>
    <tr>
      <th>dre_8som</th>
      <td>1.153358e-141</td>
      <td>2.403835e-12</td>
      <td>7.726176e-24</td>
    </tr>
    <tr>
      <th>dre_24h</th>
      <td>1.060072e-309</td>
      <td>1.281457e-92</td>
      <td>1.542833e-115</td>
    </tr>
    <tr>
      <th>dre_48h</th>
      <td>1.982380e-189</td>
      <td>8.827300e-34</td>
      <td>2.521434e-50</td>
    </tr>
  </tbody>
</table>
</div>


