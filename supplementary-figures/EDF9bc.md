

```python
import pandas as pd
import numpy as np

from glob import glob
import re

from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
import matplotlib

from scipy.stats import mannwhitneyu as MWU
from math import log
```

### Downsampling

In this analysis, we have called peaks (macs2 in each replicate, then idr peaks on the results of the macs2 peaks) in all stages, with increasingly lower number of reads. 

For example, the file in data/atac_peaks/downsampling/zebra_danRer10_8som_15600000_idr01Peaks.bed.gz    
is idr atac peaks called with 15600000 reads (in each replicate) of the reads from the 8somite zebrafish experiments which means that its coverage (15600000/avail_genome['zebra'] ) is 0.89 (we'll call it 90%) of the amphioxus coverage (9200000/ avail_genome['amphi'])

The number of reads was chosen so as to have 100%, 90%, 80% etc of the amphioxus number.    
    
The question is, do we still have more peaks in the vertebrates with worse (less reads) experiments?


```python
# assign genomic regions to the genes
greg = {}

greg['Dre'] = pd.read_csv("../data/genomic_regions/GREAT_dre.bed.gz", sep='\t', header=None)
greg['Dre'].columns = ['chrom','start','end','gene1','score','strand']
greg['Dre']['score'] = greg['Dre']['end'] - greg['Dre']['start']

greg['Bla'] = pd.read_csv("../data/genomic_regions/GREAT_bla.bed.gz", sep='\t', header=None)
greg['Bla'].columns = ['chrom','start','end','gene1','score','strand']
greg['Bla']['score'] = greg['Bla']['end'] - greg['Bla']['start']

greg['Ola'] = pd.read_csv("../data/genomic_regions/GREAT_ola.bed.gz", sep='\t', header=None)
greg['Ola'].columns = ['chrom','start','end','gene1','score','strand']
greg['Ola']['score'] = greg['Ola']['end'] - greg['Ola']['start']

greg['Mmu'] = pd.read_csv("../data/genomic_regions/GREAT_mmu.bed.gz", sep='\t', header=None)
greg['Mmu'].columns = ['chrom','start','end','gene1','score','strand']
greg['Mmu']['score'] = greg['Mmu']['end'] - greg['Mmu']['start']

print( [(k,len(v)) for k,v in greg.items()])
```

    [('Mmu', 18842), ('Bla', 20569), ('Dre', 20053), ('Ola', 15978)]



```python
stages = {}
stages['Bla'] = ['15']
stages['Dre'] = ["8som"]
stages['Ola'] = ["8som"]
```


```python
df = pd.DataFrame([['zebra', 1371719383, 1369631918, 756790655, 756666441,
        0.5517095292076951, 0.0015217872006989541, 612841263, 653072511],
       ['amphi', 495353434, 474881800, 152452412, 152231682,
        0.3077649240642995, 0.041327328317259604, 322429388, 347547588],
       ['medaka', 869000216, 700386597, 23221380, 23181494,
        0.026721949629526905, 0.19403173427979903, 677165217, 846778620]])
df.columns = ['species','genome','genome_notN','repMask','repnotN','% genome in repM','% genome is N','effective','previous_effective']
```


```python
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species</th>
      <th>genome</th>
      <th>genome_notN</th>
      <th>repMask</th>
      <th>repnotN</th>
      <th>% genome in repM</th>
      <th>% genome is N</th>
      <th>effective</th>
      <th>previous_effective</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>zebra</td>
      <td>1371719383</td>
      <td>1369631918</td>
      <td>756790655</td>
      <td>756666441</td>
      <td>0.551710</td>
      <td>0.001522</td>
      <td>612841263</td>
      <td>653072511</td>
    </tr>
    <tr>
      <th>1</th>
      <td>amphi</td>
      <td>495353434</td>
      <td>474881800</td>
      <td>152452412</td>
      <td>152231682</td>
      <td>0.307765</td>
      <td>0.041327</td>
      <td>322429388</td>
      <td>347547588</td>
    </tr>
    <tr>
      <th>2</th>
      <td>medaka</td>
      <td>869000216</td>
      <td>700386597</td>
      <td>23221380</td>
      <td>23181494</td>
      <td>0.026722</td>
      <td>0.194032</td>
      <td>677165217</td>
      <td>846778620</td>
    </tr>
  </tbody>
</table>
</div>



We will normalize the number of reads based on the amount of available genome in each species.    
That is the total genome minus the regions covered by the repeat masker.    
    
Unintuitively, medaka ends up with more available genome that zebrafish because of the very large number
of blacklisted regions in zebrafish.


```python

avail_genome = dict(df[['species','effective']].to_records(index=False))
avail_genome
```




    {'amphi': 322429388, 'medaka': 677165217, 'zebra': 612841263}




```python
# # This is how many reads the bla sample has.
# # It was much easier to hard code this number than to count in this notebook:

# We calculate coverage as the #ofReads per kilobase of available genome:
Bla_coverage = 9200000*1000/ avail_genome['amphi']
# the percentages are not exactly 70 or 80 etc, so we will force-smooth them a tiny bit to
# make the graphs better.
fixcols =[1,10,20,30,40,50,60,70,80,90,100]
```

We have the downsample series in the data subfolder, so lets load them up:


```python
greg_ = BT().from_dataframe(greg['Dre']).sort()
pre_cols = ['chrom','start','end','gene','width','strand']
cols = []
    
for thing in glob("../data/atac_peaks/downsampling/zebra_danRer10_8som_*_idr01Peaks.bed.gz"):
    reads = int(re.findall(r"[0-9]+", thing)[2])
    
    #     The coverage in this experiment:
    cov = reads*1000/avail_genome['zebra']
    #    The coverage in relation to the Bla one
    cov = round(cov*100 / Bla_coverage,2)
    cov = int(round(cov))
    cols.append( cov )
    greg_ = greg_.intersect(b=BT( thing ), nonamecheck=True, c=True)

counts_Dre = greg_.to_dataframe(names=pre_cols+cols)   
counts_Dre = counts_Dre[pre_cols+ sorted(cols)]
counts_Dre.columns = pre_cols + fixcols
dre_melt = pd.melt(counts_Dre[['gene']+fixcols], id_vars='gene')
dre_melt.columns = ['gene','pc','count']
```

Same for medaka:


```python
greg_ = BT().from_dataframe(greg['Ola']).sort()
pre_cols = ['chrom','start','end','gene','width','strand']
cols = []
    
for thing in glob("../data/atac_peaks/downsampling/medaka_8som_*_idr01Peaks.bed.gz"):
    reads = int(re.findall(r"[0-9]+", thing)[1])
    
    cov = reads*1000/avail_genome['medaka']
    cov = round(cov*100 / Bla_coverage,2)
    cov = int(round(cov))
    
    cols.append( cov )
    greg_ = greg_.intersect(b=BT( thing ), nonamecheck=True, c=True)

counts_Ola = greg_.to_dataframe(names=pre_cols+cols)   
counts_Ola = counts_Ola[pre_cols+ sorted(cols)]
counts_Ola.columns = pre_cols + fixcols
ola_melt = pd.melt(counts_Ola[['gene']+fixcols], id_vars='gene')
ola_melt.columns = ['gene','pc','count']
```

For amphioxus we'll only load two stages, the full non-downsampled one (101%) and the very minimally downsampled one (100%), which has only been downsampled so that the two replicates have the same number of reads.


```python
org = "bla"
stage='15'

full_peaks = '../data/atac_peaks/amphi_15_idrpeaks.bed.gz'
sub_peaks = '../data/atac_peaks/downsampling/amphi_15_9200000_idr01Peaks.bed.gz'


greg_ = BT().from_dataframe(greg['Bla']).sort()
cols = ['chrom','start','end','gene','width','strand']

greg_ = greg_.intersect(b=BT( full_peaks ), nonamecheck=True, c=True)
greg_ = greg_.intersect(b=BT( sub_peaks ), nonamecheck=True, c=True)

counts_Bla = greg_.to_dataframe(names=cols + [101,100])

bla_melt = pd.melt(counts_Bla[['gene',101,100]], id_vars='gene')
bla_melt.columns = ['gene','pc','count']
```


```python
bla_melt['species'] = 'bla'
ola_melt['species'] = 'ola'
dre_melt['species'] = 'dre'
```


```python
bla_melt.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene</th>
      <th>pc</th>
      <th>count</th>
      <th>species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>BL09450</td>
      <td>101</td>
      <td>10</td>
      <td>bla</td>
    </tr>
    <tr>
      <th>1</th>
      <td>BL10006</td>
      <td>101</td>
      <td>17</td>
      <td>bla</td>
    </tr>
  </tbody>
</table>
</div>




```python
ola_melt.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene</th>
      <th>pc</th>
      <th>count</th>
      <th>species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ENSORLG00000000003</td>
      <td>1</td>
      <td>0</td>
      <td>ola</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ENSORLG00000000007</td>
      <td>1</td>
      <td>0</td>
      <td>ola</td>
    </tr>
  </tbody>
</table>
</div>




```python
dre_melt.head(2)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene</th>
      <th>pc</th>
      <th>count</th>
      <th>species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ENSDARG00000063895</td>
      <td>1</td>
      <td>0</td>
      <td>dre</td>
    </tr>
    <tr>
      <th>1</th>
      <td>ENSDARG00000063899</td>
      <td>1</td>
      <td>0</td>
      <td>dre</td>
    </tr>
  </tbody>
</table>
</div>




```python
toplot = pd.concat([dre_melt, ola_melt, bla_melt])
toplot.sample(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>gene</th>
      <th>pc</th>
      <th>count</th>
      <th>species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>40920</th>
      <td>ENSDARG00000043431</td>
      <td>20</td>
      <td>3</td>
      <td>dre</td>
    </tr>
    <tr>
      <th>24092</th>
      <td>ENSORLG00000011667</td>
      <td>10</td>
      <td>4</td>
      <td>ola</td>
    </tr>
    <tr>
      <th>121324</th>
      <td>ENSDARG00000094708</td>
      <td>60</td>
      <td>0</td>
      <td>dre</td>
    </tr>
    <tr>
      <th>214599</th>
      <td>ENSDARG00000099149</td>
      <td>100</td>
      <td>14</td>
      <td>dre</td>
    </tr>
    <tr>
      <th>198075</th>
      <td>ENSDARG00000040265</td>
      <td>90</td>
      <td>1</td>
      <td>dre</td>
    </tr>
  </tbody>
</table>
</div>




```python
plt.figure(figsize=(16,9))



sns.boxplot(
    data=toplot,
    
    x = 'species',
    order= ['bla','dre','ola'],
    hue='pc',
    hue_order=[101,100,90,80,70,60,50,40,30,20,10,0],
    
    fliersize=0,
    y='count',
    palette='Blues_r'
)

plt.ylim((0,25))
plt.legend(loc='upper left')
```




    <matplotlib.legend.Legend at 0x6ac5f10>




![png](supplementary-figures/img/output_20_1.png)


Let's calculate some PValues:


```python
lot = []
blavals = toplot.loc[(toplot.species=='bla') & (toplot.pc==101),'count'].values
blavals_sub = toplot.loc[(toplot.species=='bla') & (toplot.pc==100),'count'].values

for gn,g in toplot.groupby(['species','pc']):
    species,pc = gn
    if species=='bla':
        continue
    pv = MWU( g['count'].values , blavals, alternative='greater' ).pvalue
    pv_sub = MWU( g['count'].values , blavals_sub, alternative='greater' ).pvalue
    
    lot.append([ species, pc, pv_sub,pv])
    
    
df = pd.DataFrame(lot)
df.columns = ['species','% of reads','pVal to downsampled Amphi','pVal to full Amphi']
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>species</th>
      <th>% of reads</th>
      <th>pVal to downsampled Amphi</th>
      <th>pVal to full Amphi</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>dre</td>
      <td>1</td>
      <td>1.000000e+00</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>1</th>
      <td>dre</td>
      <td>10</td>
      <td>1.000000e+00</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>2</th>
      <td>dre</td>
      <td>20</td>
      <td>5.210865e-20</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>3</th>
      <td>dre</td>
      <td>30</td>
      <td>2.131912e-37</td>
      <td>9.985630e-01</td>
    </tr>
    <tr>
      <th>4</th>
      <td>dre</td>
      <td>40</td>
      <td>3.859252e-283</td>
      <td>1.817653e-94</td>
    </tr>
    <tr>
      <th>5</th>
      <td>dre</td>
      <td>50</td>
      <td>0.000000e+00</td>
      <td>3.299567e-283</td>
    </tr>
    <tr>
      <th>6</th>
      <td>dre</td>
      <td>60</td>
      <td>0.000000e+00</td>
      <td>2.732288e-240</td>
    </tr>
    <tr>
      <th>7</th>
      <td>dre</td>
      <td>70</td>
      <td>0.000000e+00</td>
      <td>3.523587e-268</td>
    </tr>
    <tr>
      <th>8</th>
      <td>dre</td>
      <td>80</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>9</th>
      <td>dre</td>
      <td>90</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>10</th>
      <td>dre</td>
      <td>100</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>11</th>
      <td>ola</td>
      <td>1</td>
      <td>1.000000e+00</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>12</th>
      <td>ola</td>
      <td>10</td>
      <td>6.362505e-01</td>
      <td>1.000000e+00</td>
    </tr>
    <tr>
      <th>13</th>
      <td>ola</td>
      <td>20</td>
      <td>0.000000e+00</td>
      <td>2.094861e-171</td>
    </tr>
    <tr>
      <th>14</th>
      <td>ola</td>
      <td>30</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>15</th>
      <td>ola</td>
      <td>40</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>16</th>
      <td>ola</td>
      <td>50</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>17</th>
      <td>ola</td>
      <td>60</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>18</th>
      <td>ola</td>
      <td>70</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>19</th>
      <td>ola</td>
      <td>80</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>20</th>
      <td>ola</td>
      <td>90</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
    <tr>
      <th>21</th>
      <td>ola</td>
      <td>100</td>
      <td>0.000000e+00</td>
      <td>0.000000e+00</td>
    </tr>
  </tbody>
</table>
</div>


