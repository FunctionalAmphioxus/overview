

```python
import pandas as pd
from pybedtools import BedTool as BT

%matplotlib inline
import seaborn as sns
from matplotlib import pyplot as plt
# This saves fonts as fonts in the svg and not as shapes
plt.rcParams['svg.fonttype'] = 'none'

import numpy as np
from scipy.stats import mannwhitneyu as MWU

import warnings
warnings.filterwarnings('ignore')
```

## Data Load

We assign a BASAL region to each gene, it starts from its TSS and extends 5kb upstream and 1kb downstream, or untill it encounters another TSS.

For the GREAT region, we extend the BASAL region UP TO 1Mb in each way, or until a BASAL region of another gene is encountered.    
     
Here, we load and use the GREAT regions


```python
greg = {}

greg['Dre'] = pd.read_csv("./data/genomic_regions/GREAT_dre.bed.gz", sep='\t', header=None)
greg['Dre'].columns = ['chrom','start','end','geneID','score','strand']
greg['Dre']['score'] = greg['Dre']['end'] - greg['Dre']['start']

greg['Bla'] = pd.read_csv("./data/genomic_regions/GREAT_bla.bed.gz", sep='\t', header=None)
greg['Bla'].columns = ['chrom','start','end','geneID','score','strand']
greg['Bla']['score'] = greg['Bla']['end'] - greg['Bla']['start']

greg['Ola'] = pd.read_csv("./data/genomic_regions/GREAT_ola.bed.gz", sep='\t', header=None)
greg['Ola'].columns = ['chrom','start','end','geneID','score','strand']
greg['Ola']['score'] = greg['Ola']['end'] - greg['Ola']['start']

greg['Mmu'] = pd.read_csv("./data/genomic_regions/GREAT_mmu.bed.gz", sep='\t', header=None)
greg['Mmu'].columns = ['chrom','start','end','geneID','score','strand']
greg['Mmu']['score'] = greg['Mmu']['end'] - greg['Mmu']['start']

print( [(k,len(v)) for k,v in greg.items()])
```

    [('Dre', 20053), ('Bla', 20569), ('Ola', 15978), ('Mmu', 18842)]



```python
greg['Mmu'].head()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>geneID</th>
      <th>score</th>
      <th>strand</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>chr1</td>
      <td>2670503</td>
      <td>4359310</td>
      <td>ENSMUSG00000051951</td>
      <td>1688807</td>
      <td>-</td>
    </tr>
    <tr>
      <th>1</th>
      <td>chr1</td>
      <td>3676503</td>
      <td>4495409</td>
      <td>ENSMUSG00000025900</td>
      <td>818906</td>
      <td>-</td>
    </tr>
    <tr>
      <th>2</th>
      <td>chr1</td>
      <td>4365319</td>
      <td>4784706</td>
      <td>ENSMUSG00000025902</td>
      <td>419387</td>
      <td>-</td>
    </tr>
    <tr>
      <th>3</th>
      <td>chr1</td>
      <td>4501418</td>
      <td>4802819</td>
      <td>ENSMUSG00000033845</td>
      <td>301401</td>
      <td>-</td>
    </tr>
    <tr>
      <th>4</th>
      <td>chr1</td>
      <td>4790715</td>
      <td>4852810</td>
      <td>ENSMUSG00000025903</td>
      <td>62095</td>
      <td>+</td>
    </tr>
  </tbody>
</table>
</div>




```python
stages = {}
stages['Bla'] = ['8','15','36','60']
stages['Dre'] = ["dome","shield","80epi","8som","24h","48h"]
stages['Ola'] = ["dome","shield","8som","24h","48h"]
stages['Mmu'] = ['DE','ESC']
```

### The ATAC-seq peaks

As determined by the idr "pipeline", see more in the peak-calling notebook


```python
pre_ = './data/atac_peaks/'

peak_beds = {}

peak_beds['Dre'] = [ (BT("{}zebra_danRer10_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Dre']]
peak_beds['Bla'] = [(BT("{}amphi_{}_idrpeaks.bed.gz".format(pre_,x))
                     .sort()
                     ) for x in stages['Bla']]
peak_beds['Ola'] = [ (BT("{}medaka_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Ola']]
peak_beds['Mmu'] = [ (BT("{}mouse_{}_idrpeaks.bed.gz".format(pre_,x))
                    .sort()
                    )for x in stages['Mmu']]

```

### Grouping of genes into House Keeping, TransDev, Other      


```python
TDG = pd.read_csv("./data/TransDevGenes.tab.gz", sep='\t', header=None)
TDG.columns = ['GFid','species','geneID','newGFid','geneName']
TDG = TDG[TDG['species'].isin(['Bla','Dre','Ola','Mmu'])]
TDgenes = set(TDG.geneID.unique())
```


```python
HK = pd.read_csv("./data/HouseKeepGenes.tab.gz", sep='\t', header=None)
HK.columns = ['GFid','species','geneID','newGFid','geneName']
HK = HK[HK['species'].isin(['Bla','Dre','Ola','Mmu'])]
HKgenes = set(HK.geneID.unique())
```


```python
bedfields = ['chrom','start','end','name','score','strand']
big = {}

big['Dre'] = BT().from_dataframe(greg['Dre']).sort()
for bee in peak_beds['Dre']:
    big['Dre'] = big['Dre'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Dre'] = big['Dre'].to_dataframe()
big['Dre'].columns = bedfields + stages['Dre']

# the other species:
big['Bla'] = BT().from_dataframe(greg['Bla']).sort()
for bee in peak_beds['Bla']:
    big['Bla'] = big['Bla'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Bla'] = big['Bla'].to_dataframe()
big['Bla'].columns = bedfields + stages['Bla']

big['Ola'] = BT().from_dataframe(greg['Ola']).sort()
for bee in peak_beds['Ola']:
    big['Ola'] = big['Ola'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Ola'] = big['Ola'].to_dataframe()
big['Ola'].columns = bedfields + stages['Ola']

big['Mmu'] = BT().from_dataframe(greg['Mmu']).sort()
for bee in peak_beds['Mmu']:
    big['Mmu'] = big['Mmu'].intersect(b = bee, c=True, sorted=True, nonamecheck=True)
big['Mmu'] = big['Mmu'].to_dataframe()
big['Mmu'].columns = bedfields + stages['Mmu']
```


```python
big['Dre'].sample(5)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>chrom</th>
      <th>start</th>
      <th>end</th>
      <th>name</th>
      <th>score</th>
      <th>strand</th>
      <th>dome</th>
      <th>shield</th>
      <th>80epi</th>
      <th>8som</th>
      <th>24h</th>
      <th>48h</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>16114</th>
      <td>chr5</td>
      <td>58442259</td>
      <td>58559708</td>
      <td>ENSDARG00000097055</td>
      <td>117449</td>
      <td>+</td>
      <td>10</td>
      <td>10</td>
      <td>10</td>
      <td>10</td>
      <td>16</td>
      <td>21</td>
    </tr>
    <tr>
      <th>10149</th>
      <td>chr21</td>
      <td>14083062</td>
      <td>14212426</td>
      <td>ENSDARG00000073792</td>
      <td>129364</td>
      <td>-</td>
      <td>7</td>
      <td>6</td>
      <td>10</td>
      <td>17</td>
      <td>12</td>
      <td>14</td>
    </tr>
    <tr>
      <th>5343</th>
      <td>chr16</td>
      <td>11834333</td>
      <td>11897202</td>
      <td>ENSDARG00000058047</td>
      <td>62869</td>
      <td>-</td>
      <td>4</td>
      <td>6</td>
      <td>5</td>
      <td>5</td>
      <td>5</td>
      <td>3</td>
    </tr>
    <tr>
      <th>7436</th>
      <td>chr19</td>
      <td>85505</td>
      <td>102195</td>
      <td>ENSDARG00000105003</td>
      <td>16690</td>
      <td>+</td>
      <td>3</td>
      <td>4</td>
      <td>5</td>
      <td>4</td>
      <td>3</td>
      <td>9</td>
    </tr>
    <tr>
      <th>16992</th>
      <td>chr6</td>
      <td>45908869</td>
      <td>45925390</td>
      <td>ENSDARG00000060153</td>
      <td>16521</td>
      <td>+</td>
      <td>6</td>
      <td>5</td>
      <td>6</td>
      <td>8</td>
      <td>6</td>
      <td>8</td>
    </tr>
  </tbody>
</table>
</div>




```python
# set the gene ID as index in all dataframes of 'big'
big_ind = {}
for k,v in big.items():
    big_ind[k] = v.set_index('name')
    big_ind[k].columns = [str(x) for x in big_ind[k].columns]
```


```python
def categorize(x):
    if x in TDgenes:
        return 'TD'
    elif x in HKgenes:
        return 'HK'
    else:
        return 'Other'
```


```python

dd_dre = big_ind['Dre'].copy()
dd_dre['category'] = dd_dre.index.to_series().map(categorize)
# dd_dre = dd_dre[dd_dre.category != 'nop']
dd_dre['species'] = 'dre'
dd_dre = dd_dre[stages['Dre']+['category','species']]
dd_dre.columns = stages['Dre']+['category','species']

dd_ola = big_ind['Ola'].copy()
dd_ola['category'] = dd_ola.index.to_series().map(categorize)
# dd_ola = dd_ola[dd_ola.category != 'nop']
dd_ola['species'] = 'ola'
dd_ola = dd_ola[stages['Ola']+['category','species']]
dd_ola.columns = stages['Ola']+['category','species']

dd_bla = big_ind['Bla'].copy()
dd_bla['category'] = dd_bla.index.to_series().map(categorize)
# dd_bla = dd_bla[dd_bla.category != 'nop']
dd_bla['species'] = 'bla'
dd_bla = dd_bla[stages['Bla']+['category','species']]
dd_bla.columns = stages['Bla']+['category','species']

dd_mmu = big_ind['Mmu'].copy()
dd_mmu['category'] = dd_mmu.index.to_series().map(categorize)
# dd_mmu = dd_mmu[dd_mmu.category != 'nop']
dd_mmu['species'] = 'mmu'
dd_mmu = dd_mmu[stages['Mmu']+['category','species']]
dd_mmu.columns = stages['Mmu']+['category','species']
```


```python
# then melt the DFs from each species and concatenate those
TOPLOT = pd.concat([pd.melt(dd_dre, id_vars=['category','species']),
            pd.melt(dd_bla, id_vars=['category','species']),
            pd.melt(dd_ola, id_vars=['category','species']),
            pd.melt(dd_mmu, id_vars=['category','species'])
            
                   ])
TOPLOT.columns = ['category','species','stage','count']
TOPLOT['specstage'] = TOPLOT.species + '_' + TOPLOT.stage
TOPLOT.head(2)
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>category</th>
      <th>species</th>
      <th>stage</th>
      <th>count</th>
      <th>specstage</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Other</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
    <tr>
      <th>1</th>
      <td>Other</td>
      <td>dre</td>
      <td>dome</td>
      <td>0</td>
      <td>dre_dome</td>
    </tr>
  </tbody>
</table>
</div>




```python
stagespecorder = ['bla_8','bla_15','bla_36','bla_60',' ',
             'dre_dome','dre_shield','dre_80epi','dre_8som','dre_24h','dre_48h',' ',
             'ola_dome','ola_shield','ola_8som','ola_24h','ola_48h',' ',
             'mmu_ESC','mmu_DE'
            ]
```


```python
plt.figure(figsize=(21,9))
sns.boxplot(data = TOPLOT, 
            
            x='specstage',
            order=stagespecorder,
            hue='category',
            hue_order = ['HK','Other','TD'],
            y='count',
           fliersize=0, palette='Blues'
            
           )
        
plt.ylim((0,60))
plt.legend(loc='upper left')
```




    <matplotlib.legend.Legend at 0x7efd2bf1fcc0>




![png](supplementary-figures/img/output_17_1.png)

