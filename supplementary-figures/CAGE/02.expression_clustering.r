library(CAGEr)
library(dplyr)
library(ggplot2)
library(data.table)
library(BSgenome.braLan.Lenhard.braLan2)
library(rtracklayer)

# clustering is a random process
set.seed(100316)

consensus_clusters = fread("results/consensus_clusters_tpm.txt")
### FUNCTIONS MODIFYING CAGER ###

plot_clusters = function(df, fn, x=5, y=5, cl = NULL) {
    if (is.null(cl)) cl = CAGEr:::.clusterExpression(df, colnames(df), 5, 1, "som", x, y)
    mat = t(scale(t(log(df+1)), center=F))
    cl_info = CAGEr:::.extract.cluster.info(cl=setNames(cl[[1]], which(cl[[2]])))
    png(fn, 600*x, 300+540*y)
    CAGEr:::.plot.clusters.beanplots(mat[cl[[2]],], cl_info, "som", x, y, cex.axis=3, las=2)
## c3 = plot_clusters_pdf(select(consensus_clusters, Embr_32cell:FemGonads, -Muscle, -Hepatic, -FemGonads), "expression_clusters/som_3.pdf")
## c4 = plot_clusters_pdf(select(consensus_clusters, Embr_32cell:FemGonads, -Muscle), "expression_clusters/som_4.pdf", 4, 4)

stop()

consensus_clusters$cid = c1

gr = with(consensus_clusters, {
     GRanges(chr,
             IRanges(dom.ctss-1, dom.ctss),
             strand=strand,
             name=paste0("consensus_cluster_", consensus.cluster),
             cluster=cid,
             iq_range=end-start,
             iq_10=start,
             iq_90=end,
             seqinfo=seqinfo(braLan2))
})

gr$category = "None"

gr[gr$cluster == "0_0"]$category = "hepatic"
gr[gr$cluster == "0_4"]$category = "neural_tube"
gr[gr$cluster == "3_4"]$category = "32_cell"
gr[gr$cluster == "4_4"]$category = "embryo_high"
gr[gr$cluster == "4_3"]$category = "late_embryo"
gr[gr$cluster == "4_0"]$category = "fem_gonads"
gr[gr$cluster %in%  c("2_3", "2_2", "3_2", "2_1")]$category = "ubiquitous"

write.table(as.data.frame(gr), "results/consensus_clusters_partition.txt",
row.names=F, col.names=T, quote=F, sep="\t")
df = data.frame(iq=gr$iq_90-gr$iq_10, cluster=gr$cluster)
df = filter(df, cluster != ".") %>%
    mutate(c_x=tstrsplit(cluster, "_")[[1]]) %>%
    mutate(c_y=factor(tstrsplit(cluster, "_")[[2]], levels=c(4:0)))

ggplot(df, aes(x=iq)) + geom_density(adjust=0.5) + facet_grid(c_y ~ c_x) + xlim(0,100)

