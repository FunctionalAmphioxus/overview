library(dplyr)
library(data.table)
library(RColorBrewer)
library(rtracklayer)
library(GenomicRanges)
library(BSgenome.braLan.Lenhard.braLan2)
library(heatmaps)

system("mkdir -p heatmaps")

cgi_df = fread("./data/cgi_blan.txt")
cgi = makeGRangesFromDataFrame(cgi_df, keep.extra.columns=TRUE, seqinfo=seqinfo(braLan2))

cgi_ucsc_df = filter(cgi_df, obsExp > 0.6, length >= 200)
cgi_ucsc = makeGRangesFromDataFrame(cgi_ucsc_df, keep.extra.columns=TRUE, seqinfo=seqinfo(braLan2))

proms_df = fread("results/consensus_clusters_partition.txt")
proms = makeGRangesFromDataFrame(proms_df, keep.extra.columns=TRUE, seqinfo=seqinfo(braLan2))

iq=proms
start(iq) = proms$iq_10
end(iq) = proms$iq_90
strand(iq) = "*"

coords=c(-500, 500)

gg_colour_hue <- function(n) {
    hues = seq(15, 375, length=n+1)
    hcl(h=hues, l=65, c=100)[1:n]
}

for (ts in c("embryo_high", "hepatic", "neural_tube", "fem_gonads", "ubiquitous")) {
    message(ts)
    bed = subset(proms, category == ts)
    bed_sorted = bed[order(bed$iq_range, decreasing=TRUE)]
    windows = promoters(bed_sorted, -coords[1], coords[2])
    windows = trim(windows)
    windows = windows[width(windows) == 1000]
    message("cpgs")
    cpg = CoverageHeatmap(windows, cgi, label="CGI", coords=coords)
    cpg_s = smooth(cpg)
    message("iq")
    iq_hm = CoverageHeatmap(windows, iq, label="IQ", coords=coords)
    message("seq")
    seq = getSeq(braLan2, windows)
    message("patterns")
    patterns = lapply(c("TA", "CG", "WW", "SS"), getPatternOccurrence, seq=seq, coords=coords)
    patterns_s = lapply(patterns, smooth)
    message("plotting")
    png(file.path("heatmaps", paste0(ts, ".png")), width=9300, height=3000)
    plotHeatmapList(c(patterns_s, iq_hm, cpg_s),
                    groups=c(1, 1, 2, 2, 3, 4),
                    color=list(brewer.pal(9, "Blues"), brewer.pal(9, "Blues"), brewer.pal(3, "Blues"), brewer.pal(9, "YlOrRd")))
    dev.off()
    pdf(file.path("plots", paste0(ts, ".pdf")), width=12, height=8)
    plotHeatmapMeta(patterns, 1, gg_colour_hue(4))
    dev.off()
}

for (ts in c("32_cell", "late_embryo")) {
    message(ts)
    bed = subset(proms, category == ts)
    bed_sorted = bed[order(bed$iq_range, decreasing=TRUE)]
    windows = promoters(bed_sorted, -coords[1], coords[2])
    windows = trim(windows)
    windows = windows[width(windows) == 1000]
    message("cpgs")
    cpg = CoverageHeatmap(windows, cgi, label="CGI", coords=coords)
    cpg_s = smooth(cpg)
    message("iq")
    iq_hm = CoverageHeatmap(windows, iq, label="IQ", coords=coords)
    message("seq")
    seq = getSeq(braLan2, windows)
    message("patterns")
    patterns = lapply(c("TA", "CG", "WW", "SS"), getPatternOccurrence, seq=seq, coords=coords)
    patterns_s = lapply(patterns, smooth)
    message("plotting")
    png(file.path("plots", paste0(ts, ".png")), width=9300, height=1500)
    plotHeatmapList(c(patterns_s, iq_hm, cpg_s),
                    groups=c(1, 1, 2, 2, 3, 4),
                    color=list(brewer.pal(9, "Blues"), brewer.pal(9, "Blues"), brewer.pal(3, "Blues"), brewer.pal(9, "YlOrRd")))
    dev.off()
}


