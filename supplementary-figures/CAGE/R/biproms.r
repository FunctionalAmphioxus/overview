library(rtracklayer)
library(GenomicRanges)
library(dplyr)
library(data.table)

readBedGraph = function(bdg, seq_info=NULL) {
    ucsc_data = import(bdg)
    if (!is.null(seq_info)) {
        seqlevels(ucsc_data) = seqlevels(seq_info)
        seqinfo(ucsc_data) = seq_info
    }
    rle = coverage(ucsc_data, weight="score")
    return(rle)
}

windowSums = function(windows, rle) {
    myViews = Views(rle, as(windows, "RangesList")) # should throw error on dangerous behaviour
    scores = sapply(myViews, function(x) viewApply(x,sum))
    return(as.vector(unlist(scores)))
}

cageSignal = function(cage, pos, neg) {
    cage_plus = subset(cage, as.vector(strand(cage) == "+"))
    cage_minus = subset(cage, as.vector(strand(cage) == "-"))
    signal_plus = windowSums(cage_plus, pos)
    signal_minus = windowSums(cage_minus, neg)
    signal = rep(NA, length(cage))
    signal[as.vector(strand(cage) == "+")] = signal_plus
    signal[as.vector(strand(cage) == "-")] = signal_minus
    return(signal)
}


isBackToBack = function(gr, strand=c("+", "-")) {
    strand = match.arg(strand)
    strand_plus_one = strand(gr)[2:length(gr)]
    strand_minus_one = strand(gr)[1:(length(gr)-1)]
    seqnames_plus_one = seqnames(gr)[2:length(gr)]
    seqnames_minus_one = seqnames(gr)[1:(length(gr)-1)]
    btb = as.vector(strand_minus_one == "-" &
                    strand_plus_one == "+" &
                    seqnames_plus_one == seqnames_minus_one)
    if (strand=="-") return(c(btb, FALSE))
    else return(c(FALSE, btb))
}

isHeadToHead = function(gr, strand=c("+", "-")) {
    strand = match.arg(strand)
    strand_plus_one = strand(gr[2:length(gr)])
    strand_minus_one = strand(gr[1:(length(gr))-1])
    seqnames_plus_one = seqnames(gr[2:length(gr)])
    seqnames_minus_one = seqnames(gr[1:(length(gr))-1])
    hth = as.vector(strand_minus_one == "+" &
                    strand_plus_one == "-" &
                    seqnames_plus_one == seqnames_minus_one)
    if (strand=="-") return(c(hth, FALSE))
    else return(c(FALSE, hth))
}

distanceToNext = function(gr) {
    start_plus_one = start(gr[2:length(gr)])
    end_minus_one = end(gr[1:(length(gr))-1])
    seqnames_plus_one = seqnames(gr[2:length(gr)])
    seqnames_minus_one = seqnames(gr[1:(length(gr))-1])
    dtn = ifelse(seqnames_minus_one == seqnames_plus_one,
                 start_plus_one - end_minus_one, Inf)
    dtn = c(dtn, Inf) # no dtn for final promoter
    return(dtn)
}

makeBiPromRegions = function(bi_proms, seq_info=NULL) {
    GRanges(seqnames(subset(bi_proms, strand=="-")),
            IRanges(start(subset(bi_proms, strand=="-")),
                    end(subset(bi_proms, strand=="+"))),
            strand="*", seqinfo=seq_info)
}

readCageCTSS = function(fn, seq_info) {
    dframe = fread(fn)
    dframe = rename(dframe, iq_10=start, iq_90=end, start=dominant.ctss)
    dframe$end = dframe$start
    dframe = arrange(dframe, seqnames, start)
    dframe$id = 1:nrow(dframe)
    gr = makeGRangesFromDataFrame(dframe, keep.extra.columns=TRUE)
    seqlevels(gr) = seqlevels(seq_info)
    seqinfo(gr) = seq_info
    return(gr)
}

readCageIQ = function(fn, seq_info) {
    dframe = fread(fn)
    dframe = arrange(dframe, seqnames, dominant.ctss)
    dframe$id = 1:nrow(dframe)
    gr = makeGRangesFromDataFrame(dframe, keep.extra.columns=TRUE)
    seqlevels(gr) = seqlevels(seq_info)
    seqinfo(gr) = seq_info
    return(gr)
}

both = function(x) {
  odd = x[c(T,F)]
  even = x[c(F,T)]
  joint = 2*(which(odd & even))-1
  x[] = FALSE
  x[c(joint, joint+1)] = TRUE
  x
}

is_duplicated = function(x) {
  x %in% x[duplicated(x)]
}

filterBiProms = function(x, d=1000) {
    x = sort(x, ignore.strand = TRUE)
    starts = which(isBackToBack(x, "-") & distanceToNext(x) <= 1000)
    x[sort(c(starts, starts+1))]
}
