
consensusClustersDominantCTSS = function(cage_set) {
    tcicc = cage_set@tagClustersInConsensusClusters
    tc = cage_set@tagClusters
    expr = cage_set@consensusClustersTpmMatrix
    max_expr = colnames(expr)[apply(expr, 1, which.max)]
    tcicc$cl_max = max_expr[match(tcicc$consensus.cluster, 1:length(max_expr))]
    tcicc = filter(tcicc, sample==cl_max)
    tcicc$tpm = 0
    tcicc$dom.ctss = 0
    for (sample in colnames(expr)) {
        tag_clusters = tc[[sample]]
        mapping = match(tcicc[tcicc$cl_max==sample,]$cluster, tag_clusters$cluster)
        tcicc$tpm[tcicc$cl_max==sample] = tag_clusters$tpm[mapping]
        tcicc$dom.ctss[tcicc$cl_max==sample] = tag_clusters$dominant_ctss[mapping]
    }
    cc_tc = group_by(tcicc, consensus.cluster) %>% filter(tpm == max(tpm))
    cc_tc = cc_tc[!duplicated(cc_tc$consensus.cluster),]
    cc = consensusClusters(cage_set)
    cc$dom.ctss = cc_tc$dom.ctss[match(cc$consensus.cluster, cc_tc$consensus.cluster)]
    cc = cc[!is.na(cc$dom.ctss),] # remove wierd non-supported CCs
    return(cc)
}

