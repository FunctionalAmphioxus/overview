library(BSgenome.Mmusculus.UCSC.mm9)
library(BSgenome.Drerio.UCSC.danRer7)
library(BSgenome.Dmelanogaster.UCSC.dm3)
library(BSgenome.braLan.Lenhard.braLan2)
library(GenomicRanges)
library(readr)
library(data.table) # readr chokes on 9e+05
library(dplyr)
library(ggplot2)
library(heatmaps)
library(RColorBrewer)
library(org.Hs.eg.db)
library(org.Mm.eg.db)
library(org.Dr.eg.db)
library(org.Dm.eg.db)
source("R/go_enrichment.r")

# annotate amphi with hgnc

amphi_cage = read_tsv("results/consensus.clusters.annotated.txt")
## annotation = import("./data/Bla_annot-FINAL_v4_names.gtf")
amphi_cage$hgnc_symbol = annotation$gene_name[match(amphi_cage$gene_id, annotation$gene_id)]
amphi_cage$gene_biotype = annotation$source[match(amphi_cage$gene_id, annotation$gene_id)]
amphi_cage_fn = "plots/amphi.consensus.clusters.annotated.txt"
write_tsv(amphi_cage, amphi_cage_fn)

organisms = list(
  amphioxus=list(
    name="amphioxus",
    cage=amphi_cage_fn,
    converter=org.Hs.egSYMBOL,
    annotation="org.Hs.eg.db",
    gene_id="hgnc_symbol", # id for calculating GO enrichment
    genome=braLan2
  ),
  mouse=list(
    name="mouse",
    cage="mouse/mm9.consensus.clusters.annotated.txt",
    converter=org.Mm.egENSEMBL,
    annotation="org.Mm.eg.db",
    gene_id="ensembl_gene_id",
    genome=Mmusculus
  ),
  zebrafish=list(
    name="zebrafish",
    cage="zebrafish/danRer7.consensus.clusters.annotated.txt",
    converter=org.Dr.egENSEMBL,
    annotation="org.Dr.eg.db",
    gene_id="ensembl_gene_id",
    genome=Drerio
  ),
  fly=list(
    name="fly",
    cage="fly/dm3.consensus.clusters.annotated.txt",
    converter=org.Dm.egENSEMBL,
    annotation="org.Dm.eg.db",
    gene_id="gene_id",
    genome=Dmelanogaster
  )
 )

organisms = organisms[1] # only rerun for amphi

mkdirp = function(x) {
  dir.create(x, showWarnings = FALSE)
}

makeBiPromRegions = function(bi_proms, seq_info=NULL) {
    GRanges(seqnames(subset(bi_proms, strand=="-")),
            IRanges(start(subset(bi_proms, strand=="-")),
                    end(subset(bi_proms, strand=="+"))),
            strand="*", seqinfo=seq_info)
}

filterMultiTSS = function(x) {
    keep_pos = !duplicated(x$gene_id) & strand(x) == "+"
    keep_neg = rev(!duplicated(rev(x$gene_id)) & rev(strand(x)) == "-")
    x[keep_pos | keep_neg]
}

filterBiProms = function(x, d=1000) {
    x = sort(x, ignore.strand = TRUE)
    starts = which(isBackToBack(x, "-") & distanceToNext(x) <= 1000)
    x[sort(c(starts, starts+1))]
}

isBackToBack = function(gr, strand=c("+", "-")) {
    strand = match.arg(strand)
    strand_plus_one = strand(gr)[2:length(gr)]
    strand_minus_one = strand(gr)[1:(length(gr)-1)]
    seqnames_plus_one = seqnames(gr)[2:length(gr)]
    seqnames_minus_one = seqnames(gr)[1:(length(gr)-1)]
    btb = as.vector(strand_minus_one == "-" &
                    strand_plus_one == "+" &
                    seqnames_plus_one == seqnames_minus_one)
    if (strand=="-") return(c(btb, FALSE))
    else return(c(FALSE, btb))
}

distanceToNext = function(gr) {
    start_plus_one = start(gr[2:length(gr)])
    end_minus_one = end(gr[1:(length(gr))-1])
    seqnames_plus_one = seqnames(gr[2:length(gr)])
    seqnames_minus_one = seqnames(gr[1:(length(gr))-1])
    dtn = ifelse(seqnames_minus_one == seqnames_plus_one,
                 start_plus_one - end_minus_one, Inf)
    dtn = c(dtn, Inf) # no dtn for final promoter
    return(dtn)
}

filterBiProms_protein_coding = function(x, d=1000) {
  biproms_pc = filterBiProms(x[x$gene_biotype == "protein_coding" & !is.na(x$gene_biotype)])
  filterBiProms(filterMultiTSS(biproms_pc))
}

ENTREZ = function(id, conv) {
  id = id[id %in% mappedkeys(revmap(conv))]
  unlist(mget(unique(id), revmap(conv)))
}

for (info in organisms) {
  print(paste("Running biprom analysis for", info$name))
  dir.create(file.path("plots", info$name), showWarnings = FALSE)

  df = fread(info$cage) %>% as_data_frame
  df$gene_id = df[[info$gene_id]]

  if (info$name == "fly") {
    df = df %>% rename(gene_biotype=source)
  }

  CAGE_clusters = makeGRangesFromDataFrame(df, keep.extra.columns=TRUE,
                                           seqinfo=seqinfo(info$genome))

  CAGE = df %>%
    mutate(iq_10 = start, iq_90 = end) %>%
    mutate(start = dom.ctss, end = dom.ctss) %>%
    makeGRangesFromDataFrame(keep.extra.columns=TRUE) %>%
    sort(ignore.strand=TRUE)

  # keep the last TSS in a linear order
  bi_proms = filterBiProms(CAGE)
  bi_proms_p = filterBiProms_protein_coding(CAGE)

  write_tsv(as.data.frame(bi_proms), file.path("plots", info$name, "bidirectional_promoters_1kb.txt"))
  write_tsv(as.data.frame(bi_proms_p), file.path("plots", info$name, "bidirectional_promoters_1kb_pc.txt"))

  ### GO ENRICHMENT ###

  universe_ids = mcols(subset(CAGE, gene_biotype=="protein_coding"))[[info$gene_id]]
  universe = ENTREZ(universe_ids, info$converter)
  bp_ids = mcols(subset(bi_proms, gene_biotype=="protein_coding"))[[info$gene_id]]
  all_bp = ENTREZ(bp_ids, info$converter)
  pc_ids = mcols(subset(bi_proms_p, gene_biotype=="protein_coding"))[[info$gene_id]]
  bp_pc = ENTREZ(pc_ids, info$converter)

  go_df = go_enrichment(bp_pc, universe, annotation=info$annotation)
  write_tsv(go_df, file.path("plots", info$name, "go_enrichment_biproms.txt"))

  go_df_under = go_enrichment(bp_pc, universe, annotation=info$annotation, testDirection="under")
  write_tsv(go_df_under, file.path("plots", info$name, "go_exclusion_biproms.txt"))

  ### HEATMAPS ###

  biprom_regions = makeBiPromRegions(bi_proms_p, seqinfo(info$genome))
  pdf(file.path("plots", info$name, "lengths.pdf"), width=8, height=5)
  qplot(width(biprom_regions), geom="histogram")
  dev.off()

  biprom_regions$bp_width = width(biprom_regions)
  strand(biprom_regions) = "+"
  biprom_neg = trim(promoters(biprom_regions, 800, 1200))
  biprom_neg = biprom_neg[width(biprom_neg) == 2000]
  biprom_neg = biprom_neg[order(biprom_neg$bp_width, decreasing=TRUE)]

  biprom_neg_seq = getSeq(info$genome, biprom_neg)

  ww = smoothHeatmap(PatternHeatmap(biprom_neg_seq, "WW", coords=c(-800, 1200)))
  cg = smoothHeatmap(PatternHeatmap(biprom_neg_seq, "CG", coords=c(-800, 1200)))
  iq = CoverageHeatmap(biprom_neg, CAGE_clusters, coords=c(-800, 1200), label="IQ")

  png(file.path("plots", info$name, "biproms.png"),
      height=20, width=30, units="cm", res=150)
  plotHeatmapList(list(ww, cg, iq), color=list("Blues", "Blues", brewer.pal(3, "Blues")))
  dev.off()

  png(file.path("plots", info$name, "biproms2.png"),
      height=10, width=30, units="cm", res=150)
  plotHeatmapList(list(ww, cg, iq), color=list("Blues", "Blues", brewer.pal(3, "Blues")))
  dev.off()

  png(file.path("plots", info$name, "biproms3.png"),
      height=6, width=30, units="cm", res=150)
  plotHeatmapList(list(ww, cg, iq), color=list("Blues", "Blues", brewer.pal(3, "Blues")))
  dev.off()
}

