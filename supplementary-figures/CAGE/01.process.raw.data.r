#!/usr/bin/Rscript
library(CAGEr)
library(dplyr)
library(ggplot2)
library(tidyr)
library(BSgenome.braLan.Lenhard.braLan2)
source("R/cc_dominant_ctss.r")

### READ IN RAW DATA ###

mapped = "data"
system("mkdir -p results")

myCAGEset <- new(
    "CAGEset",
    genomeName = "BSgenome.braLan.Lenhard.braLan2",
    inputFiles = c(
        file.path(mapped, "Embr_32cell.sorted.bam"),
        file.path(mapped, "Embr_8h.sorted.bam"),
        file.path(mapped, "Embr_15h.sorted.bam"),
        file.path(mapped, "NeuralTube.sorted.bam"),
        file.path(mapped, "Hepatic.sorted.bam"),
        file.path(mapped, "Muscle.sorted.bam"),
        file.path(mapped, "FemGonads.sorted.bam")),
    inputFilesType = "bam",
    sampleLabels = c(
        "Embr_32cell",
        "Embr_8h",
        "Embr_15h",
        "NeuralTube",
        "Hepatic",
        "Muscle",
        "FemGonads")
)

### LIBRARY PROCESSING AND QC ###

librarySizes(myCAGEset)
normalizeTagCount(myCAGEset, method = "powerLaw", fitInRange = c(5, 1000), alpha = 1.1)

clusterCTSS(object = myCAGEset, threshold = 0.5, thresholdIsTpm = TRUE,
            nrPassThreshold = 1, method = "distclu", maxDist = 20,
            removeSingletons = TRUE, keepSingletonsAbove = 1)

cumulativeCTSSdistribution(myCAGEset, clusters = "tagClusters")
quantilePositions(myCAGEset, clusters = "tagClusters", qLow = 0.1, qUp = 0.9)

aggregateTagClusters(myCAGEset, tpmThreshold = 1, qLow = 0.1, qUp = 0.9, maxDist = 100)
cumulativeCTSSdistribution(myCAGEset, clusters = "consensusClusters")
quantilePositions(myCAGEset, clusters="consensusClusters", qLow=0.1, qUp=0.9)

## saveRDS(myCAGEset, "results/cage_set_processed.rds")

# myCAGEset_tpm_1 = myCAGEset
# clusterCTSS(object = myCAGEset_tpm_1, threshold = 1, thresholdIsTpm = TRUE,
#             nrPassThreshold = 1, method = "distclu", maxDist = 20,
#             removeSingletons = TRUE, keepSingletonsAbove = 1)

# cumulativeCTSSdistribution(myCAGEset_tpm_1, clusters = "tagClusters")
# quantilePositions(myCAGEset_tpm_1, clusters = "tagClusters", qLow = 0.1, qUp = 0.9)
# plotInterquantileWidth(myCAGEset_tpm_1, "tagClusters", tpmThreshold=1, qLow = 0.1, qUp = 0.9, xlim=c(0,50))

plotReverseCumulatives(myCAGEset, fitInRange = c(5, 1000), onePlot = TRUE)

plotCorrelation(myCAGEset, samples = "all", method = "spearman", values = "normalized", what = "consensusClusters")
plotCorrelation(myCAGEset, samples = "all", method = "spearman", values = "normalized", what = "CTSS")
plotInterquantileWidth(myCAGEset, "tagClusters", tpmThreshold=5, qLow = 0.1, qUp = 0.9, xlim=c(0,50))
plotInterquantileWidth(myCAGEset, "consensusClusters", tpmThreshold =1, qLow = 0.1, qUp = 0.9, xlim=c(0,50))

### OUTPUT BED FILES FOR EACH SAMPLE ###

tag_clusters_tpm = function(myCAGEset, ts) {
    names(myCAGEset@tagClusters)
    df = myCAGEset@tagClusters[[ts]]
    df$start = myCAGEset@tagClustersQuantileLow[[ts]]$q_0.1
    df$end = myCAGEset@tagClustersQuantileUp[[ts]]$q_0.9
    df$dominant_ctss_minus_one = df$dominant_ctss - 1
    select(df, chr, start, end, name=cluster, score=tpm, strand, thickStart=dominant_ctss_minus_one, thickEnd=dominant_ctss)
}

options(scipen=100) # no scientific notation

for (ts in myCAGEset@sampleLabels) {
    df = tag_clusters_tpm(myCAGEset, ts)
    write.table(df, file.path("results", paste0("tag_clusters_", ts, ".bed")), row.names=F, col.names=F, quote=F, sep="\t")
}

### OUTPUT CONSENSUNS CLUSTERS WITH TPM AND DOM.CTSS ###

write.table(consensusClusters(myCAGEset), quote=F, file="results/consensus_clusters_100bp.bed", sep="\t", col.names=F, row.names=F)

consensus_clusters = consensusClustersDominantCTSS(myCAGEset)
consensus_clusters_tpm = cbind(consensus_clusters, myCAGEset@consensusClustersTpmMatrix) %>% select(-tpm)

write.table(consensus_clusters_tpm, "results/consensus_clusters_tpm.txt", row.names=F, col.names=T, quote=F, sep="\t")

consensus_clusters_long = gather(consensus_clusters_tpm, stage, tpm, Embr_32cell:FemGonads)
write.table(consensus_clusters_long,
            "results/consensus_clusters_tpm_long.txt",
            row.names=F, col.names=T, quote=F, sep="\t")

### OUTPUT CONSENSUS CLUSTERS WITH ANNOTATION ###

annotation = import("./data/Bla_annot-FINAL_v4_names.gtf")

exons = subset(annotation, type=="exon")

consensus_clusters_gr = makeGRangesFromDataFrame(consensus_clusters, keep.extra.columns=TRUE, seqinfo=seqinfo(braLan2))
dtn = distanceToNearest(consensus_clusters_gr, exons)
sum(duplicated(queryHits(dtn))) # should be 0
distances = mcols(dtn)$dist

percentage = function(x) 100*sum(x)/length(x)

percentage(distances < 1000)
percentage(distances < 2000)
percentage(distances < 5000)

hits = dtn[distances <= 1000]

consensus_clusters_gr$gene_id = "No gene assigned"
consensus_clusters_gr$gene_id[queryHits(hits)] = exons$gene_id[subjectHits(hits)]
consensus_clusters_gr$source = "None"
consensus_clusters_gr$source[queryHits(hits)] = as.character(exons$source[subjectHits(hits)])

consensus_clusters_gr$names = with(consensus_clusters_gr, paste0(gene_id, "|",
                                               seqnames(consensus_clusters_gr), "|",
                                               "ctss=", dominant.ctss))

write.table(as.data.frame(consensus_clusters_gr), "results/consensus.clusters.annotated.txt", quote=F, sep="\t", row.names=F, col.names=T)

### OUTPUT TRACKS FOR TRACKHUB ###
exportCTSStoBedGraph(myCAGEset, values = "normalized", oneFile = FALSE)

getCTSS(myCAGEset)
saveRDS(myCAGEset, "results/my_cage_set.rds")


