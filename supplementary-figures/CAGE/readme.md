# CAGE Analysis

* Scripts can be run in place.
* Relevant data should be added to a folder named "data" in the same directory.
* Results and figures will be written to "./results" and "./plots" respectively.

## Packages Required
 
```
#bioconductor:annotation
library(BSgenome.braLan.Lenhard.braLan2)
library(BSgenome.Dmelanogaster.UCSC.dm3)
library(BSgenome.Drerio.UCSC.danRer7)
library(BSgenome.Mmusculus.UCSC.mm9)
library(org.Dm.eg.db)
library(org.Dr.eg.db)
library(org.Hs.eg.db)
library(org.Mm.eg.db)

#bioconductor:analysis
library(CAGEr)
library(GenomicRanges)
library(heatmaps)

#CRAN
library(RColorBrewer)
library(data.table)
library(dplyr)
library(ggplot2)
library(readr)
library(rtracklayer)
library(tidyr)
```
