# Getting regulatory landscape boundaries from 4C experiments

We will use as input file a bedgraph that contains the raw count of reads overlapping each restriction fragment of the 4Cseq experiment. We will then smooth it and use peakC from deWitLab/peakC (Geeven et al. 2018, https://doi.org/10.1093/nar/gky443) to get significant interactions. peakC uses a monotonic regression and rank products from two replicates to do so. First and last interacting fragments are considered to be regulatory landscape boundaries. In order to make the calculation more robust two additions were made. We further filtered out interactions happening in regions that were poorly covered. Briefly, for each fragment we calculate a local coverage score considering the nearest 25 fragments. The score is the sum of the fragments covered by at least one read. The local coverage score for a fragment to be considered an interaction should be in the top 25% of the experiment. We also required interactions to appear in regions where the isotonic regression calculated by peakC was above an empirical threshold to ensure a sufficient signal to noise ratio.This empirical threshold was set to 2.5% of the maximum value of the isotonic for each experiment.


In order to reproduce the sample analysis with default parameters run the following: 

 
```sh
>> Rscript scripts/fourC_bcaller.R --input sample_data/sample.input --outdir sample_results

```

Input file consists in a 5 columns tsv with:

bedgraph1_path--bedgraph2_path--bait_chromosome--bait_coordinate--exp_name

Additional parameters can be changed, and defaults are stated in the help (concordant with the ones used in the paper)

```sh
>> Rscript scripts/fourC_bcaller.R -h
```

It produces two output files for experiments, one is a table ".Coutput" with internal values such as pvalues, isotonic values, zero smoothing window values, etc. The other is the following plot. 

![png](sample_results/plots/shha_24hpf.png/)  

The first two graphics are the two 4Cseq replicates with the isotonic and the minimum isotonic heights drawn in blue. In read are the regions that are considered significant peaks. The borders calculated are also stated. In the botton graphic is the smoothing window of zeros and in blue the threshold for this window. 

The final file is a tsv file with only the boundary coordinates in [bed format](sample_results/sample.boundaries)




