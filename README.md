# Abstract
Chordates share a bodyplan that was greatly elaborated in vertebrates. Vertebrates also evolved distinctive genomes, sculpted by two whole genome duplications (WGD). To investigate the evolution of genome regulation in chordates, we characterized promoters, DNA methylation, chromatin accessibility, histone modifications and transcriptomes in multiple tissues and throughout development of amphioxus. This revealed an intermediate stage in the evolution of differentially methylated regulatory elements, and high conservation of gene expression and its cis-regulatory logic between amphioxus and vertebrates, maximally at a developmental phylotypic period. We also unraveled the principal route of regulatory evolution following WGD: over 80% of broadly expressed gene families with multiple paralogs in vertebrates have members that restricted their ancestral expression, undergoing specialization rather than subfunctionalization. Counter-intuitively, vertebrate genes that underwent expression restriction increased the complexity of their regulatory landscapes. Altogether, these data pave the way for a better understanding of the regulatory principles underlying key vertebrate innovations.


## How some figures were created:

### Main Figures
* [Fig 1 d](main-figures/F1d.md)
* [Fig 3 c](main-figures/F3c.md)
* [Fig 4 a, b, d](main-figures/F4acf.md)
* [Fig 5 a, b, c](main-figures/F56.md)
* [Fig 5 e](main-figures/F5e.md)
* [Fig 6 a-e](main-figures/F6ae.md)
* [Fig 6 g](main-figures/F56.md)

### Extended Data Figures 
* [EDF 2 c](main-figures/F1d.md)
* [EDF 3 , cage analyses](supplementary-figures/CAGE/readme.md)
* [EDF 4 , cage analyses](supplementary-figures/CAGE/readme.md)
* [EDF 8 a](supplementary-figures/EDF8a.md)
* [EDF 8 b](main-figures/F4a.md)
* [EDF 9 a](supplementary-figures/EDF9a.md)
* [EDF 9 b, c](supplementary-figures/EDF9bc.md)
* [EDF 9 d](supplementary-figures/EDF9d.md)
* [EDF 9 f](supplementary-figures/EDF9f.md)
* [EDF 10 a, b, f, g](main-figures/F6ae.md)
* [EDF 10 L](supplementary-figures/EDF10L.md)

### Supplementary Figures
* [Sup. Fig 6 a, b](supplementary-figures/SF6a.md)
* [Sup. Fig 6 d](supplementary-figures/SF6d.md)

## Other analyses
* 4c-seq related :
    * [Boundary calling scripts and guide](other-analyses/4Cseq/)


* CAGE Analysis
    * [Supplementary Figures 3 and 4](supplementary-figures/CAGE/readme.md)
 

* Atac-seq related :
    * [Mapping ATACseq reads](other-analyses/atac-seq-analysis-details.md)
    * [Getting the nucleosome-free reads, +5-4 ATACseq correction](other-analyses/atac-seq-analysis-details.md)
    * [Peak calling](other-analyses/atac-seq-analysis-details.md)
    * [ATAC dynamic peaks](other-analyses/atac-seq-analysis-details.md)
    * [Downsampling](other-analyses/atac-seq-analysis-details.md)
    * [Mapping PWMs to ATACseq peaks](other-analyses/atac-seq-analysis-details.md)
    * [BASAL and GREAT regions](other-analyses/atac-seq-analysis-details.md)    





